## Website

![Website](preview.png)

### Due Dates
Features Discussion, Design Sketch:
Tuesday, March 7, 2017
In class

Project:
Thursday, March 16, 2017
S01 9:30 a.m.
S02 11:00 a.m.

### Philosophy

The key underlying component in a browser which facilitates a smooth transition from Processing, based in Java, to p5.js is the HTML5 canvas. The popularity of JavaScript as a language and the prevalence of libraries to ease development make more alternatives to Processing's model available than ever. Furthermore, the flexibility of the HTML5 canvas allows a website to not merely be the delivery mechanism for interactive art or video games but to be the art or game itself.

### Directions

* Form a group of 2 - 4 people.
* Draft a paper sketch of a webpage. This will be turned in midway through the project.
* Draft an ordered list of features that will characterize the webpage. This will be discussed and turned in midway through the project.
    * Order them from most essential to least essential.
    * Use complete sentences.
    * Aim for seven features.
* Use a JavaScript library in addition - or as an alternative - to p5.js. Some suggested possibilities:
    * [p5.dom](http://p5js.org/reference/#/libraries/p5.dom)
    * [p5.play](http://p5play.molleindustria.org/)
    * [p5.sound](http://p5js.org/reference/#/libraries/p5.sound)
    * [Easel](http://www.createjs.com/easeljs)
    * [Paper](http://paperjs.org/)
    * [Phaser](https://phaser.io)
    * [Pixi](http://www.pixijs.com/)
    * [Three](https://threejs.org/)
* Either create or access [DOM (Document-Object Model) elements](https://www.w3schools.com/js/js_htmldom.asp) using JavaScript or HTML.

### Inspiration

* [Design + Code – Hour 2: Wireframing](https://youtu.be/PQO47_AP6lo)

### Rubric

Points | Description
------ | -----------
30 | A drawing has been made on paper to plan the design of the webpage.
30 | The webpage's feature list is discussed at the project midway point.
40 | The project has been created by a group. (Include the names of group members in a comment if only one person is submitting.)
20 | The webpage demonstrates sustained intentionality in design, including hierarchy for the arrangement of DOM elements, choice of color, choice and flow of user interaction.
10 | The submission uses a new JS library. (The core p5 library does not count.)
40 | The final submission includes at least 4/7 of the projected features.
10 | The code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps.
10 | The webpage runs without errors.
10 | All files are contained in a folder, which is named according to the following convention: s17_magd271_assignment4_lastnames. The folder has been compressed into a .zip file and upload to D2L.
200 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
