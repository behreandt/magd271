'use strict';

var rndr, scene, cam;

// Lights
var directional, pt1, pt2;

// Components which contribute to a textured 3D object.
var geom;
var mtrl;
var texture, bumpmap, alpha;
var objs;

// Number of objects in the ring to display.
var count = 14;

// Radius of the ring.
var radius = 10;

// Variable to track frame count,
// which is used to create theta.
var fCount = 0;

// HTML DOM elements.
var loremIpsum;

window.onload = function() {

    rndr = new THREE.WebGLRenderer({
        antialias: true
    });
    rndr.setPixelRatio(window.devicePixelRatio);
    rndr.setSize(window.innerWidth, window.innerHeight);
    rndr.setClearColor(0x202020);

    scene = new THREE.Scene();

    cam = new THREE.PerspectiveCamera(50,
        window.innerWidth / window.innerHeight,
        0.1, 1000);
    cam.position.z = 500;

    directional = new THREE.DirectionalLight(0xffffff, 1);
    directional.position.set(0, -0.5, 0.5);
    scene.add(directional);

    pt1 = new THREE.PointLight(0x00ff88);
    pt1.position.set(0, -window.innerHeight / 2.0, 0);
    scene.add(pt1);

    pt2 = new THREE.PointLight(0x0088ff);
    pt2.position.set(window.innerWidth / 2.0, window.innerHeight / 2.0, 0);
    scene.add(pt2);

    // Loader to handle loading of textures.
    texture = new THREE.TextureLoader().load('assets/fan.png');
    // Horizontal wrapping (U).
    texture.wrapS = THREE.RepeatWrapping;
    // Vertical wrapping (V).
    texture.wrapT = THREE.RepeatWrapping;
    // Sets UV scale.
    texture.repeat.set(8, 4);

    // A bump map adds texture to the surface of the material
    // without actually changing the geometry.
    bumpmap = new THREE.TextureLoader().load('assets/noise.png');
    bumpmap.wrapS = THREE.RepeatWrapping;
    bumpmap.wrapT = THREE.RepeatWrapping;

    // An alpha map converts grayscale value (0 - 255) to alpha,
    // allowing you to create translucent objects provided that you
    // set the material's transparency to true (see below).
    alpha = new THREE.TextureLoader().load('assets/alpha.png');
    alpha.wrapS = THREE.RepeatWrapping;
    alpha.wrapT = THREE.RepeatWrapping;

    mtrl = new THREE.MeshStandardMaterial();
    mtrl.map = texture;
    mtrl.bumpMap = bumpmap;
    mtrl.alphaMap = alpha;
    mtrl.transparent = true;

    geom = new THREE.SphereGeometry(28, 32, 32);

    // Create a ring of objects.
    objs = [];
    var theta = 0;
    for (var i = 0; i < count; ++i) {
        theta = map(i, 0, count, 0, Math.PI * 2.0);
        objs.push(new THREE.Mesh(geom, mtrl));
        objs[i].position.set(Math.cos(theta) * radius,
            0, Math.sin(theta) * radius);
        scene.add(objs[i]);
    }

    document.body.appendChild(rndr.domElement);
    window.addEventListener('resize', onWindowResize, false);

    // Acquire an HTML DOM element by its ID, then store that reference.
    loremIpsum = document.getElementById('loremIpsum');
    // Add an event listener to the stored DOM element. The first argument
    // specifies what kind of event to listen for, the second argument creates
    // a function which changes the opacity of the element.
    loremIpsum.addEventListener('mouseover', function() {
        loremIpsum.style.opacity = 1;
    });
    loremIpsum.addEventListener('mouseout', function() {
        loremIpsum.style.opacity = 0;
    });

    render();
}

function render() {
    requestAnimationFrame(render);
    rndr.render(scene, cam);

    var theta = 0,
        hghtOsc = 0,
        scl = 0;

    // Add to frame count each frame.
    fCount++;

    // The size of the ring and the amount which it oscillates up
    // and down are adjusted based on window height and width.
    hghtOsc += (window.innerHeight * 4.0 - hghtOsc) * 0.025;
    radius += (window.innerWidth * 0.25 - radius) * 0.025;

    // Scale the size of the spheres based on the window's height.
    scl = window.innerHeight * 0.0025;

    // Cycle through all the spheres in the ring.
    for (var i = 0; i < count; ++i) {
        theta = map(i, 0, count, 0, Math.PI * 2.0);
        objs[i].rotation.x = i + fCount / 90.0;
        objs[i].rotation.y = i + fCount / 45.0;
        objs[i].position.y = Math.sin(i + fCount / 60.0) * hghtOsc;
        objs[i].position.x = Math.cos(theta + fCount / 180.0) * radius;
        objs[i].position.z = Math.sin(theta + fCount / 180.0) * radius;
        objs[i].scale.set(scl, scl, scl);
    }

    // Cycle the textures on all the spheres.
    texture.offset.x += 0.005;
    texture.offset.y += 0.005;
}

function onWindowResize() {
    cam.aspect = window.innerWidth / window.innerHeight;
    cam.updateProjectionMatrix();
    rndr.setSize(window.innerWidth, window.innerHeight);
}

// From the map function in Processing and p5.js
// https://github.com/processing/p5.js/blob/1c0a623923530d78bda2390cf5b0da0bb3c09196/src/math/calculation.js#L440
function map(value, lbOrigin, ubOrigin, lbTarget, ubTarget) {
    return ((value - lbOrigin) / (ubOrigin - lbOrigin)) * (ubTarget - lbTarget) + lbTarget;
}
