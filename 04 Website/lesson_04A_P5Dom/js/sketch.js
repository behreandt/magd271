// This will make JavaScript a bit more scrupulous about error-checking. For example,
// you won't be able to use variables unless they have been declared. See
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode .
'use strict';

// Variable to hold the HTML5 canvas once created in setup.
var cnvs;
// Variable to hold an HTML element <div></div> placed in the HTML.
var exmplDiv;
// Variable to hold an HTML element <input type='range'> placed in the HTML.
var exmplSlider;
// Variable to hold an HTML element <input type='radio'> placed in the HTML.
var exmplRadio;
// Variables to hold an HTML element <input type='checkbox'>
var exmplCheckBoxSin, exmplCheckBoxCos;
// Variables to hold HTML elements <input> where you specify the type.
var exmplInputSinMin, exmplInputSinMax, exmplInputCosMin, exmplInputCosMax;
// Variables to hold an HTML element <button>
var exmplBttn;
// Errata.
var leftMargin, lineHeight,
    val, theta1, theta2, autoToggle, radius,
    bkgClr, bkgClrMousePressed, bkgClrMouseReleased,
    x1, x2, y1, y2;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    background(64);
    leftMargin = windowWidth * 0.0625;
    lineHeight = windowHeight * 0.0825;
    theta1 = 0;
    theta2 = 0;
    radius = windowHeight / 2.25;

    // Create the div. The text between parentheses
    // will be assigned to the innerHTML.
    exmplDiv = createDiv('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');

    // Sets the div style to absolute position, then positions it
    // x pixels from the left, y pixels from the top.
    exmplDiv.position(leftMargin, lineHeight);

    // Sets any CSS styles for the created div, where the first
    // argument is the key, the second argument is the value.
    // See W3 Schools for reference on styling possibilities:
    // https://www.w3schools.com/css/
    // Note that anything in the CSS3 section leads to complications
    // due to differences in syntax between browsers.
    exmplDiv.style('color', 'white');
    exmplDiv.style('font-family', 'sans-serif');
    exmplDiv.style('font-size', '0.9em');
    exmplDiv.style('width', '50%');

    // For any kind of element where you want to establish a
    // a standard style, it would be better to create a separate
    // .css file.
    exmplSlider = createSlider(0, /* Lower bound */
        1, /* Upper bound */
        0, /* Default value */
        0); /* Step. If set to 0, then the slider movement will be continuous. */
    exmplSlider.position(leftMargin, lineHeight * 2);

    // Radio button set.
    exmplRadio = createRadio();
    var cw = exmplRadio.option('Clockwise', 0);
    var ccw = exmplRadio.option('Counter-clockwise', 1);
    cw.checked = true;
    exmplRadio.position(leftMargin, lineHeight * 3);

    // Check box set.
    exmplCheckBoxSin = createCheckbox('Sine', true);
    exmplCheckBoxCos = createCheckbox('Cosine', true);
    exmplCheckBoxSin.position(leftMargin, lineHeight * 4);
    exmplCheckBoxCos.position(leftMargin + 50, lineHeight * 4);

    // Number input set. Allow the user to enter the minimum and maximum
    // rotation to be plugged into sine and cosine in degrees.
    exmplInputSinMin = createInput('0', 'number');
    exmplInputSinMin.attribute('min', 0);
    exmplInputSinMin.attribute('max', 360);
    exmplInputSinMin.attribute('step', 1);
    exmplInputSinMin.position(leftMargin, lineHeight * 5);

    exmplInputSinMax = createInput(360, 'number');
    exmplInputSinMax.attribute('min', 0);
    exmplInputSinMax.attribute('max', 360);
    exmplInputSinMax.attribute('step', 1);
    exmplInputSinMax.position(leftMargin + 50, lineHeight * 5);

    exmplInputCosMin = createInput('0', 'number');
    exmplInputCosMin.attribute('min', 0);
    exmplInputCosMin.attribute('max', 360);
    exmplInputCosMin.attribute('step', 1);
    exmplInputCosMin.position(leftMargin, lineHeight * 6);

    exmplInputCosMax = createInput(360, 'number');
    exmplInputCosMax.attribute('min', 0);
    exmplInputCosMax.attribute('max', 360);
    exmplInputCosMax.attribute('step', 1);
    exmplInputCosMax.position(leftMargin + 50, lineHeight * 6);

    // Button
    // Event listeners respond to user input, whether it be by mouse or touch.
    // They can be added to any DOM element. Since a function can be passed as
    // an argument to another function in JavaScript, you can create a function
    // right here which specifies what should happen when, for example, the
    // mouse leaves the canvas or is pressed.
    exmplBttn = createButton('Run');
    autoToggle = false;
    exmplBttn.position(leftMargin, lineHeight * 7);
    exmplBttn.mousePressed(function() {
        autoToggle = !autoToggle;
        if (autoToggle) {
            exmplBttn.elt.textContent = "Stop";
        } else {
            exmplBttn.elt.textContent = "Run";
        }
    });

    bkgClr = color(32, 32, 32);
    bkgClrMousePressed = color(32, 0, 0);
    bkgClrMouseReleased = color(0, 32, 32);

    cnvs.mousePressed(function() {
        bkgClr = bkgClrMousePressed;
    });

    cnvs.mouseOut(function() {
        bkgClr = bkgClrMouseReleased;
    });

}

function draw() {
    background(bkgClr);

    // If the auto-run button has not been pressed, then value is
    // retrieved from the slider. If it has been pressed, then value
    // is increased every frame until it hits 1, whereupon it is reset
    // to zero. The slider's value is updated to match value.
    if (autoToggle) {
        val = (val + 0.005) % 1;
        exmplSlider.value(val);
    } else {
        val = exmplSlider.value();
    }

    // Remap the value from the slider from 0 - 1 to 0 - TWO_PI.
    theta1 = map(val, 0, 1,
        radians(exmplInputCosMin.value()),
        radians(exmplInputCosMax.value()));
    theta2 = map(val, 0, 1,
        radians(exmplInputSinMin.value()),
        radians(exmplInputSinMax.value()));

    // If the radio button's value is 1, then Counter-clockwise
    // has been selected, and so theta should be reversed.
    if (exmplRadio.value() == 1) {
        theta1 = -theta1;
        theta2 = -theta2;
    }

    // x1 and y1 are updated to match the window's width and height.
    x1 = windowWidth / 2.0;
    y1 = windowHeight / 2.0;

    // x2 and y2 are updated only if cosine and sine checkboxes are
    // checked.
    if (exmplCheckBoxCos.checked()) {
        x2 = x1 + radius * cos(theta1);
    }
    if (exmplCheckBoxSin.checked()) {
        y2 = y1 + radius * sin(theta2);
    }

    strokeWeight(1.5);
    noFill();
    stroke(255, 127, 0);
    ellipse(x1, y1, 30, 30);
    stroke(0, 127, 255);
    line(x1, y1, x2, y2);
    stroke(0, 255, 127);
    ellipse(x2, y2, 15, 15);
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
    leftMargin = windowWidth * 0.0625;
    lineHeight = windowHeight * 0.0825;
    radius = windowHeight / 2.5;
}
