'use strict';

var rndr, scene, cam;

// Lights
var sptlts, count, frCount;

// Geometry
var grndGeom, trsKntGeom;

// Texture
var dgnstc, displacement;

// Materials
var grndMat, trsKntMat;

// Meshes
var ground, torusKnot;

window.onload = function() {

    rndr = new THREE.WebGLRenderer({
        antialias: true
    });
    rndr.setPixelRatio(window.devicePixelRatio);
    rndr.setSize(window.innerWidth, window.innerHeight);
    rndr.shadowMap.enabled = true;
    rndr.shadowMap.type = THREE.PCFShadowMap;
    rndr.setClearColor(0x202020);

    // Create a scene.
    scene = new THREE.Scene();

    // Create a camera, set its position.
    cam = new THREE.PerspectiveCamera(50, /* Field of View */
        window.innerWidth / window.innerHeight, /* Aspect */
        0.01, /* Near Plane */
        3000); /* Far Plane */

    // Lights.
    sptlts = [];
    count = 10;
    var t;
    var cstart = new THREE.Color(0xAA0000);
    var cstop = new THREE.Color(0x0000AA);
    frCount = 0;
    for (var i = 0; i < count; ++i) {
        t = map(i, 0, count, 0, Math.PI * 2);
        sptlts.push(new THREE.PointLight(cstart));
        sptlts[i].color.lerp(cstop, i / (count - 1));
        sptlts[i].position.set(Math.cos(t) * window.innerWidth,
            Math.sin(t) * window.innerHeight, Math.sin(t) * window.innerWidth);
        sptlts[i].castShadow = true;
        scene.add(sptlts[i]);
    }

    // Geometry.
    grndGeom = new THREE.PlaneGeometry(window.innerWidth,
        window.innerHeight);
    trsKntGeom = new THREE.TorusKnotGeometry(50, /* Radius */
        12.5, /* Tube Diameter */
        128, /* Tube Segments */
        16); /* Radial Segments */

    // Texture.
    var txtrLdr = new THREE.TextureLoader();
    dgnstc = txtrLdr.load('assets/diagnosticTexture.png');

    // Materials.
    grndMat = new THREE.MeshStandardMaterial({
        color: 0xffffff
    });
    grndMat.map = dgnstc;
    trsKntMat = new THREE.MeshStandardMaterial({
        color: 0xff8800
    });
    trsKntMat.map = dgnstc;

    // Meshes.
    ground = new THREE.Mesh(grndGeom, grndMat);
    ground.position.set(0, -window.innerHeight / 2.0, window.innerHeight * -1.5);
    ground.rotation.set(-Math.PI / 2.0, 0, 0);
    ground.receiveShadow = true;
    ground.castShadow = true;
    scene.add(ground);

    torusKnot = new THREE.Mesh(trsKntGeom, trsKntMat);
    torusKnot.position.set(0, 0, window.innerHeight * -0.5);
    torusKnot.castShadow = true;
    torusKnot.receiveShadow = true;
    scene.add(torusKnot);

    // Essentials.
    document.body.appendChild(rndr.domElement);
    window.addEventListener('resize', onWindowResize, false);
    render();
}

function render() {
    requestAnimationFrame(render);
    rndr.render(scene, cam);

    torusKnot.rotation.x += 0.01;
    torusKnot.rotation.z += 0.01;
    frCount += 1;
    var t = 0;
    for (var i = 0; i < count; ++i) {
        t = map(i, 0, count, 0, Math.PI * 2)
        sptlts[i].position.set(
            Math.cos(t + frCount / 90.0) * window.innerWidth,
            Math.sin(t + frCount / 60.0) * window.innerHeight,
            Math.sin(t + frCount / 90.0) * window.innerWidth);
    }
}

function onWindowResize() {
    cam.aspect = window.innerWidth / window.innerHeight;
    cam.updateProjectionMatrix();
    rndr.setSize(window.innerWidth, window.innerHeight);
}

// From the map function in Processing and p5.js
// https://github.com/processing/p5.js/blob/1c0a623923530d78bda2390cf5b0da0bb3c09196/src/math/calculation.js#L440
function map(value, lbOrigin, ubOrigin, lbTarget, ubTarget) {
    return ((value - lbOrigin) / (ubOrigin - lbOrigin)) * (ubTarget - lbTarget) + lbTarget;
}
