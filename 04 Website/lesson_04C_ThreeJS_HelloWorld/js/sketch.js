'use strict';

// Critical variables to store the WebGL renderer, scene and camera.
var rndr, scene, cam;

// Lights
var directional;

// Geometry
var box, cylinder, icosahedron, sphere, torus;

// Materials
var basic, lambert, phong, standard, normal;

// Meshes
var m1, m2, m3, m4, m5;

// Wait until the browser has loaded the HTML document, then
// perform this function on completion of load. This is
// the rough equivalent of void setup() or void Start().
window.onload = function() {

    // Set up the renderer, including size. Different libraries
    // respond to JavaScript's lack of function overloading - esp.
    // in constructors - differently. For THREE.js, you can often
    // pass into your constructor an optional object literal by
    // using curly braces, '{}', specifying certain properties
    // then assigning them a value with a colon, ':' .
    rndr = new THREE.WebGLRenderer({
        antialias: true
    });
    // For high-density screens.
    // Equivalent to Processing's pixelDensity(displayDensity());
    rndr.setPixelRatio(window.devicePixelRatio);
    rndr.setSize(window.innerWidth, window.innerHeight);
    // Equivalent to Processing's background();
    rndr.setClearColor(0x202020);

    // Create a scene.
    scene = new THREE.Scene();

    // Create a camera, set its position.
    cam = new THREE.PerspectiveCamera(50, /* Field of View */
        window.innerWidth / window.innerHeight, /* Aspect */
        0.1, /* Near Plane */
        1000); /* Far Plane */
    cam.position.z = 600;

    // Lights.
    directional = new THREE.DirectionalLight(0xffffff, 1);
    directional.position.set(0, 0.5, 0.25);

    // Geometry.
    box = new THREE.BoxGeometry(100, /* Width */
      100, /* Height */
      100); /* Depth */
    cylinder = new THREE.CylinderGeometry(25, /* Radius Top */
       50, /* Radius Bottom */
       100, /* Height */
       24, /* Radius Segments */
       5); /* Height Segments */
    icosahedron = new THREE.IcosahedronGeometry(50); /* Radius */
    sphere = new THREE.SphereGeometry(50, /* Radius */
      32, /* Width Segments */
      32); /* Height Segments */
    torus = new THREE.TorusGeometry(50, /* Radius */
      25, /* Tube Diameter */
      32, /* Radial Segments */
      32); /* Tubular Segments */

    // Materials.
    // Unaffected by lighting, meaning there is no shading.
    basic = new THREE.MeshBasicMaterial({color: 0xff0000});
    // For shaded, non-shiny surfaces.
    lambert = new THREE.MeshLambertMaterial({color: 0x00ff00});
    // For shaded, shiny surfaces.
    phong = new THREE.MeshPhongMaterial({color: 0x0000ff});
    // Uses physically based rendering (PBR).
    standard = new THREE.MeshStandardMaterial({color: 0xffff00});
    // Displays surface normals as colors.
    normal = new THREE.MeshNormalMaterial();

    // Meshes.
    m1 = new THREE.Mesh(box, basic);
    m1.position.set(-window.innerWidth * 0.325, 0, 0);
    m2 = new THREE.Mesh(cylinder, lambert);
    m2.position.set(-window.innerWidth * 0.125, 0, 0);
    m3 = new THREE.Mesh(icosahedron, phong);
    m3.position.set(0, 0, 0);
    m4 = new THREE.Mesh(sphere, standard);
    m4.position.set(window.innerWidth * 0.125, 0, 0);
    m5 = new THREE.Mesh(torus, normal);
    m5.position.set(window.innerWidth * 0.325, 0, 0);

    // Add items to the scene.
    scene.add(directional);
    scene.add(m1);
    scene.add(m2);
    scene.add(m3);
    scene.add(m4);
    scene.add(m5);

    // Add the renderer canvas to the document body.
    document.body.appendChild(rndr.domElement);

    // Add an event listener for the resizing of the browser window.
    // When this event happens, the function onWindowResize, defined
    // below will be called, adjusting the canvas and camera.
    window.addEventListener('resize', onWindowResize, false);

    // Begin the render loop.
    render();
}

// The rough equivalent of void draw() or void Update()
function render() {

    // These two following lines are necessities.
    requestAnimationFrame(render);
    rndr.render(scene, cam);

    m1.rotation.x += 0.01;
    m2.rotation.x += 0.01;
    m3.rotation.x += 0.01;
    m5.rotation.x += 0.01;

    m1.rotation.y += 0.01;
    m2.rotation.y += 0.01;
    m3.rotation.y += 0.01;
    m5.rotation.y += 0.01;
}

function onWindowResize() {
    cam.aspect = window.innerWidth / window.innerHeight;
    cam.updateProjectionMatrix();
    rndr.setSize(window.innerWidth, window.innerHeight);
}
