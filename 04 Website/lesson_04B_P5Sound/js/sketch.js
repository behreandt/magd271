'use strict';

var cnvs, oscillator,
    oscType, oscTypeSelector,
    pitch, pitchDisplay, pitchSlider,
    vol, volDisplay, volSlider,
    isPlaying,
    fft, startColor, stopColor;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    colorMode(HSB, 359, 100, 100, 100);
    background(0);

    // Set default values of oscillator.
    oscillator = new p5.Oscillator('sine');
    oscillator.freq(440);
    oscillator.amp(0.5);

    // Allow user to change type of oscillator.
    oscType = 'sine';
    oscTypeSelector = createSelect();
    oscTypeSelector.position(10, 10);
    oscTypeSelector.option('sine');
    oscTypeSelector.option('triangle');
    oscTypeSelector.option('sawtooth');
    oscTypeSelector.option('square');
    oscTypeSelector.changed(function() {
        oscType = oscTypeSelector.value();
        oscillator.setType(oscType);
    });

    // Allow user to change frequency.
    pitch = 440;
    pitchDisplay = createSpan("Pitch: " + pitch + " Hz");
    pitchDisplay.position(180, 40);
    pitchSlider = createSlider(75, /* Minimum */
        2000, /* Maximum */
        pitch, /* Default, A4 = 440 */
        1); /* Step */
    pitchSlider.position(10, 40);
    pitchSlider.style('width', '160px');
    pitchSlider.changed(function() {
        pitch = pitchSlider.value();
        pitchDisplay.elt.textContent = "Pitch: " + round(pitch) + " Hz";
        oscillator.freq(pitch);
    });

    // Allow the user to change volume.
    vol = 0.5;
    volDisplay = createSpan("Volume: " + vol);
    volDisplay.position(180, 80);
    volSlider = createSlider(0, 1, vol, 0.01);
    volSlider.position(10, 80);
    volSlider.style('width', '160px');
    volSlider.changed(function() {
        vol = volSlider.value();
        volDisplay.elt.textContent = "Volume: " + vol;
        oscillator.amp(vol);
    });

    // Use a mouse press to play/stop the oscillator.
    isPlaying = true;
    cnvs.mousePressed(function() {
        isPlaying = !isPlaying;
        if (isPlaying) {
            oscillator.stop();
        } else {
            oscillator.start();
        }
    });

    // Fast-Fourier Transform to analyze sound.
    fft = new p5.FFT();
    startColor = color(120, 100, 100);
    stopColor = color(240, 100, 100);
}

function draw() {
    // background(0);
    noStroke();
    fill(0, 0, 0, 10);
    rect(0, 0, width, height);

    var wvfm = fft.waveform();
    var x = 0, y = 0, size = wvfm.length;
    for(var i = 0; i < size; ++i) {
      x = map(i, 0, size, 0, width);
      y = height / 2.0 + map(wvfm[i], -1, 1, -height / 2.0, height / 2.0);
      fill(lerpColor(startColor, stopColor, i / size));
      ellipse(x, y, 2, 2);
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
