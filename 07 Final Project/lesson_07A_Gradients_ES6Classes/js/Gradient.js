'use strict';

class Gradient extends Native {
    constructor(cnt,
        startX, startY,
        stopX, stopY,
        clrstops) {

        // The parent- or super-class's constructor, Native,
        // can be called using super.
        super(cnt);

        this._start = createVector(startX, startY);
        this._stop = createVector(stopX, stopY);

        this._colorStops = [];
        for (var i = 0, size = clrstops.length; i < size; ++i) {
            this._colorStops.push({
                step: map(i, 0, size - 1, 0, 1),
                color: color(clrstops[i])
            });
        }
    }

    get start() {
        return this._start;
    }

    set start(v) {
        this._start = v;
    }

    setStart(x, y) {
        this._start.set(x, y);
    }

    get stop() {
        return this._stop;
    }

    set stop(v) {
        this._stop = v;
    }

    setStop(x, y) {
        this._stop.set(x, y);
    }

    // It is generally considered poor practice to provide
    // getters and setters to an entire array.
    getColorStop(i) {
        return this._colorStops[i];
    }

    setColorStop(i, c) {
        this._colorStop[i] = c;
    }
}
