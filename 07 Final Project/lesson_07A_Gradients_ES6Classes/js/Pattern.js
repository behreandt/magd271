'use strict';

class Pattern extends Native {
    constructor(cnt, imgPth, repetition) {
        // The parent- or super-class's constructor, Native,
        // can be called using super.
        super(cnt);
        this._repetition = repetition;
        this._img = new Image();
        this._img.src = imgPth;

        // We need to wait for our image to finish loading before we can use it
        // to create a pattern. Thus, we need to give the image an onload function.
        // However, inside that function we create, 'this' refers to the image, the
        // object for which onload is a method. This is a problem, since the image
        // cannot set the Pattern object's fill or context properties. We need to
        // use the bind function to make 'this' refer to the Pattern.
        var loadComplete = function() {
          this._fill = this._cntxt.createPattern(this._img, this._repetition);
        }
        this._img.onload = loadComplete.bind(this);
    }
}
