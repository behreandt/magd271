'use strict';

class LinearGradient extends Gradient {
    constructor(cnt,
        startX, startY,
        stopX, stopY,
        clrstops) {
        super(cnt, startX, startY, stopX, stopY, clrstops);
        this._fill = this._cntxt.createLinearGradient(
            startX, startY,
            stopX, stopY);
        for (var i = 0, size = this._colorStops.length; i < size; ++i) {
            this._fill.addColorStop(this._colorStops[i].step,
                this._colorStops[i].color.toString());
        }
    }

    update(startX, startY, stopX, stopY, cs) {
        this._start.set(startX, startY);
        this._stop.set(stopX, stopY);
        this._fill = this._cntxt.createLinearGradient(
            startX, startY,
            stopX, stopY);
        if (cs === undefined) {
            for (var i = 0, size = this._colorStops.length; i < size; ++i) {
                this._fill.addColorStop(this._colorStops[i].step,
                    this._colorStops[i].color.toString());
            }
        } else {
            for (var i = 0, size = cs.length; i < size; ++i) {
                this._fill.addColorStop(map(i, 0, size - 1, 0, 1),
                    cs[i].toString());
            }
        }
    }
}
