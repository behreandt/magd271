'use strict';

var cnvs, cntxt,
    cnvsLinGrad1, cnvsRadGrad1, divLinGrad1, /* To demonstrate kinds of gradient */
    pts, count, /* To simulate points of data on a line chart */
    angle, angleIncr, /* To demonstrate an arc. */
    cnvsPattern1; /* To demonstrate patterns on canvas shapes. */

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    background(64);

    // In the vanilla HTML5 Canvas API, we need to acquire
    // the drawing context of the canvas DOM element. The
    // drawing context is like an imaginary pen which is
    // picked up, put down and moved over the canvas.

    // This will not work if you use a different renderer,
    // such as WEBGL.
    cntxt = cnvs.elt.getContext('2d');

    cnvsLinGrad1 = new LinearGradient(cntxt, /* drawing context */
        0, /* start position x */
        0, /* start position y */
        width, /* stop position x */
        height, /* stop position y */
        ['#0088ff', '#00ff88'] /* an array of color stops */
    );

    angle = 0;
    angleIncr = 0.005;

    cnvsRadGrad1 = new RadialGradient(cntxt, /* drawing context */
        width * 0.5, /* start position x */
        height * 0.5, /* start position y */
        width * 0.25, /* inner radius */
        width * 0.5, /* stop position x */
        height * 0.5, /* stop position y */
        width * 0.65, /* outer radius */
        [color(127, 0, 255, 204), color(63, 0, 255, 54), color(255, 0, 127, 127), color(255, 255, 0, 255), color(255, 0, 0, 54)] /* an array of color stops */
    );

    // pts is will receive Perlin noise values for testing
    // radial gradient's shape.
    pts = [];
    count = 100;
    for (var i = 0; i < count; ++i) {
        pts.push(createVector(
            map(i, 0, count - 1, 0, width),
            map(noise(i / count), 0, 1, 0, height)
        ));
    }

    // While the above points will form the line plot,
    // these two are the anchor points at the bottom of
    // the graph.
    pts.push(createVector(width, height));
    pts.push(createVector(0, height));

    // The gradient of this div created by p5.dom is determined
    // by the css/style.css file.
    divLinGrad1 = createDiv('The quick brown fox jumps over the lazy dog.');
    divLinGrad1.id('divLinGrad1');

    cnvsPattern1 = new Pattern(cntxt, 'assets/fan.png', 'repeat');
}

function draw() {
    // Updating point values with 2-dimensional Perlin noise.
    for (var i = 0; i < count; ++i) {
        pts[i].y = map(noise(i / count, frameCount / 90.0), 0, 1, 0, height);
    }

    // Change the end angle of the arc.
    angle = angle >= TWO_PI ? 0 : angle + angleIncr;

    background(32);

    // Update the start and stop positions of the gradient based on the mouse's position.
    // cnvsLinGrad1.update(mouseX, mouseY, width - mouseX, height - mouseY);
    cnvsRadGrad1.update(mouseX, mouseY, map(cos(frameCount / 90.0), -1, 1, 10, height * 0.64), mouseX, mouseY, height * 0.65);

    // Draw shapes.
    cnvsLinGrad1.rect(0, 0, width, height);
    cnvsPattern1.arc(width * 0.5, /* x */
        height * 0.5, /* y */
        height * 0.425, /* radius */
        0, /* start angle */
        angle, /* end angle */
        64); /* stroke thickness */
    cnvsPattern1.text('MAGD 271', /* text */
        width * 0.5, /* x */
        height * 0.5, /* y */
        'bold 85px Helvetica', /* font-size and font */
        'center', /* horizontal alignment */
        'middle'); /* vertical alignment */
    cnvsRadGrad1.shape(pts);
}
