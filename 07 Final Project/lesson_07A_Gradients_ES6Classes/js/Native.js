'use strict';

// Ideally this class would be marked with the keyword 'abstract', meaning
// we wouldn't want to actually construct a Gradient object/instance, we'd
// only want to use it as a parent- or super-class which stores information
// shared by its children. ES6 doesn't seem to support 'abstract' yet.
class Native {

    // The concept of 'private' vs. 'public' variables is weak in JS,
    // but there is now support for getters and setters. The underscores
    // here indicate that these are intended to be internal and private
    // variables, which are accessed via the getters/setters below.
    constructor(cnt) {
        // Patterns, gradients and shapes created by the context in
        // vanilla JS. This means that this class and all those which
        // inherit from it _depend_ on the context, so a reference to it
        // is to be injected.
        this._cntxt = cnt;
        this._fill;
    }

    get fill() {
        return this._fill;
    }

    // JavaScript requires that setters have only one
    // parameter, meaning that you cannot, for example, do
    // set start(x, y) { this._start.set(x, y); }.
    set fill(g) {
        this._fill = g;
    }

    arc(x, y, radius, startAngle, endAngle, strokeWeight) {
        this._cntxt.beginPath();
        this._cntxt.lineCap = 'butt';
        this._cntxt.lineWidth = strokeWeight;
        this._cntxt.arc(x, y, radius, startAngle, endAngle);
        this._cntxt.strokeStyle = this._fill;
        this._cntxt.stroke();
        this._cntxt.closePath();
    }

    rect(x, y, w, h) {
        this._cntxt.fillStyle = this._fill;
        this._cntxt.fillRect(x, y, w, h);
    }

    shape(vs) {
        this._cntxt.beginPath();
        if (vs.length > 0) {
            this._cntxt.moveTo(vs[0].x, vs[0].y);
        } else {
            return;
        }
        for (var i = 1, size = vs.length; i < size; ++i) {
            this._cntxt.lineTo(vs[i].x, vs[i].y);
        }
        this._cntxt.closePath();
        this._cntxt.fillStyle = this._fill;
        this._cntxt.fill();
    }

    text(msg, x, y, style, haln, valn) {
        this._cntxt.font = style;
        this._cntxt.textAlign = haln;
        this._cntxt.textBaseline = valn;
        this._cntxt.fillStyle = this._fill;
        this._cntxt.fillText(msg, x, y);
    }
}
