'use strict';

class RadialGradient extends Gradient {
    constructor(cnt,
        startX, startY, startRad,
        stopX, stopY, stopRad,
        clrstops) {
        super(cnt, startX, startY, stopX, stopY, clrstops);
        this._radii = createVector(startRad, stopRad);
        this._fill = this._cntxt.createRadialGradient(
            startX, stopY, startRad,
            stopX, stopY, stopRad);
        for (var i = 0, size = this._colorStops.length; i < size; ++i) {
            this._fill.addColorStop(this._colorStops[i].step,
                clrstops[i].toString());
        }
    }

    get radii() {
        return this._radii;
    }

    set radii(v) {
        this._radii = v;
    }

    setRadii(inner, outer) {
        this._radii.set(inner, outer);
    }

    update(startX, startY, startRad,
        stopX, stopY, stopRad,
        cs) {
        this._start.set(startX, startY);
        this._stop.set(stopX, stopY);
        this._radii.set(startRad, stopRad);
        this._fill = this._cntxt.createRadialGradient(
            startX, startY, startRad,
            stopX, stopY, stopRad);
        if (cs === undefined) {
            for (var i = 0, size = this._colorStops.length; i < size; ++i) {
                this._fill.addColorStop(this._colorStops[i].step,
                    this._colorStops[i].color.toString());
            }
        } else {
            for (var i = 0, size = cs.length; i < size; ++i) {
                this._fill.addColorStop(map(i, 0, size - 1, 0, 1),
                    cs[i].toString());
            }
        }
    }
}
