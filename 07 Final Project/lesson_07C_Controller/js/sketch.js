/* The Gamepad API is relatively new in JavaScript. See
 * https://developer.mozilla.org/en-US/docs/Web/API/Gamepad_API/Using_the_Gamepad_API
 *
 * This script uses Xbox One Controller Driver for Mac OS, downloaded from
 * https://github.com/360Controller/360Controller/ . It only works under
 * the assumption that this hardware will be used with this driver.
 *
 * You should test your controller at http://www.html5gamepad.com before testing
 * with this code, as this is to demonstrate integrating the above with p5.js.
*/

'use strict';

var cnvs;
var pads = [];
var pc;

// Button and axis mappings are effectively dictionaries with entries
// consisting of Key-Value Pairs (KVPs). The list of keys ['A', 'B', ... ]
// can be retrieved using Object.keys, while the list of values [0, 1, ... ]
// can be retrieved using Object.values, but only in newer browsers.
var btnMappings;
var btnMappingsArr = [];
var axisMappings;
var axisMappingsArr = [];

if (navigator.userAgent.indexOf('Mac OS X') != -1) {
    pc = false;
    console.log("Attempting Mac configuration...");
    var btnMappings = {
        A: 0,
        B: 1,
        X: 2,
        Y: 3,
        LT: 4,
        RT: 5,
        LS_Click: 6,
        RS_Click: 7,
        Menu: 8,
        Back: 9,
        Guide: 10,
        Up: 12,
        Down: 13,
        Left: 14,
        Right: 15
    };
    // btnMappingsArr = Object.keys(btnMappings);
    var btnMappingsArr = ['A',
        'B',
        'X',
        'Y',
        'LT',
        'RT',
        'LS_Click',
        'RS_Click',
        'Menu',
        'Back',
        'Guide',
        'Up',
        'Down',
        'Left',
        'Right'
    ];

    // An enumeration to map numbers to axis names.
    var axisMappings = {
        LS_X: 0,
        LS_Y: 1,
        LT: 2,
        RS_X: 3,
        RS_Y: 4,
        RT: 5
    }
    // axisMappingsArr = Object.keys(axisMappings);
    var axisMappingsArr = ['LS_X',
        'LS_Y',
        'LT',
        'RS_X',
        'RS_Y',
        'RT'
    ];
} else {
    pc = true;
    console.log("Attempting PC configuration...");
    var btnMappings = {
        A: 0,
        B: 1,
        X: 2,
        Y: 3,
        LB: 4,
        RB: 5,
        LT: 6,
        RT: 7,
        Back: 8,
        Menu: 9,
        LS_Click: 10,
        RS_Click: 11,
        Up: 12,
        Down: 13,
        Left: 14,
        Right: 15
    };
    // btnMappingsArr = Object.keys(btnMappings);
    var btnMappingsArr = ['A',
        'B',
        'X',
        'Y',
        'LB',
        'RB',
        'LT',
        'RT',
        'Back',
        'Menu',
        'LS_Click',
        'RS_Click',
        'Up',
        'Down',
        'Left',
        'Right'
    ];

    // An enumeration to map numbers to axis names.
    var axisMappings = {
        LS_X: 0,
        LS_Y: 1,
        RS_X: 2,
        RS_Y: 3
    }
    // axisMappingsArr = Object.keys(axisMappings);
    var axisMappingsArr = ['LS_X',
        'LS_Y',
        'RS_X',
        'RS_Y'
    ];
}

// Variables to store DOM Elements.
var controllerDiagnosticDiv;
var controllerNameDiv;
var buttonsTable;
var axesTable;
var buttonPressed = [];
var buttonPressedDur = [];
var isPressedClr, isNotPressedClr;
var axesVal = [];

// Variables to represent 2-dimensional axes as ellipses.
var axis1Circ, axis2Circ, ltScl, rtScl;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);

    // Acquire relevant HTML DOM elements.
    controllerDiagnosticDiv = document.getElementById('controllerDiagnostic');
    controllerNameDiv = document.getElementById('controllerName');

    buttonsTable = document.getElementById('buttons');

    // To change the color of a DOM element's text when a button is pressed.
    isPressedClr = 'rgba(255, 127, 0, 1)';
    isNotPressedClr = 'rgba(255, 255, 255, 1)';

    axesTable = document.getElementById('axes');

    // Populate the buttons table.
    for (var i = 0, size = btnMappingsArr.length; i < size; ++i) {

        // Create row.
        var row = document.createElement('tr');

        // Create columns.
        var id = document.createElement('td');
        var name = document.createElement('td');
        buttonPressed.push(document.createElement('td'));
        buttonPressedDur.push(0);
        id.textContent = i;
        name.textContent = btnMappingsArr[i];
        buttonPressed[i].textContent = '0';
        buttonPressed[i].className = 'numerical';

        // Add columns to row.
        row.appendChild(id);
        row.appendChild(name);
        row.appendChild(buttonPressed[i]);

        // Add row to table.
        buttonsTable.appendChild(row);
    }

    // Populate the axes table.
    for (var j = 0, size = axisMappingsArr.length; j < size; ++j) {

        // Create row.
        var row = document.createElement('tr');

        // Create columns.
        var id = document.createElement('td');
        var name = document.createElement('td');
        axesVal.push(document.createElement('td'));
        id.textContent = j;
        name.textContent = axisMappingsArr[j];
        axesVal[j].textContent = '0';
        axesVal[j].className = 'numerical';

        // Add columns to row.
        row.appendChild(id);
        row.appendChild(name);
        row.appendChild(axesVal[j]);

        // Add row to table.
        axesTable.appendChild(row);
    }

    // Set the position of the circles to default values.
    axis1Circ = createVector(width * 0.5, height * 0.5);
    axis2Circ = createVector(width * 0.5, height * 0.5);

    // Set the scale of the circles to default values.
    ltScl = 50;
    rtScl = 50;

    background(32);
}

function draw() {
    noStroke();
    fill(32, 32, 32, 54);
    rect(0, 0, width, height);

    // kGamepads() returns an array, which may include null elements.
    pads = navigator.getGamepads();
    if (pads[0]) {
        controllerDiagnosticDiv.style.visibility = 'visible';
        // Update name of gamepad.
        controllerNameDiv.textContent = pads[0].id;

        // Update HTML for buttons.
        var isPressed = false;
        for (var i = 0, size = pads[0].buttons.length; i < size; ++i) {
            isPressed = pads[0].buttons[i].pressed;
            if (buttonPressed[i] && isPressed) {
                buttonPressed[i].style.color = isPressedClr;
                buttonPressedDur[i] += 1;
                buttonPressed[i].textContent = buttonPressedDur[i];
            } else if (buttonPressed[i]) {
                buttonPressed[i].style.color = isNotPressedClr;
                buttonPressedDur[i] = 0;
                buttonPressed[i].textContent = 0;
            }
        }

        // Update HTML for axes.
        for (var j = 0, size = pads[0].axes.length; j < size; ++j) {
            if (axesVal[j]) {
                axesVal[j].textContent = pads[0].axes[j].toFixed(2);
                if (pads[0].axes[j] > 0) {
                    axesVal[j].style.color = 'rgba(127, 255, 127, 1)';
                } else if (pads[0].axes[j] < 0) {
                    axesVal[j].style.color = 'rgba(255, 127, 127, 1)';
                } else {
                    axesVal[j].style.color = 'rgba(255, 255, 255, 1)';
                }
            }
        }

        // Left stick governs circle position.
        axis1Circ.set(
            map(pads[0].axes[axisMappings.LS_X], -1, 1, 0, width),
            map(pads[0].axes[axisMappings.LS_Y], -1, 1, 0, height)
        );

        // Left trigger governs circle scale.
        if (!pc) {
            ltScl = map(pads[0].axes[axisMappings.LT], -1, 1, 75, 200);
        }

        // Draw the circle.
        fill(255, 0, 127, 204);
        ellipse(axis1Circ.x, axis1Circ.y, ltScl, ltScl);

        // Right stick governs circle position.
        axis2Circ.set(
            map(pads[0].axes[axisMappings.RS_X], -1, 1, 0, width),
            map(pads[0].axes[axisMappings.RS_Y], -1, 1, 0, height)
        );

        // Right trigger governs circle scale.
        if (!pc) {
            rtScl = map(pads[0].axes[axisMappings.RT], -1, 1, 75, 200);
        }

        // Draw the circle.
        fill(127, 0, 255, 204);
        ellipse(axis2Circ.x, axis2Circ.y, rtScl, rtScl);
    } else {
        controllerDiagnosticDiv.style.visibility = 'hidden';
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
