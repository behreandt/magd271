class Bkg {
  constructor(path, z) {
    this.scl = createVector();
    this.pos = createVector();
    this.zIndex = z;

    // Translate a layer index into a 'depth' by which
    // the change in camera position is multiplied in draw.
    this.depthFactor = map(this.zIndex, 1, 8, 0.1, 0.95);

    // After the image is loaded, the display scale will
    // need to be determined. 'e' is the information returned
    // by the loadImage function, i.e., the p5.Image object.
    var rsz = function(e) {

      // We want the numerator of the aspect ratio
      // to be greater than the denominator so we can
      // increase the width by that amount.
      var w = e.width;
      var h = e.height;
      var longEdge = max(w, h);
      var shortEdge = min(w, h);
      var aspect = longEdge / shortEdge;
      this.scl.set(width * aspect, height);
      this.pos.set(width * 0.5, height * 0.5);
    }

    // Load the image then call the callback function, rsz,
    // defined above. The rsz function's context is bound
    // to this Bkg object being created so that the function
    // knows what we mean by 'this.scl' & 'this.pos'.
    this.img = loadImage(path, rsz.bind(this));
  }

  draw() {
    push();
    imageMode(CENTER);
    image(this.img,
      // Dampen the change in camera position by the depthFactor
      // so that 'closer' images will move more quickly than
      // more 'distant' images.
      (camera.position.x * this.depthFactor) - this.pos.x,
      this.pos.y,
      this.scl.x,
      this.scl.y);
    pop();
  }
}
