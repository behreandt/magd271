'use strict';

var cnvs;
var hillCount = 7;
var hillBkgs = [];
var groundImg, groundSprite;
var hero;
var defaultAnim;
var grounded;

function setup() {
  pixelDensity(displayDensity());
  cnvs = createCanvas(windowWidth, windowHeight);

  // Specify no smooth to ensure pixel art retains its quality
  // even after up-scaling.
  noSmooth();

  // A Bkg class is a wrapper around an image which performs the
  // calculations which simulate parallax.
  for (var i = 0; i < hillCount; ++i) {
    hillBkgs.push(new Bkg('assets/hills' + i + '.png', hillCount - i));
  }

  // Create the ground with which the avatar will collide.
  groundImg = loadImage('assets/ground.png');
  groundSprite = createSprite(width * 0.5, height - 64);
  groundSprite.addImage(groundImg);

  // p5.play is a nuissance when it comes to resizing sprites
  // and images. Scale up the sprite by n * 100%.
  groundSprite.scale = 2;
  groundSprite.friction = 0.97;

  // Create the player's avatar.
  hero = createSprite(width * 0.5, height * 0.5);
  defaultAnim = hero.addAnimation('default', 'assets/fr0.png', 'assets/fr1.png', 'assets/fr2.png', 'assets/fr3.png', 'assets/fr4.png', 'assets/fr5.png');

  // Lessen the speed of the animation.
  defaultAnim.frameDelay = 6;
  hero.friction = 0.98;
  hero.scale = 1.5;
}

function draw() {
  background(32);

  // Initiate p5.play's camera.
  camera.on();

  // Draw the parallaxing background.
  for (var i = 0; i < hillCount; ++i) {
    hillBkgs[i].draw();
  }

  // The hero is grounded if it collides with the ground sprite.
  grounded = hero.collide(groundSprite);

  // If the hero is not grounded, then add gravity to the hero
  // (90 degrees = straight down).
  if (!grounded) {
    hero.addSpeed(1, 90);
  }

  // If the A key is pressed, move West.
  if (keyIsDown(65)) {
    hero.addSpeed(0.1, 180);
  }

  // If the D key is pressed, move East.
  if (keyIsDown(68)) {
    hero.addSpeed(0.1, 0);
  }

  // If the W key is pressed, jump.
  if(keyIsDown(87) && grounded) {
    hero.addSpeed(25, 270);
  }

  // The camera follows the hero.
  camera.position.lerp(hero.position, 0.05);

  // Clamp the camera to the bounds of the world.

  // Clamp to minimum y.
  if (camera.position.y < -height * 0.5) {
    camera.position.y = -height * 0.5;
  }

  // Clamp to maximum y.
  if (camera.position.y > height * 0.5) {
    camera.position.y = height * 0.5;
  }

  // Clamp to minimum x.
  if(camera.position.x < -groundSprite.width * 0.5 / groundSprite.scale + width * 0.5) {
    camera.position.x = -groundSprite.width * 0.5 / groundSprite.scale + width * 0.5;
  }

  // Clamp to maximum x.
  if(camera.position.x > groundSprite.width * 0.5 / groundSprite.scale + width * 0.5) {
    camera.position.x = groundSprite.width * 0.5 / groundSprite.scale + width * 0.5;
  }

  drawSprites();
  camera.off();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
