'use strict';

Math.lerp = function(a, b, s) {
    return (1 - s) * a + s * b;
}

Math.map = function(v, l1, u1, l2, u2) {
    return l2 + (u2 - l2) * (v - l1) / (u1 - l1);
}

Math.randomRange = function(lb, ub) {
    if (ub) {
        return lb + (ub - lb) * Math.random();
    } else {
        return lb * Math.random();
    }
}
