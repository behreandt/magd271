'use strict';

class Widget {
    constructor(x, y, w, h, svg, id) {
        this._id = id;
        this._svg = svg.cloneNode(true);
        this._scl = new Vector(w, h);
        this._pos = new Vector(x, y);

        this._svg.setAttributeNS(null, 'class', 'widget');
        this._svg.id = 'widget' + id;
        this._svg.style.zIndex = -id;
        this._pos.apply(this._svg, 'left', 'top');
        this._scl.apply(this._svg, 'width', 'height');
        this._svg.style.transitionDuration = ((id + 1) * 3 / count) + 's';

        // Set the colors of this widget.
        this._lightColor = new Color(Math.map(id, 0, count - 1, Widget.hueSeed, 360), Widget.satSeed, 33.7, 1);
        this._darkColor = this._lightColor.copy();
        this._darkColor.l /= 2;
        this._lightColor.apply(this._svg.getElementById('lightBody'), 'fill');
        this._darkColor.apply(this._svg.getElementById('darkBody'), 'fill');
        this._lightColor.complement().apply(this._svg.getElementById('lightUnder'), 'fill');
        this._darkColor.complement().apply(this._svg.getElementById('darkUnder'), 'fill');
        document.body.appendChild(this._svg);
    }

    update(v) {
        // While you could create lerp or other easing functions,
        // it's not necessary in this case because CSS has already
        // taken care of them (linear, ease-in, ease-out, etc.).
        this._pos = (v ? v : new Vector());
        this._pos.apply(this._svg, 'left', 'top');

        this._lightColor.h += 0.1;
        this._darkColor.h += 0.1;
        this._lightColor.a = Math.map(Math.cos(frameCount / 90.0), -1, 1, 0.75, 1);
        this._darkColor.a = Math.map(Math.sin(frameCount / 90.0), -1, 1, 0.75, 1);
        this._lightColor.apply(this._svg.getElementById('lightBody'), 'fill');
        this._darkColor.apply(this._svg.getElementById('darkBody'), 'fill');
        this._lightColor.complement().apply(this._svg.getElementById('lightUnder'), 'fill');
        this._darkColor.complement().apply(this._svg.getElementById('darkUnder'), 'fill');
    }
}

Widget.hueSeed = Math.random() * 360;
Widget.satSeed = 75 + Math.random() * 25;
