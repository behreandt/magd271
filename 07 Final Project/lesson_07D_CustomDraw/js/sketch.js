'use strict';

var frameCount, paused;

// HTML DOM elements to be acquired by JS in setup.
var frameCounter;
var pauseDiv, pos;

// Variable to store SVG retrieved by HTTP Request.
var svg;

var widgets = [], count, following;
var mouse = new Vector(window.innerWidth * 0.5, window.innerHeight * 0.5);

// When the mouse is moved, update its position.
window.onmousemove = updateMouse;

// When all the HTML on index.html is finished loading,
// then call the function setup defined below.
window.onload = setup;

function setup() {
    frameCount = 0;
    paused = false;
    count = 20;
    following = false;

    // Acquire SVG through an HTTP request. Create a function
    // to process the results of the request.
    var request = new XMLHttpRequest();
    request.onreadystatechange = processResults;
    request.open('GET', 'assets/tentacle.svg', true);
    request.send();

    // Acquire HTML DOM elements by ID.
    frameCounter = document.getElementById('frameCounter');
    pauseDiv = document.getElementById('pauseDiv');
    pos = new Vector(window.innerWidth * 0.5, window.innerHeight * 0.5);

    // Add a click function.
    pauseDiv.onclick = function(e) {

        // Turn paused to its opposite.
        paused = !paused;

        // If paused is false, then call a new frame.
        if (!paused) {
            window.requestAnimationFrame(draw);
            this.style.width = '150px';
            this.style.height = '150px';
            this.style.opacity = 0.85;
        } else {
            this.style.width = '75%';
            this.style.height = '75%';
            this.style.opacity = 0.5;
        }
    }

    document.body.onclick = function(e) {
        following = !following;
    }

    // Begin the request animation frame at the conclusion of setup.
    window.requestAnimationFrame(draw);
}

function draw(e) {
    // console.log(e);

    // Update the frame count, display it in an HTML element.
    frameCount++;
    frameCounter.textContent = frameCount;


    // Increase the roundedness of the div until it looks like a circle.
    pauseDiv.style.borderRadius = ((Math.sin(frameCount / 30.0) + 1) * 50) + "%";

    pos.set(
        Math.map(Math.cos(frameCount / 90), -1, 1, 0, 100),
        Math.map(Math.sin(frameCount / 120), -1, 1, 0, 100)
    );
    pos.apply(pauseDiv, 'top', 'left', '%');

    // At the conclusion of frame, the draw function supplies itself to
    // the request animation frame to make the loop infinite.
    if (!paused) {

        if (following) {
            for (var i = 0; i < count; ++i) {
                widgets[i].update(mouse);
            }
        } else {
            for (var i = 0; i < count; ++i) {
                var r = Math.map(Math.cos(frameCount / 120.0), -1, 1, 10, 20);
                var t = frameCount / 90.0 + Math.map(i, 0, count, 0, Math.PI * 2);
                if(widgets[i]) widgets[i].update(new Vector(
                    window.innerWidth * 0.5 + Math.cos(t) * i * r,
                    window.innerHeight * 0.5 + Math.sin(t) * i * r
                ));
            }
        }

        window.requestAnimationFrame(draw);
    } else {

        // Send the widgets to a random location on screen when paused.
        for (var i = 0; i < count; ++i) {
            widgets[i].update(Vector.random(0, 0, window.innerWidth, window.innerHeight));
        }
    }
}

function processResults() {

    // See https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState , https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
    if (this.readyState == 4 && this.status == 200) {

        // Load the response into the svg variable.
        svg = this.responseXML.documentElement;

        // Create widget based on SVG.
        var shortEdge = Math.min(window.innerWidth, window.innerHeight);
        var w = shortEdge / 5;
        var h = w;
        var x = 0;
        var y = 0;
        for (var i = 0; i < count; ++i) {
            h *= 0.925;
            w *= 0.925;
            x = Math.randomRange(w, window.innerWidth - w);
            y = Math.randomRange(h, window.innerHeight - h);
            widgets.push(new Widget(x, y, w, h, svg, i));
        }
    }
}

function updateMouse(e) {
    mouse.set(e.clientX, e.clientY);
}
