'use strict';

var c, v, hp, gold, keys, name,
  spot, deg = 0;

// Assign event listeners to the browser window.
window.onload = setup;
window.onkeydown = register;

function setup() {

  // Create random values.
  hp = parseInt(Math.randomRange(99, 1000));
  gold = parseInt(Math.randomRange(500, 1000));
  keys = parseInt(Math.randomRange(1, 4));
  name = "";

  // Session storage lasts as long as the browser is open. It exists
  // as a set of key-value pairs.
  sessionStorage.hp = hp;
  sessionStorage.gold = gold;
  sessionStorage.keys = keys;

  // Display the results on the webpage.
  document.getElementById('hpValue').textContent = hp;
  document.getElementById('goldValue').textContent = gold;
  document.getElementById('keysValue').textContent = keys;
  document.getElementById('nameInput').onchange = function(e) {
    sessionStorage.name = name = e.srcElement.value;
  }

  // If the browser cookie contains x, y, z information, then
  // create a vector from it.
  v = Vector.fromCookie();
  if (v.x === 0 && v.y === 0) {
    v = Vector.random(0, 0, window.innerWidth, window.innerHeight);
  }

  spot = document.getElementById('spot');

  // Resize the spot to 1/4th the browser window's short edge.
  var scl = Math.min(window.innerWidth, window.innerHeight);
  spot.style.width = spot.style.height = scl + 'px';

  // Apply the vector's x and y component to the spot.
  v.apply(spot, 'left', 'top');

  spot.style.visibility = 'display';
  spot.style.opacity = 1;

  // Create a random color.
  c = Color.randomRange(0, 360, 50, 100, 25, 50);

  // Write the color to the browser's cookie.
  c.toCookie();

  // Apply the random color to the body's background.
  c.apply(document.body, 'background-color');

  // Get a complementary color to the random color.
  var opp = c.complement();
  opp.l = 100 - opp.l;
  // opp.l = Math.map(opp.l, 0, 100, 90, 10);
  var s = opp.s;
  for (var i = 0; i < 6; ++i) {
    opp.s = Math.map(i, 0, 6, s, 25);
    opp.h += 15;
    var paths = spot.getElementsByClassName('st' + i);
    for (var j = 0, size = paths.length; j < size; ++j) {
      opp.apply(paths[j], 'fill');
    }
  }
}

function register(e) {
  if (e.code === 'ArrowUp') {
    v.y -= window.innerHeight * 0.125;
    deg -= 30;
  }
  if (e.code === 'ArrowDown') {
    v.y += window.innerHeight * 0.125;
    deg += 30;
  }
  if (e.code === 'ArrowLeft') {
    v.x -= window.innerWidth * 0.125;
    deg -= 30;
  }
  if (e.code === 'ArrowRight') {
    v.x += window.innerWidth * 0.125;
    deg += 30;
  }
  v.apply(spot, 'left', 'top');
  spot.style.transform = 'translate(-50%, -50%) rotate(' + deg + 'deg)';
  v.toCookie();
}
