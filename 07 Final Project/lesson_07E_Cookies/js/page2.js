'use strict';

window.onload = setup;

function setup() {
  // If sessionStorage contains a key, then grab an HTML element and set
  // its text content to the result.
  if (sessionStorage.hp) {
    document.getElementById('hpValue').textContent = sessionStorage.hp;
  }

  if (sessionStorage.keys) {
    document.getElementById('keysValue').textContent = sessionStorage.keys;
  }

  if (sessionStorage.gold) {
    document.getElementById('goldValue').textContent = sessionStorage.gold;
  }

  if (sessionStorage.name) {
    document.getElementById('nameValue').textContent = sessionStorage.name;
  }

  var c = Color.fromCookie();

  c.apply(document.body, 'background-color');
}
