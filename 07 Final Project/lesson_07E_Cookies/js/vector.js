'use strict';

class Vector {
  constructor(x, y) {
    this._x = x ? x : 0;
    this._y = y ? y : 0;
  }

  get x() {
    return this._x;
  }

  set x(x) {
    this._x = x;
  }

  get y() {
    return this._y;
  }

  set y(y) {
    this._y = y;
  }

  set(x, y) {
    this._x = x;
    this._y = y;
  }

  toCookie() {
    document.cookie = 'x=' + this._x.toFixed(2);
    document.cookie = 'y=' + this._y.toFixed(2);
    return document.cookie;
  }

  apply(elt, prop1, prop2, unit) {
    elt.style.setProperty(prop1, this._x + (unit ? unit : 'px'));
    elt.style.setProperty(prop2, this._y + (unit ? unit : 'px'));
  }
}

Vector.random = function(xmin, ymin, xmax, ymax) {
  return new Vector(Math.randomRange(xmin, xmax), Math.randomRange(ymin, ymax));
}

Vector.fromCookie = function() {
  var result = new Vector();
  var str = document.cookie.split(/;\s?/);
  for (var i = 0, size = str.length, kvp; i < size; ++i) {
    kvp = str[i].split(/=/);
    if (kvp[0] === 'x') {
      result.x = parseFloat(kvp[1]);
    } else if (kvp[0] === 'y') {
      result.y = parseFloat(kvp[1]);
    }
  }
  return result;
}
