'use strict';

class Color {
  constructor(h = 0, s = 100, l = 50, a = 1) {
    this._h = h;
    this._s = s;
    this._l = l;
    this._a = a;
  }

  toString() {
    return 'hsla(' + this._h.toFixed(2) +
      ', ' + this._s.toFixed(2) +
      '%, ' + this._l.toFixed(2) +
      '%, ' + this._a.toFixed(1) +
      ')';
  }

  toCookie() {
    document.cookie = 'h=' + this._h.toFixed(2);
    document.cookie = 's=' + this._s.toFixed(2);
    document.cookie = 'l=' + this._l.toFixed(2);
    document.cookie = 'a=' + this._a.toFixed(1);
    return document.cookie;
  }

  get h() {
    return this._h;
  }

  set h(h) {
    this._h = h;
  }

  get s() {
    return this._s;
  }

  set s(s) {
    this._s = s;
  }

  get l() {
    return this._l;
  }

  set l(l) {
    this._l = l;
  }

  get a() {
    return this._a;
  }

  set a(a) {
    this._a = a;
  }

  set(h, s, l, a) {
    this._h = h % 360;
    this._s = s;
    this._l = l;
    this._a = a ? a : 1;
  }

  copy() {
    return new Color(this._h, this._s, this._l, this._a);
  }

  complement() {
    return new Color(this._h + 180, this._s, this._l, this._a);
  }

  apply(elt, property) {
    elt.style.setProperty(property, this.toString());
  }
}

Color.random = function() {
  return new Color(Math.random() * 360,
    Math.random() * 100,
    Math.random() * 100,
    1);
}

Color.randomRange = function(hMin, hMax, sMin, sMax, lMin, lMax) {
  return new Color(Math.randomRange(hMin, hMax),
    Math.randomRange(sMin, sMax),
    Math.randomRange(lMin, lMax),
    1);
}

Color.triad = function(h, s, l, a) {
  return [new Color(h, s, l, a),
    new Color(h + 120, s, l, a),
    new Color(h + 240, s, l, a)
  ];
}

Color.fromCookie = function() {
  var result = new Color();

  // Split the cookie string into entries by searching
  // for a semicolon followed by any number of spaces
  // (this search criterion uses regular expressions).
  var str = document.cookie.split(/;\s?/);
  for (var i = 0, size = str.length, kvp; i < size; ++i) {

    // Split each entry into a key-value pair by
    // equal sign.
    kvp = str[i].split(/=/);
    if (kvp[0] === 'h') {
      result._h = parseFloat(kvp[1]);
    } else if (kvp[0] === 's') {
      result._s = parseFloat(kvp[1]);
    } else if (kvp[0] === 'l') {
      result._l = parseFloat(kvp[1]);
    } else if (kvp[0] === 'a') {
      result._a = parseFloat(kvp[1]);
    }
  }
  return result;
}
