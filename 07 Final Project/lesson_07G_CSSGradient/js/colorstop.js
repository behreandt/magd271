class ColorStop {
  constructor(perc, clr) {
    this._percent = perc;
    this._color = clr;
  }

  set percent(v) {
    this._percent = v;
  }

  get percent() {
    return this._percent;
  }

  set color(clr) {
    this._color = clr % 1.0;
  }

  get color() {
    return this._color;
  }

  setColor(h = 0, s = 100, l = 50, a = 1) {
    this._color.set(h, s, l, a);
  }

  toString() {
    return this._color.toString() + " " + (this._percent * 100) + "%";
  }
}
