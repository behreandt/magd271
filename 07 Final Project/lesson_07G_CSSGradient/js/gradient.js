class Gradient {
  constructor(stps, type = 'linear-gradient', a = 0) {
    this._angle = a % 360;
    this._stops = stps;
    this._type = type;
    this._shape = 'circle';
  }

  get angle() {
    return this._angle;
  }

  set angle(v) {
    this._angle = v % 360;
  }

  get type() {
    return this._type;
  }

  set type(v) {
    this._type = v;
  }

  get shape() {
    return this._shape;
  }

  set shape(v) {
    if (v === 'ellipse' || v === 'circle') {
      this._shape = v;
    }
  }

  length() {
    return this._stops.length;
  }

  getColorStop(i) {
    if (i >= this._stops.length) {
      return null;
    }
    return this._stops[i];
  }

  toString() {
    var result = "";
    if (this._type === 'radial-gradient') {
      result = 'radial-gradient(' + this._shape + ', ';
    } else {
      result = 'linear-gradient(' + this._angle.toFixed(2) + 'deg, ';
    }
    for (var i = 0, size = this._stops.length; i < size; ++i) {
      result += this._stops[i].toString();
      if (i < size - 1) {
        result += ', ';
      }
    }
    result += ')';
    return result + ', white';
  }

  apply(elt, property = 'background') {
    if (elt.nodeName === 'svg') {
      // var children = elt.getElementsByTagName('linearGradient');
      // console.log(children);
      // for (var i = 0, size1 = children.length; i < size1; ++i) {
      //
      //   // Remove old stops.
      //   // while (children[i].childNodes.length > 1) {
      //   //   children[i].removeChild(children[i].firstChild);
      //   // }
      //
      //   for (var j = 0, size2 = this._stops.length; j < size2; ++j) {
      //     var stop = document.createElement('stop');
      //     stop.setAttribute('offset', this._stops[j].percent);
      //     stop.setAttribute('stop-color', this._stops[j]._color.toString());
      //     children[i].appendChild(stop);
      //   }
      // }
    } else {
      elt.style.setProperty(property, this.toString());
    }
  }

}
