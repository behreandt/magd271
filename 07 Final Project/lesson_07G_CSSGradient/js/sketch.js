'use strict';

var frameCount = 0;
var sky = null;
var silhouette = null;
var pinwheel = null;
var x = 0;
var y = 0;
var w = 0;
var h = 0;
var incr = 1;
var grad = null;

window.onload = setup;
window.onresize = resize;
window.onmousemove = changeAngle;
window.onclick = changeType;

function setup() {
  window.requestAnimationFrame(draw);
  sky = document.getElementById('sky');
  pinwheel = document.getElementById('pinwheel');

  var h = Math.random() * 360;
  grad = new Gradient([new ColorStop(0, new Color(h, 100, 75, 1)),
      new ColorStop(0.5, new Color(h + 180, 100, 75, 1)),
      new ColorStop(0.75, new Color(h + 90, 100, 75, 1)),
      new ColorStop(1, new Color(h + 270, 100, 75, 1))
    ],
    'linear-gradient', /* Gradient type */
    0); /* Initial angle */

  grad.apply(pinwheel);
}

function draw(e) {
  frameCount++;
  window.requestAnimationFrame(draw);
  for (var i = 0, size = grad.length(); i < size; ++i) {
    var n = (i + 1) / size;
    grad.getColorStop(i).color.h += incr;
    grad.getColorStop(i).color.l = Math.map(Math.cos((frameCount * n) / 60.0), -1, 1, 10, 90);
  }
  grad.apply(sky);
}

function resize(e) {}

function changeAngle(e) {
  grad.angle = Math.map(e.clientX, 0, window.innerWidth, 0, 360);
  incr = Math.map(e.clientY, 0, window.innerHeight, 0, 2);
}

function changeType(e) {
  if (grad.type === 'linear-gradient') {
    grad.type = 'radial-gradient';
  } else {
    grad.type = 'linear-gradient';
  }
}
