'use strict';

Math.lerp = function(a, b, s) {
    return (1 - s) * a + s * b;
}

Math.map = function(v, l1, u1, l2, u2) {
    return l2 + (u2 - l2) * (v - l1) / (u1 - l1);
}

Math.clamp = function(v, min = 0, max = 1) {
  return Math.min(Math.max(v, min), max);
};

Math.randomRange = function(lb, ub) {
    if (ub) {
        return lb + (ub - lb) * Math.random();
    } else {
        return lb * Math.random();
    }
}

// See https://bost.ocks.org/mike/shuffle/ ,
// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
Array.prototype.shuffle = function() {
  var m = this.length, t, i;
  while (m) {
    i = Math.floor(Math.random() * m--);
    t = this[m];
    this[m] = this[i];
    this[i] = t;
  }
  return this;
}

String.random = function(len) {
  var result = "";
  for(var i = 0; i < len; ++i) {
    result += String.fromCharCode(parseInt(Math.randomRange(97, 123)));
  }
  return result;
}
