'use strict';

class Color {
  constructor(h = 0, s = 100, l = 50, a = 1) {
    this._h = h % 360;
    this._s = s;
    this._l = l;
    this._a = a;
  }

  toString() {
    return 'hsla(' + this._h.toFixed(2) + ', ' +
      this._s.toFixed(2) + '%, ' +
      this._l.toFixed(2) + '%, ' +
      this._a.toFixed(2) + ')';
  }

  get h() {
    return this._h;
  }

  set h(h) {
    this._h = h % 360;
  }

  get s() {
    return this._s;
  }

  set s(s) {
    this._s = s % 101;
  }

  get l() {
    return this._l;
  }

  set l(l) {
    this._l = l % 101;
  }

  get a() {
    return this._a;
  }

  set a(a) {
    this._a = a;
  }

  set(h = 0, s = 100, l = 50, a = 1) {
    this._h = h % 360;
    this._s = s;
    this._l = l;
    this._a = a;
  }

  copy() {
    return new Color(this._h, this._s, this._l, this._a);
  }

  complement() {
    return new Color(this._h + 180, this._s, this._l, this._a);
  }

  apply(elt, property) {
    elt.style.setProperty(property, this.toString());
  }
}

Color.random = function() {
  return new Color(Math.random() * 360,
    Math.random() * 100,
    Math.random() * 100,
    1);
}

Color.triad = function(h, s, l, a) {
  return [new Color(h, s, l, a),
    new Color(h + 120, s, l, a),
    new Color(h + 240, s, l, a)
  ];
}
