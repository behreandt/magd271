'use strict';

var fan,
    x, y, w, h,
    highlights, midtones, shadows,
    highlightColor1, midtoneColor1, shadowColor1,
    highlightColor2, midtoneColor2, shadowColor2;

// Window onload is the equivalent of setup.
window.onload = function() {

    // p5.js's design philosophy is to as much as possible avoid the use of objects
    // to make it easier for beginning programmers. However, much of the functionality
    // of p5 is available in the native Javascript Math library.
    w = h = Math.min(window.innerWidth, window.innerHeight);
    x = (window.innerWidth * 0.5) - (w * 0.5);
    y = (window.innerHeight * 0.5) - (h * 0.5);

    // Acquire a JavaScript reference to the fan SVG element.
    fan = document.getElementById('fan');

    // Center and scale the fan.
    dynamicPosition(fan);

    // Acquire a JavaScript references to the paths from the SVG by their class.
    highlights = fan.getElementsByClassName('highlight');
    midtones = fan.getElementsByClassName('midtone');
    shadows = fan.getElementsByClassName('shadow');

    // Colors for when the mouse is hovering over the path.
    highlightColor1 = new Clr(255, 0, 166, 1);
    midtoneColor1 = new Clr(255, 85, 0, 1);
    shadowColor1 = new Clr(169, 255, 0, 1);

    // Colors for whe the mouse is not hovering over the path.
    highlightColor2 = new Clr(255, 129, 211, 1);
    midtoneColor2 = new Clr(255, 169, 129, 1);
    shadowColor2 = new Clr(212, 255, 129, 1);

    initializePaths(highlights, highlightColor1, highlightColor2, 3);
    initializePaths(midtones, midtoneColor1, midtoneColor2, 3);
    initializePaths(shadows, shadowColor1, shadowColor2, 1.5);
}

function initializePaths(p,  c1, c2, animDelay) {
    for (var i = 0, size = p.length; i < size; ++i) {

        // Set initial color.
        p[i].style.fill = c2.toString();

        // Saturate on mouse over.
        p[i].onmouseover = function() {
            this.style.fill = c1.toString();
        }

        // Desaturate on mouse out.
        p[i].onmouseout = function() {
            this.style.fill = c2.toString();
        }

        // On mouse click, toggle between rotation and scaling.
        p[i].onclick = function() {
            if (this.style.animationName === 'rot') {
                this.style.animationName = 'scaleIn';
                this.style.animationDuration = '3s';
                this.style.animationDelay = animDelay + 's';
                this.style.animationDirection = 'alternate-reverse';
            } else {
                this.style.animationName = 'rot';
                this.style.animationDuration = '12s';
                this.style.animationDelay = '0s';
                this.style.animationDirection = 'normal';
            }
        }
    }
}

// If the browser window is resized, any calculations dependent
// on the window's width, such as the SVG's size and position,
// will have to be redone.
window.onresize = function() {
    dynamicPosition(fan);
}

function dynamicPosition(elt) {
    w = h = Math.min(window.innerWidth, window.innerHeight);
    x = (window.innerWidth * 0.5) - (w * 0.5);
    y = (window.innerHeight * 0.5) - (h * 0.5);
    elt.style.position = 'absolute';
    elt.style.width = w + 'px';
    elt.style.height = h + 'px';
    elt.style.left = x + 'px';
    elt.style.top = y + 'px';
}

class Clr {
    constructor(r, g, b, a) {
        this._r = r;
        this._g = g;
        this._b = b;
        this._a = a;
    }

    toString() {
        return 'rgba(' + this._r + ',' + this._g + ',' + this._b + ',' + this._a + ')';
    }
}
