# Final Project

![Final Project](preview.png)

### Due Dates
[Thursday, May 11, 2017](http://www.uww.edu/Documents/registrar/Schedule_of_Classes/Spring_2017/2017%20Spring%20Exam%20Schedule.pdf)
S01 7:45 a.m.
S02 10:00 a.m.

### Philosophy

The final project is the culmination of the goals and philosophy of the course, as outlined in the syllabus.

### Directions

* Work either individually or in groups of up to 4 people.
* Select from the following development possibilities:
    * Processing
    * JavaScript libraries that facilitate animation and interactivity in HTML5 canvas. For example,
        * [p5.js](https://p5js.org/)
        * [anime.js](http://anime-js.com/)
        * [matter.js](http://brm.io/matter-js/)
        * [mo.js](http://mojs.io/)
        * [three.js](https://threejs.org/)
    * Scalable Vector Graphics (.svg) animated with SMIL, JS or CSS3.
* (Optional) Peripherals are available for loan to interested groups:
    * [MaKey MaKey](http://www.makeymakey.com/) Deluxe Kit
    * [SparkFun Arduino 101 Kit](https://www.sparkfun.com/products/13844)
    * [Leap Motion](https://www.leapmotion.com/) (1x)
* Write a design document.
* Develop the project so as to reflect the key concerns of the course:
    * Animation
    * Interactivity
    * Organization of code and assets
* Present the project to the class during the final exam period.

### Inspiration

* [Dan Shiffman - The Coding Train](https://www.youtube.com/user/shiffman/videos)
* [Processing Exhibition](https://processing.org/exhibition/)
* [OpenProcessing](https://www.openprocessing.org/browse/?viewBy=most&filter=favorited#)
* [Generative Design](http://www.generative-gestaltung.de/code)
* [Form+Code](http://formandcode.com/code-examples/)
* [W3 Schools](https://www.w3schools.com/)
* [Codepen](https://codepen.io)
* [CSS-Tricks](https://css-tricks.com)

### Rubric

Points | Description
------ | -----------
100 | The project is presented to the class during the final exam period.
75 | The project demonstrates a facility with topics covered in previous course projects: aesthetic quality, animation, interaction, organization and efficiency in scripting and asset management.
50 | The project has implemented at least four key features outlined in the design document.
25 | The project demonstrates that all group members have learned the skills they planned to acquire in the design document.
25 | The project runs without error.
25 | All files are contained in a folder, which is named according to the following convention: s17_magd271_projectname_lastnames. The folder has been compressed into a .zip file and upload to D2L.
300 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). For this assignment there will be no time remaining to resubmit should students fail to meet this standard.__
