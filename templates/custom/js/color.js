'use strict';

class Color {
  constructor(h, s, l, a) {
    this._h = h ? h % 360 : 0;
    this._s = s ? s : 100;
    this._l = l ? l : 100;
    this._a = a ? a : 1;
  }

  toString() {
    return 'hsla(' + this._h + ',' + this._s + '%,' + this._l + '%,' + this._a + ')';
  }

  get h() {
    return this._h;
  }

  set h(h) {
    this._h = h % 360;
  }

  get s() {
    return this._s;
  }

  set s(s) {
    this._s = s;
  }

  get l() {
    return this._l;
  }

  set l(l) {
    this._l = l;
  }

  get a() {
    return this._a;
  }

  set a(a) {
    this._a = a;
  }

  set(h, s, l, a) {
    this._h = h % 360;
    this._s = s;
    this._l = l;
    this._a = a ? a : 1;
  }

  copy() {
    return new Color(this._h, this._s, this._l, this._a);
  }

  complement() {
    return new Color(this._h + 180, this._s, this._l, this._a);
  }

  apply(elt, property) {
    elt.style.setProperty(property, this.toString());
  }
}

Color.random = function() {
  return new Color(Math.random() * 360,
    Math.random() * 100,
    Math.random() * 100,
    1);
}

Color.triad = function(h, s, l, a) {
  return [new Color(h, s, l, a),
    new Color(h + 120, s, l, a),
    new Color(h + 240, s, l, a)
  ];
}
