'use strict';

class Vector {
  constructor(x, y) {
    this._x = x ? x : 0;
    this._y = y ? y : 0;
  }

  get x() {
    return this._x;
  }

  set x(x) {
    this._x = x;
  }

  get y() {
    return this._y;
  }

  set y(y) {
    this._y = y;
  }

  set(x, y) {
    this._x = x;
    this._y = y;
  }

  apply(elt, prop1, prop2, unit) {
    elt.style.setProperty(prop1, this._x + (unit ? unit : 'px'));
    elt.style.setProperty(prop2, this._y + (unit ? unit : 'px'));
  }
}

Vector.random = function(xmin, ymin, xmax, ymax) {
  return new Vector(Math.randomRange(xmin, xmax), Math.randomRange(ymin, ymax));
}
