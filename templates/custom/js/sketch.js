'use strict';

var frameCount = 0;
var canvas = null;
var context = null;

window.onload = setup;
window.onresize = resize;

function setup() {
    canvas = document.createElement('canvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    context = canvas.getContext('2d');
    // If you want to disable right mouse click menu on the canvas.
    // canvas.oncontextmenu = function(e) {return false;};
    document.body.appendChild(canvas);
    window.requestAnimationFrame(draw);
}

function draw(e) {
    // console.log(e);
    frameCount++;
    window.requestAnimationFrame(draw);
}

function resize(e) {
    // console.log(e);
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}
