'use strict';

// Critical variables to store the WebGL renderer, scene and camera.
var rndr, scene, cam;

// Lights
var directional;

// Geometry
var geom;

// Material
var standard;

// Mesh = Geometry + Material
var mesh;

// Wait until the browser has loaded the HTML document, then
// perform this function on completion of load. This is
// the rough equivalent of void setup() or void Start().
window.onload = setup;
window.onresize = adjust;

function setup() {

    // Set up the renderer, including size.
    rndr = new THREE.WebGLRenderer({
        antialias: true
    });

    // For high-density screens.
    rndr.setPixelRatio(window.devicePixelRatio);

    // Set to the size of the browser window.
    rndr.setSize(window.innerWidth, window.innerHeight);

    // Equivalent to Processing's background();
    rndr.setClearColor(0x202020);

    // Create a scene.
    scene = new THREE.Scene();

    // Create a camera, set its position.
    cam = new THREE.PerspectiveCamera(50, /* Field of View */
        window.innerWidth / window.innerHeight, /* Aspect */
        0.1, /* Near Plane */
        1000); /* Far Plane */
    cam.position.z = 500;

    // Lights.
    directional = new THREE.DirectionalLight(0xfff5cd, 0.5);
    directional.position.set(0.5, 0.75, 0.75);

    // Geometry.
    geom = new THREE.SphereGeometry(100, 128, 128);

    // Materials.
    standard = new THREE.MeshStandardMaterial({ color: 0x0088ff });

    // Mesh = Geometry + Material.
    mesh = new THREE.Mesh(geom, standard);
    mesh.position.set(0, 0, 0);

    // Add items to the scene.
    scene.add(directional);
    scene.add(mesh);

    // Add the renderer canvas to the document body.
    document.body.appendChild(rndr.domElement);

    // If you want to disable right mouse click menu on the canvas.
    rndr.domElement.oncontextmenu = function(e) {return false;};

    // Begin the draw loop.
    draw();
}

function draw() {
    // These two following lines are necessities.
    requestAnimationFrame(draw);
    rndr.render(scene, cam);
}

// When the browser window is resized, update the camera aspect
// and projection, adjust the renderer size.
function adjust() {
    cam.aspect = window.innerWidth / window.innerHeight;
    cam.updateProjectionMatrix();
    rndr.setSize(window.innerWidth, window.innerHeight);
}
