'use strict';

var cnvs = null;
var cntxt = null;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    cntxt = cnvs.elt.getContext('2d');

    // If you want to disable right mouse click menu on the canvas.
    // cnvs.elt.oncontextmenu = function(e) {return false;};

    background(64);
}

function draw() {
    background(32);
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
