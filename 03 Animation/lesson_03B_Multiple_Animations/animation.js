function Animation(name, imageFiles) {
    this.current = 0;
    this.adv = 1;
    this.frames = [];
    // The variable imageFiles passed into the Animation
    // constructor is intended to be an array of strings
    // containing the path and filename of the images.
    // These are used to load the image and push the
    // resulting image onto the frames array.
    for (var i = 0; i < imageFiles.length; ++i) {
        this.frames.push(loadImage(imageFiles[i]));
    }
    // this.interval = 12;
    this.name = name;
    this.oscillate = true;
    // This is called 'offset' and not 'position' because,
    // as animation is further organized, and it comes
    // to belong to an actor, the actor's position in space
    // should determine the animation's position.
    this.offset = createVector(0, 0);
    this.pivot = CENTER;
    this.scale = createVector(64, 64);
}

Animation.prototype.draw = function() {
    this.advance();
    push();
    imageMode(this.pivot);
    image(this.frames[this.current],
        this.offset.x, this.offset.y,
        this.scale.x, this.scale.y);
    pop();
}

Animation.prototype.advance = function() {
    // if(frameCount % this.interval == 0) {
    if (this.oscillate) {
        this.current += this.adv;
        if (this.current == 0 || this.current >= this.frames.length - 1) {
            this.adv *= -1;
        }
    } else {
        this.current = (this.current + this.adv) % this.frames.length;
    }
    // }
}
