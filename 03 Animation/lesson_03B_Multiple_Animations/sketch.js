'use strict';

var cnvs, idle, act;

function preload() {
    idle = new Animation("Idle", ["data/idle1.png", "data/idle2.png", "data/idle3.png", "data/idle4.png"]);
    act = new Animation("Action", ["data/action1.png", "data/action2.png", "data/action3.png", "data/action4.png"]);
}

function setup() {
    cnvs = createCanvas(420, 420);
    pixelDensity(displayDensity());
    background(64);
    frameRate(12);

    // The spatial offset of the animation isn't set in preload,
    // as the size of the sketch has not yet been established.
    idle.offset = createVector(width / 2.0, height / 2.0);
    idle.oscillate = true;
    act.offset = createVector(width / 2.0, height / 2.0);
}

function draw() {
    background(32);
    if (keyIsPressed) {
        act.draw();
    } else {
        idle.draw();
    }
}
