## Animation

![Animation](preview.png)

### Due Date
~~Thursday, February 23rd, 2017~~
Tuesday, February 28th, 2017
S01 9:30 a.m.
S02 11:00 a.m.

### Philosophy

Constructing animation must be considered from the standpoint of both the coder and the visual designer. Some aspects of animation, such as translation, rotation and scale may be effected via script, and thus will be implemented by the coder. Furthermore, to accomplish some of the 12 traditional principles of animation outlined below, the coder may be obliged to simulate certain physical forces. Other aspects of animation, such as drawing cels for a walk cycle, are the purview of the visual designer. However the animation is achieved, whether in 2D or 3D, the crucial component for this assignment is to invite the user or player into a meaningful interaction with the animation in a scene.

### Directions

* Form a group of 2 - 3 people.
* Create a design drawing on paper.
* Migrate from Processing (based on Java) to p5 (based on JavaScript).
    * Download or link to the [p5 library](https://p5js.org/download/).
    * Use a text editor such as [Atom](https://atom.io/), [Brackets](http://brackets.io), Dreamweaver or [Sublime Text](https://www.sublimetext.com/) to edit code.
    * Reference [Daniel Shiffman's tutorials](https://www.youtube.com/user/shiffman/playlists) for particulars on working in p5.js.
    * Students are free to use any example code provided here and are furthermore encouraged to explore the [p5 play library](http://p5play.molleindustria.org/) as an addition or alternative.
    * For coders, a few reminders when migrating to p5:
        * JavaScript is loosely-typed, meaning there is no `int`, `float` or `boolean`, only `var`. As a consequence functions do not need to specify the data type of parameters or of returned information.
        * While Java adheres to an Object-Oriented philosophy, JavaScript does not. There is no inheritance (`class foo extends bar`). There are, however, prototypes which behave like objects. [Object literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types#Object_literals) can also be created to package data.
        * JavaScript compensates for the lack of a [collections library](https://docs.oracle.com/javase/7/docs/api/java/util/Collections.html) by having a very flexible [array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array).
        * A few important differences specific to Processing/p5:
            * `console.log("Hello World!");` instead of `println("Hello World!");`
            * `createCanvas(420, 420);` instead of `size(420, 420);`
            * `var v = createVector(x, y, z);` instead of `PVector v = new PVector(x, y, z);`
            * `push();` and `pop();` combine `pushMatrix();` with `pushStyle();` and `popMatrix();` with `popStyle();`
            * The [keyIsDown](http://p5js.org/reference/#/p5/keyIsDown) function obviates the need for a key listener.
        * `preload` precedes `setup` and is the place to load media assets. The security of many browsers, however, prevent this operation from a local HTML file. Either use Mozilla Firefox, which does not have such security, or create a local host in terminal (Mac) or command prompt (PC). See [Shiffman](https://youtu.be/UCHzlUiDD10) for more.
* Create an animated scene, observing the 12 principles of animation.
    * Squash & Stretch
    * Anticipation
    * Staging
    * Straight-Ahead & Pose-to-Pose
    * Follow-Through & Overlap
    * Slow-In & Slow-Out
    * Arcs
    * Secondary Action
    * Timing
    * Exaggeration
    * Solid Drawings
    * Appeal

### Inspiration

* [Cento Lodigiani - The Illusion of Life](https://vimeo.com/93206523)
* [Ken Perlin - Beyond Animation](https://youtu.be/O9b-rtrcPEA?t=8m20s)
* [Extra Frames - Squash & Stretch in Jak & Daxter](https://youtu.be/HYP7rJni1vc)
* [Extra Frames - Anticipation in Monster Hunter](https://youtu.be/DtmHYwVODIU)
* [Paul Robertson - Cube Race](https://youtu.be/aeoeb-l5dLU)
* [Paul Robertson - Super Dino Boys](https://youtu.be/n8yAfHU-0JI)
* [Felix Sputnik - Quadruped Locomotion Tutorial](https://vimeo.com/202205778)
* [Lorn - Anvil](https://vimeo.com/181706731)
* [Sparni un airi - Wings and Oars](https://vimeo.com/155696373)
* [Vitaliy Shushko - X-Story](https://vimeo.com/190513423)

### Rubric

Points | Description
------ | -----------
25 | The animations are composed with the 12 principles of animation in mind.
15 | A drawing has been made on paper to plan the design. It has been turned in on the day of the submission deadline. This could show rough sketches of an animation, brainstorming, diagrams for code, etc.
10 | There are at least three animations. (E.g., one actor could have three animation states, or three actors could each have one animation state.)
10 | At least one animation state changes in response to user input (e.g., an idle animation switches to a walking animation on key press).
10 | The project has been created by a group. Include the names of group members in a comment if only one person is submitting.
10 | The code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps.
10 | The webpage runs without errors.
10 | All files are contained in a folder, which is named according to the following convention: s17_magd271_assignment3_lastnames. The folder has been compressed into a .zip file and upload to D2L.
100 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
