'use strict';

// In Java, you may be accustomed to the need for a Timer
// class when working with duration and interval. One is
// provided here, but note that JavaScript as a base programming
// language is much more accessible than Java and furthermore has
// the built-in functions setTimeout for invoking a behavior
// once after n milliseconds delay, setInterval for invoking
// a behavior repeatedly every n milliseconds. The
// corresponding clearTimeout and clearInterval to cease
// those behaviors. See
// http://www.w3schools.com/jsref/met_win_settimeout.asp ,
// http://www.w3schools.com/jsref/met_win_setinterval.asp ,
// http://www.w3schools.com/jsref/met_win_cleartimeout.asp
// and http://www.w3schools.com/jsref/met_win_clearinterval.asp
// for more information.

// To work with time on a larger scale, see the Date object:
// http://www.w3schools.com/js/js_dates.asp .

var cnvs, t1, t2,
    x, y, w, h,
    pendulum, a, stpFll, runFll, fll,
    cntrlTmrDelay, cntrlStopTriggered,
    blinkSmoothing;

function setup() {
    cnvs = createCanvas(windowWidth, windowHeight);
    pixelDensity(displayDensity());
    background(64);
    textAlign(LEFT, TOP);
    textSize(16);
    noStroke();

    t1 = new Timer("Controlled");

    // JavaScript permits you to create anonymous functions which have no
    // name. It also allows you to pass those functions as arguments into
    // other functions. This means that you can create a timer function that
    // checks to see if it should perform a behavior at a certain time if
    // that behavior is defined. You can then decide on what that behavior
    // should be later.
    t2 = new Timer("Auto-Timer", 5, function() {
            console.log("Auto-Timer Custom Start Function! " + millis());
        },
        10,
        function() {
            console.log("Auto-Timer Custom Stop Function! " + millis());
        });
    x = width / 2.0;
    y = height / 2.0;
    w = h = 100;
    a = 0;
    stpFll = color(255, 127, 0);
    runFll = color(0, 127, 255);
    fll = color(255, 127, 0);
    pendulum = false;
    cntrlTmrDelay = 5;
    cntrlStopTriggered = false;
    blinkSmoothing = 0.01;
}

function draw() {
    t1.tick();
    t2.tick();

    if (pendulum) {
        x = width * 0.5 + sin(a) * width * 0.35;
        a += 0.05;
        fll = lerpColor(fll, runFll, 0.05);
    } else {
        fll = lerpColor(fll, stpFll, 0.05);
    }

    background(32);
    if (cntrlStopTriggered) {
        fill(64);
        ellipse(width * 0.5, height * 0.5, height, height);
    }
    fill(0, 0, 0, (cos(frameCount * blinkSmoothing) + 1) * 255);
    ellipse(width * 0.5, height * 0.5, height * 0.5, height * 0.5)
    fill(fll);
    ellipse(x, y, w, h);
    fill(255);
    text("millis: " + round(millis()) +
        "\nframeCount: " + frameCount +
        "\n" + new Date().toLocaleTimeString() +
        "\ntimer delay: " + cntrlTmrDelay +
        "\nblink smoothing: " + blinkSmoothing.toFixed(3), 10, 10);
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function keyPressed() {
    if (key == ' ') {
        t1.srt(cntrlTmrDelay, function() {
            console.log("Controlled stop function.");
            cntrlStopTriggered = !cntrlStopTriggered;
        });
    }
}

function keyReleased() {
    if (keyCode == LEFT_ARROW && cntrlTmrDelay > 0) {
        cntrlTmrDelay--;
    } else if (keyCode == RIGHT_ARROW) {
        cntrlTmrDelay++;
    } else if (keyCode == DOWN_ARROW && blinkSmoothing > 0) {
        blinkSmoothing -= 0.001;
    } else if (keyCode == UP_ARROW && blinkSmoothing < 1) {
        blinkSmoothing += 0.001;
    }

}

function mousePressed() {
    // After 3 seconds (3000 milliseconds), invoke a function which
    // flips the pendulum switch.
    setTimeout(function() {
        pendulum = !pendulum;
    }, 3000);
}
