'use strict';

function Timer(nm, n1, strtFn, n2, stopFn) {
    this.name = nm === undefined ? "Timer" : nm;
    this.reset();
    if (n1 !== undefined) {
        this.scheduleStart(n1, strtFn);
        if (n2 !== undefined && n2 > n1) {
            this.scheduleStop(n2, stopFn);
        }
    }
}

Timer.prototype.elapsed = function() {
    return this.stop - this.start;
}

Timer.prototype.reset = function() {
    this.start = 0;
    this.stop = 0;
    this.scheduledStart = 0;
    this.scheduledStop = 0;
    this.started = false;
    this.stopped = false;
    this.startScheduled = false;
    this.stopScheduled = false;
    this.startFnctn = undefined;
    this.stopFnctn = undefined;
}

Timer.prototype.toggle = function() {
    if (this.started && this.stopped) {
        this.reset();
        return this.srt();
    } else if (this.started && !this.stopped) {
        return this.stp();
    } else if (!this.started && !this.stopped) {
        return this.srt();
    }
    return -1;
}

Timer.prototype.srt = function(n, fnc) {
    if (!this.started && !this.stopped) {
        this.start = millis();
        // this.start = new Date().getTime();
        this.started = true;
        this.startScheduled = false;
        if (this.startFnctn !== undefined) {
            this.startFnctn();
        }
        if (n !== undefined) {
            this.scheduleStop(n, fnc);
        }
    } else if (this.started && this.stopped) {
        this.reset();
        this.start = millis();
        // this.start = new Date().getTime();
        this.started = true;
        this.startScheduled = false;
        if (n !== undefined) {
            this.scheduleStop(n, fnc);
        }
    }
    return this.start;
}

Timer.prototype.stp = function(fnc) {
    if (this.started && !this.stopped) {
        this.stop = millis();
        // this.stop = new Date().getTime();
        this.stopped = true;
        this.stopScheduled = false;
        if (fnc !== undefined) {
            fnc();
        }
    }
    this.elapsed();
    return this.stop;
}

Timer.prototype.tick = function() {
    // var c = new Date().getTime();
    if (!this.started && this.startScheduled && millis() >= this.scheduledStart) {
    // if (!this.started && this.startScheduled && c >= this.scheduledStart) {
        this.srt();
    }
    if (!this.stopped && this.stopScheduled && millis() >= this.scheduledStop) {
    // if (!this.stopped && this.stopScheduled && c >= this.scheduledStop) {
        this.stp(this.stopFnctn);
    }
}

Timer.prototype.scheduleStart = function(n, fnc) {
    if (!this.started && !this.stopped) {
        this.scheduledStart = millis() + (n * 1000);
        // this.scheduledStart = new Date().getTime() + (n * 1000);
        this.startScheduled = true;
        this.startFnctn = fnc;
        console.log(this.name + " start scheduled for " + this.scheduledStart + ".");
    }
    return this.scheduledStart;
}

Timer.prototype.scheduleStop = function(n, fnc) {
    if (!this.stopped) {
        this.scheduledStop = millis() + (n * 1000);
        // this.scheduledStop = new Date().getTime() + (n * 1000);
        this.stopScheduled = true;
        this.stopFnctn = fnc;
        console.log(this.name + " stop scheduled for " + this.scheduledStop + ".");
    }
    return this.scheduledStop;
}
