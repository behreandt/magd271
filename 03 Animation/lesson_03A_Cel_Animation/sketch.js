'use strict';

var cnvs, animation, currentFrame1,
    interval1, currentFrame2, interval2,
    dir, x1, y1, w1, h1,
    x2, y2, w2, h2;

function preload() {
    // Animation is here conceived of as an array of still images.
    animation = [];
    // Push loaded images onto the array.
    for (var i = 0; i < 6; ++i) {
        animation.push(loadImage("data/fr0" + (i + 1) + ".png"));
    }
}

function setup() {
    cnvs = createCanvas(420, 420);
    pixelDensity(displayDensity());
    background(64);
    imageMode(CENTER);

    // Animation version 1.
    currentFrame1 = 0;
    interval1 = 50;
    x1 = width * 0.25;
    y1 = height * 0.5;
    w1 = 64;
    h1 = 64;

    // Animation version 2.
    currentFrame2 = 0;
    interval2 = 25;
    dir = 1;
    x2 = width * 0.75;
    y2 = height * 0.5;
    w2 = 128;
    h2 = 128;
}

function draw() {
    background(32);

    // Animation version 1.
    image(animation[currentFrame1], x1, y1, w1, h1);
    // Every time the frame count is cleanly divisible by an interval,
    // then advance the current frame by 1. If the current frame has
    // reached the last frame in the array, then the modulo will assure
    // that the current frame returns to the 0th element of the array.
    if (frameCount % interval1 == 0) {
        currentFrame1 = (currentFrame1 + 1) % animation.length;
    }

    // Animation version 2.
    image(animation[currentFrame2], x2, y2, w2, h2);
    // It is likely beneficial to your aesthetic and work-flow to establish
    // a universal frame rate which will apply to all animations that you create,
    // so you can set frameRate(n); and thus dispense with this if conditional
    // every frame, but just in case, it is a useful technique to know of.
    if(frameCount % interval2 == 0) {
        currentFrame2 += 1 * dir;
    }
    // Unlike version 1 which always traveled through the frames in the
    // same direction and then reset to 0 when it reached the end, version
    // 2 ping-pongs when it reaches either the lower- or upper-bound of the
    // array of frames, hence the dir variable being multiplied by -1.
    if(currentFrame2 <= 0 || currentFrame2 >= animation.length - 1) {
      dir *= -1;
    }
}
