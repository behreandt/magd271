'use strict';

// 1. Be sure to include the link to the p5.play.js file in your index.html file.
// 2. Use examples for p5.play as a reference:
// http://p5play.molleindustria.org/examples/index.html?fileName=collisions3.js

var cnvs, actr, ground, isGrounded, jumpForce, walkForce, gForce;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    background(64);

    // 3. createSprite(x, y, w, h);
    actr = createSprite(width / 2.0, height / 2.0, 128, 128);

    // 4. Add an animation to the sprite. Note that this returns to image 0 after
    //    reaching the last frame of animation.
    var anim = actr.addAnimation("idle", "data/greenGuy01.png", "data/greenGuy02.png", "data/greenGuy03.png", "data/greenGuy04.png",
        "data/greenGuy03.png",
        "data/greenGuy02.png");

    // Optional: Adjust the frame rate for the animation.
    anim.frameDelay = 8;

    // 4. Add a collider to the sprite.
    actr.setCollider("circle", 0, 0, 64);

    // Optional: Adjust the bounciness of the actor, which here is called restitution.
    actr.restitution = 0.01;
    actr.friction = 0.99;

    // 5. For the ground, a sprite sheet may eventually be more practicable.
    var groundTexture = loadImage("data/ground.png");
    ground = new Group();
    for(var i = 0; i < width / 128; ++i) {
      var g = createSprite(64 + i * 128, height - 64);
      g.addImage("groundTexture", groundTexture);
      g.immovable = true;
      g.friction = 0.99;
      ground.add(g);
    }

    isGrounded = false;
    jumpForce = 4;
    walkForce = 0.0375;
    gForce = 0.05;
}

function draw() {
    background(32);

    // 5. Draw the sprites.
    drawSprites();

    // Optional: If you'd like to see what the collider looks like at mouse press.
    actr.debug = mouseIsPressed;

    // Add gravity to the actor.
    actr.velocity.y += gForce;

    // Establish collision and bouncing between actor and ground.
    isGrounded = ground.collide(actr);
    actr.bounce(ground);

    // Add velocity to the actor if the arrow keys are pressed.
    if(keyIsDown(LEFT_ARROW)) {
      actr.velocity.x -= walkForce;
    }
    if(keyIsDown(RIGHT_ARROW)) {
      actr.velocity.x += walkForce;
    }
    if(isGrounded && keyIsDown(UP_ARROW)) {
      console.log("UP");
      actr.velocity.y -= jumpForce;
    }

    // Reset the actor to the opposite edge of the screen if they go off.
    if(actr.position.x - 64 > width) {
      actr.position.x = 0;
    } else if(actr.position.x + 64 < 0) {
      actr.position.x = width;
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
