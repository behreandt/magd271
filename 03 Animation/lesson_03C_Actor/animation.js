function Animation(name, imageFiles) {
    this.current = 0;
    this.adv = 1;
    this.frames = [];
    for (var i = 0; i < imageFiles.length; ++i) {
        this.frames.push(loadImage(imageFiles[i]));
    }
    this.interval = 24;
    this.name = name;
    this.oscillate = true;
    this.offset = createVector(0, 0);
    this.scale = createVector(1, 1);
}

Animation.prototype.draw = function() {
    this.advance();
    image(this.frames[this.current],
        this.offset.x, this.offset.y,
        this.scale.x, this.scale.y);
}

Animation.prototype.advance = function() {
    if (frameCount % this.interval == 0) {
        if (this.oscillate) {
            this.current += this.adv;
            if (this.current == 0 || this.current >= this.frames.length - 1) {
                this.adv *= -1;
            }
        } else {
            this.current = (this.current + this.adv) % this.frames.length;
        }
    }
}
