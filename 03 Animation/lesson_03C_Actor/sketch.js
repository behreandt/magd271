'use strict';

var cnvs, hero, idle, act;

function preload() {
    idle = new Animation("Idle", ["data/idle1.png", "data/idle2.png", "data/idle3.png", "data/idle4.png"]);
    act = new Animation("Action", ["data/action1.png", "data/action2.png", "data/action3.png", "data/action4.png"]);
}

function setup() {
    cnvs = createCanvas(windowWidth, windowHeight);
    pixelDensity(displayDensity());
    background(64);
    hero = new Actor();
    hero.current = hero.idle = idle;
    hero.action = act;
}

function draw() {
    background(32);
    hero.draw();
    hero.screenWrap();
}

// When the browser window has been resized, this function
// resizes the canvas.
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
