'use strict';

var cnvs, gl, hero, idle, act, cam, sky, ground;

function preload() {
    idle = new Animation("Idle", ["data/idle1.png", "data/idle2.png", "data/idle3.png", "data/idle4.png"]);
    act = new Animation("Action", ["data/action1.png", "data/action2.png", "data/action3.png", "data/action4.png"]);

    sky = loadImage("data/sky.png");
    ground = loadImage("data/ground.png");
}

function setup() {
    cnvs = createCanvas(windowWidth, windowHeight, WEBGL);
    // SOURCE & REFERENCE: http://learningwebgl.com/blog/?p=859
    gl = cnvs.GL;
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable(gl.BLEND);
    gl.disable(gl.DEPTH_TEST);
    pixelDensity(displayDensity());
    background(64);
    hero = new Actor();
    hero.current = hero.idle = idle;
    hero.action = act;
    cam = new Cam();
}

function draw() {
    background(32);
    cam.draw(hero.position);
    drawSky();
    hero.draw();
    drawGround();

    if(keyIsPressed){
      if(key == 'S' || key == 's') {
        saveCanvas('preview', 'png');
      }
    }
}

function drawSky() {
    push();
    translate(0, 0, 0);
    texture(sky);
    plane(750, 750);
    pop();
}

function drawGround() {
    push();
    translate(0, height / 2.0 + 40, 0);
    texture(ground);
    plane(750, 150)
    pop();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
