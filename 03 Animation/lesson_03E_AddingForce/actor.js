// SOURCE & REFERENCE:
// Dan Shiffman, Nature of Code Chapter 2: Forces
// http://natureofcode.com/book/chapter-2-forces/
// https://youtu.be/MkXoQVWRDJs?list=PLRqwX-V7Uu6ZRrqLcQ5BkBKmBLiGD8n4O

function Actor() {
    this.position = createVector(0, 0, 0);
    // Speed and direction of movement to add to position.
    this.velocity = createVector(0, 0, 0);
    // Increase in speed to add to velocity.
    this.acceleration = createVector(0, 0, 0);
    // Bounciness of the actor upon hitting the ground, where
    // 0 means a complete stop and 1 means a rebound with no
    // loss of energy.
    this.bounciness = 0.25;
    // The amount of frictional force influencing the actor.
    // this.fricForceGround = 0.9;
    // The amount of gravitational force influencing the actor.
    // Ignores mass of the actor.
    this.gravForce = createVector(0, 1, 0);
    // The point upon which a downward force is no longer
    // applied to an actor and it rebounds according to its
    // bounciness.
    this.ground = height / 2.0;
    // A variable to record how long the player has been grounded.
    this.isGrounded = 0;
    // The amount of power applied when the actor jumps.
    this.jumpPower = 18;
    // Mass of the actor, by which any force is divided. V/M
    this.mass = 1;
    // The scale of the actor, which is impacted by clicking the down arrow.
    this.scaleDuck = createVector(64, 32, 1);
    this.scaleNormal = createVector(64, 64, 1);
    this.scale = this.scaleNormal.copy();
    this.scaleRate = 0.02;
    this.scaleRestore = 0.1;
    // Amount of energy to invest in walking, whether that be
    // to the left or right.
    this.walkPower = 5;

    // Animation states.
    this.current;
    this.action;
    this.idle;
}

Actor.prototype.draw = function() {
    this.animate();
    this.update();
    this.move();
    push();
    noStroke();
    imageMode(CENTER);
    translate(this.position.x, this.position.y, this.position.z);
    scale(this.scale.x, this.scale.y, this.scale.z);
    this.current.draw();
    pop();
}

// Changes animation state.
Actor.prototype.animate = function() {
    if (this.isGrounded <= 0) {
        this.current = this.action;
    } else {
        this.current = this.idle;
    }
}

// Performs physics calculations every frame.
Actor.prototype.update = function() {
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
    this.gravity();
}

// Add forces to the actor.
Actor.prototype.addForce = function(v) {
    this.acceleration.add(p5.Vector.div(v, this.mass));
}

// Handles all user-input related to the actor.
Actor.prototype.move = function() {
    if (keyIsDown(LEFT_ARROW) && !keyIsDown(DOWN_ARROW)) {
        // this.addForce(createVector(-this.walkPower, 0, 0));
        this.position.x -= this.walkPower;
    }

    if (keyIsDown(RIGHT_ARROW) && !keyIsDown(DOWN_ARROW)) {
        // this.addForce(createVector(this.walkPower, 0, 0));
        this.position.x += this.walkPower;
    }

    if(this.isGrounded > 0) {
      // Jump.
      if(keyIsDown(UP_ARROW) && !keyIsDown(DOWN_ARROW)) {
        this.addForce(createVector(0, -this.jumpPower, 0));
      }

      // Duck (reduce the height of the actor).
      if(keyIsDown(DOWN_ARROW)) {
        this.scale.lerp(this.scaleDuck, this.scaleRate);
      } else {
        this.scale.lerp(this.scaleNormal, this.scaleRestore);
      }
    }
}

// Called by update.
Actor.prototype.gravity = function() {
    // Gravity is an applied force, but it does not consider mass.
    this.acceleration.add(this.gravForce);
    // Check for collision with the ground, taking into account the
    // vertical scale of the actor.
    if (this.position.y > this.ground - this.scale.y) {
        // The rebound of a bouncy object upon hitting the ground
        // is simulated by this multiplication of the velocity's y
        // component by -1 multiplied by a dampening percentage.
        this.velocity.y *= -this.bounciness;
        this.position.y = this.ground - this.scale.y;
        this.isGrounded++;
    } else {
        this.isGrounded = 0;
    }
}
