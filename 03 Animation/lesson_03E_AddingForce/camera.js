function Cam() {
    this.panSmoothing = 0.025;
    this.position = createVector(0, 0, 0);
}

Cam.prototype.draw = function(focus) {
    // JavaScript does not support function overloading; to accomplish
    // a similar effect, test to see if arguments are null and supply
    // a default behavior.
    if(focus !== null) {
      this.position.lerp(focus, this.panSmoothing);
    }
    orbitControl();
    camera(this.position.x, this.position.y, this.position.z);
}
