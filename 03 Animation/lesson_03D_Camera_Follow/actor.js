function Actor() {
    this.position = createVector(0, 0, 0);
    this.moveSpeed = 2.5;
    this.rotation = 0;
    this.rotSpeed = 0.01;
    this.scale = createVector(64, 64, 1);
    this.current;
    this.action;
    this.idle;
}

Actor.prototype.draw = function() {
    this.animate();
    this.move();
    push();
    noStroke();
    imageMode(CENTER);
    translate(this.position.x, this.position.y, this.position.z);
    // Since we are working in 2.5D, we can use rotateZ.
    rotateZ(this.rotation);
    scale(this.scale.x, this.scale.y, this.scale.z);
    this.current.draw();
    pop();
}

Actor.prototype.animate = function() {
    if (keyIsDown(UP_ARROW) || keyIsDown(DOWN_ARROW)) {
        this.current = this.action;
    } else {
        this.current = this.idle;
    }
}

// Because y+ is up in WEBGL and y- is down, the direction
// of rotation is reversed to counter-clockwise.
Actor.prototype.move = function() {
    if (keyIsDown(UP_ARROW)) {
        this.position.x += cos(this.rotation) * this.moveSpeed;
        this.position.y -= sin(this.rotation) * this.moveSpeed;
    }

    if (keyIsDown(LEFT_ARROW)) {
        this.rotation += this.rotSpeed;
    }

    if (keyIsDown(DOWN_ARROW)) {
        this.position.x -= cos(this.rotation) * this.moveSpeed;
        this.position.y += sin(this.rotation) * this.moveSpeed;
    }

    if (keyIsDown(RIGHT_ARROW)) {
        this.rotation -= this.rotSpeed;
    }
}

Actor.prototype.screenWrap = function() {
    if (this.position.x > width) {
        this.position.x = 0;
    }
    if (this.position.x < 0) {
        this.position.x = width;
    }
    if (this.position.y > height) {
        this.position.y = 0;
    }
    if (this.position.y < 0) {
        this.position.y = height;
    }
}
