'use strict';

var cnvs, gl, hero, idle, act, cam, bkg;

function preload() {
    idle = new Animation("Idle", ["data/idle1.png", "data/idle2.png", "data/idle3.png", "data/idle4.png"]);
    act = new Animation("Action", ["data/action1.png", "data/action2.png", "data/action3.png", "data/action4.png"]);

    bkg = loadImage("data/bkg.png");
}

function setup() {
    cnvs = createCanvas(windowWidth, windowHeight, WEBGL);
    // By default, if the textures we use contain any transparency or alpha,
    // they will not be recognized by the WEBGL renderer when we place them
    // onto a 3D surface. We need to do the following for the transparency.
    // SOURCE & REFERENCE: http://learningwebgl.com/blog/?p=859
    gl = cnvs.GL;
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable(gl.BLEND);
    gl.disable(gl.DEPTH_TEST);
    pixelDensity(displayDensity());
    background(64);
    hero = new Actor();
    hero.current = hero.idle = idle;
    hero.action = act;
    cam = new Cam();
}

function draw() {
    background(32);
    cam.draw(hero.position);
    drawFloor();
    hero.draw();
}

function drawFloor() {
    push();
    translate(0, 0, 0);
    texture(bkg);
    plane(750, 750);
    pop();
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
