function Cam() {
    this.position = createVector(0, 0, 0);
    this.panSmoothing = 0.025;
}

Cam.prototype.draw = function(focus) {
    this.position.lerp(focus, this.panSmoothing);
    camera(this.position.x, this.position.y, this.position.z);
}
