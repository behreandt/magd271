## Extra Credit

### Due Date
Thursday, May 5th, 2017
S01 9:30 a.m.
S02 11:00 a.m.

### Possibilities

* Attend and respond to an event significant to new media art practice or video game development. This might include an art exhibit or talk, theatrical or musical performance, video game tournament or con.

* The response could be emphasize creativity or analysis or both.
    * Analytical responses should be relevant to the concerns of the course, e.g., animation, interaction, design philosophy, development. Concision and clarity of thought are more important than page count.
        * Cite any sources used.
    * For creative responses to an event, please speak with me in person before undertaking an extra credit assignment.

* Create a browser-based interactive animation using a library other than p5. Examples would be
    * [anime.js](http://anime-js.com/)
    * [d3.js](https://d3js.org/)
    * [matter.js](http://brm.io/matter-js/)
    * [mo.js](http://mojs.io/)
    * [snap.svg](http://snapsvg.io/)
    * [three.js](https://threejs.org/)

### Logistics

* Name the submission according to the following convention: s17_magd271_extraCredit_yourlastname.ext, where '.ext' is the file extension, depending on the format.
    * If necessary, compress the submission into a .zip file and upload to D2L.
    * If submitting a Processing sketch, remember that all additional code and media files need to be included in the folder which is to be compressed into a zip file.
    * If submitting an HTML page, remember to either link to CDNs or include the libraries upon which your code relies.

### Rubric

Points | Description
------ | -----------
100 | Maximum possible.

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). Work which falls into this category will not be accepted for extra credit.__
