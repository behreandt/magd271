'use strict';

// Variables to control the ambient light.
var ambv1, ambv2, ambv3, ambx1, amby1, ambz1,
  ambv4, ambv5, ambv6, ambx2, amby2, ambz2;

// Variables to control the point light.
var ptv1, ptv2, ptv3, ptx1, pty1, ptz1,
  ptv4, ptv5, ptv6, ptx2, pty2, ptz2;

// Variables to control the directional light.
var dirv1, dirv2, dirv3, dirx1, diry1, dirz1,
  dirv4, dirv5, dirv6, dirx2, diry2, dirz2;

var a, rate;

function setup() {
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    // Initialize variables that control ambient light.
    ambv4 = ambv1 = random(0, 156);
    ambv5 = ambv2 = random(0, 156);
    ambv6 = ambv3 = random(0, 156);
    ambx2 = ambx1 = random(-width / 2.0, width / 2.0);
    amby2 = amby1 = random(-height / 2.0, height / 2.0);
    ambz2 = ambz1 = 0;

    // Initialize variables that control point light.
    ptv4 = ptv1 = random(0, 156);
    ptv5 = ptv2 = random(0, 156);
    ptv6 = ptv3 = random(0, 156);
    ptx2 = ptx1 = random(-width / 2.0, width / 2.0);
    pty2 = pty1 = random(-height / 2.0, height / 2.0);
    ptz2 = ptz1 = 0;

    // Initialize variables that control directional light.
    dirv4 = dirv1 = random(0, 80);
    dirv5 = dirv2 = random(0, 80);
    dirv6 = dirv3 = random(0, 80);

    // Unlike the other lights, the directional light doesn't
    // have a spatial position, only normals which are oriented
    // relative to positive and negative x, y, z axes.
    dirx2 = dirx1 = random(-1, 1);
    diry2 = diry1 = random(-1, 1);
    dirz2 = dirz1 = random(-1, 1);

    a = 0;
    rate = 100;
}

function draw() {
    background(32);

    // Create the lights using values for color and position.
    ambientLight(ambv1, ambv2, ambv3, ambx1, amby1, ambz1);
    pointLight(ptv1, ptv2, ptv3, ptx1, pty1, ptz1);
    directionalLight(dirv1, dirv2, dirv3, dirx1, diry1, dirz1);

    // Draw the sphere.
    push();
    translate(-width * 0.4, 0, -height / 2.0);
    specularMaterial(127, 127, 127);
    sphere(height / 2.5);
    pop();

    // Draw the box.
    push();
    translate(width * 0.4, 0, -height / 2.0);
    specularMaterial(127, 127, 127);
    rotateX(a);
    rotateY(a);
    box(height / 2.5);
    pop();

    a+= 0.01;

    // Every so often,
    if (frameCount % rate == 0) {
      // Set a new destination for the ambient light.
      ambx2 = random(-width / 2.0, width / 2.0);
      amby2 = random(-height / 2.0, height / 2.0);
      ambz2 = random(-5.0, 5.0);

      // Set a new destination for the spot light.
      ptx2 = random(-width / 2.0, width / 2.0);
      pty2 = random(-height / 2.0, height / 2.0);
      ptz2 = random(-5.0, 5.0);

      // Set a new destination for the directional light.
      dirx2 = random(-1.0, 1.0);
      diry2 = random(-1.0, 1.0);
      dirz2 = random(-1.0, 1.0);

      // Set new colors.
      ambv4 = random(0, 156);
      ambv5 = random(0, 156);
      ambv6 = random(0, 156);

      ptv4 = random(0, 156);
      ptv5 = random(0, 156);
      ptv6 = random(0, 156);

      dirv4 = random(0, 80);
      dirv5 = random(0, 80);
      dirv6 = random(0, 80);
    }

    // Move from current position to destination.
    ambx1 = lerp(ambx1, ambx2, 0.05);
    amby1 = lerp(amby1, amby2, 0.05);
    ambz1 = lerp(ambz1, ambz2, 0.05);

    ptx1 = lerp(ptx1, ptx2, 0.05);
    pty1 = lerp(pty1, pty2, 0.05);
    ptz1 = lerp(ptz1, ptz2, 0.05);

    dirx1 = lerp(dirx1, dirx2, 0.05);
    diry1 = lerp(diry1, diry2, 0.05);
    dirz1 = lerp(dirz1, dirz2, 0.05);

    // Move from current color to target.
    ambv1 = lerp(ambv1, ambv4, 0.05);
    ambv2 = lerp(ambv2, ambv5, 0.05);
    ambv3 = lerp(ambv3, ambv6, 0.05);

    ptv1 = lerp(ptv1, ptv4, 0.05);
    ptv2 = lerp(ptv2, ptv5, 0.05);
    ptv3 = lerp(ptv3, ptv6, 0.05);

    dirv1 = lerp(dirv1, dirv4, 0.05);
    dirv2 = lerp(dirv2, dirv5, 0.05);
    dirv3 = lerp(dirv3, dirv6, 0.05);
}
