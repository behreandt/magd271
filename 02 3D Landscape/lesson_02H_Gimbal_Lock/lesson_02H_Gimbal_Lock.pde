float tx, ty, tz;

void setup() {
  size(640, 420, P3D);
  surface.setResizable(true);
  surface.setTitle("Gimbal Lock");
  pixelDensity(displayDensity());
  background(64);
  ellipseMode(RADIUS);
  tx = 0;
  ty = 0;
  tz = 0;
}

void draw() {
  tx += 0.01;
  ty += 0.01;
  tz += 0.01;

  background(32);
  lights();

  // 1. Top-Left
  pushMatrix();
  translate(width * 0.25, height * 0.25);
  rotateX(tx);
  ring(255, 0, 0, height * 0.15);
  rotateY(ty);
  ring(0, 255, 0, height * 0.125);
  rotateZ(tz);
  ring(0, 0, 255, height * 0.1);
  scale(height * 0.05, height * 0.045, height * 0.04);
  cube();
  popMatrix();

  // 2. Top-Middle
  pushMatrix();
  translate(width * 0.5, height * 0.25);
  rotateY(ty);
  ring(0, 255, 0, height * 0.15);
  rotateZ(tz);
  ring(0, 0, 255, height * 0.125);
  rotateX(tx);
  ring(255, 0, 0, height * 0.1);
  scale(height * 0.05, height * 0.045, height * 0.04);
  cube();
  popMatrix();

  // 3. Top-Right
  pushMatrix();
  translate(width * 0.75, height * 0.25);
  rotateZ(tz);
  ring(0, 0, 255, height * 0.15);
  rotateX(tx);
  ring(255, 0, 0, height * 0.125);
  rotateY(ty);
  ring(0, 255, 0, height * 0.1);
  scale(height * 0.05, height * 0.045, height * 0.04);
  cube();
  popMatrix();

  // 4. Bottom-Left
  pushMatrix();
  translate(width * 0.25, height * 0.75);
  rotateX(tx);
  ring(255, 0, 0, height * 0.15);
  rotateZ(tz);
  ring(0, 0, 255, height * 0.125);
  rotateY(ty);
  ring(0, 255, 0, height * 0.1);
  scale(height * 0.05, height * 0.045, height * 0.04);
  cube();
  popMatrix();

  // 5. Bottom-Middle
  pushMatrix();
  translate(width * 0.5, height * 0.75);
  rotateY(ty);
  ring(0, 255, 0, height * 0.15);
  rotateX(tx);
  ring(255, 0, 0, height * 0.125);
  rotateZ(tz);
  ring(0, 0, 255, height * 0.1);
  scale(height * 0.05, height * 0.045, height * 0.04);
  cube();
  popMatrix();

  // 6. Bottom-Right
  pushMatrix();
  translate(width * 0.75, height * 0.75);
  rotateZ(tz);
  ring(0, 0, 255, height * 0.15);
  rotateY(ty);
  ring(0, 255, 0, height * 0.125);
  rotateX(tx);
  ring(255, 0, 0, height * 0.1);
  scale(height * 0.05, height * 0.045, height * 0.04);
  cube();
  popMatrix();
}

void scale(PVector v) {
  scale(v.x, v.y, v.z);
}

void ring(float rd, float bl, float gr, float rds) {
  pushStyle();
  noFill();
  strokeWeight(1.5);
  stroke(rd, bl, gr);
  ellipse(0, 0, rds, rds);
  strokeWeight(7.5);
  point(rds, 0);
  popStyle();
}

void cube() {
  pushStyle();
  noStroke();

  fill(154, 204, 255);
  beginShape(QUADS);
  vertex(-1, -1, 1);
  vertex(1, -1, 1);
  vertex(1, 1, 1);
  vertex(-1, 1, 1);
  endShape();

  fill(255, 154, 204);
  beginShape(QUADS);
  vertex(1, -1, 1);
  vertex(1, -1, -1);
  vertex(1, 1, -1);
  vertex(1, 1, 1);
  endShape();

  fill(204, 154, 255);
  beginShape(QUADS);
  vertex(1, -1, -1);
  vertex(-1, -1, -1);
  vertex(-1, 1, -1);
  vertex(1, 1, -1);
  endShape();

  fill(255, 204, 154);
  beginShape(QUADS);
  vertex(-1, -1, -1);
  vertex(-1, -1, 1);
  vertex(-1, 1, 1);
  vertex(-1, 1, -1);
  endShape();

  fill(255, 255, 154);
  beginShape(QUADS);
  vertex(-1, 1, 1);
  vertex(1, 1, 1);
  vertex(1, 1, -1);
  vertex(-1, 1, -1);
  endShape();

  fill(154, 255, 204);
  beginShape(QUADS);
  vertex(-1, -1, -1);
  vertex(1, -1, -1);
  vertex(1, -1, 1);
  vertex(-1, -1, 1);
  endShape();
  popStyle();
}
