'use strict';

var tx, ty, tz;

function setup() {
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);
    noFill();

    // JavaScript's loose-typing means that variables cannot
    // go uninitialized, especially if you are going to use
    // increments like i++ or i += 2. It is only because Java
    // has an 'int' datatype that it can safely assume that the
    // default value of int i; is 0.
    tx = 0;
    ty = 0;
    tz = 0;
}

function draw() {
    tx -= 0.01;
    ty += 0.01;
    tz -= 0.01;

    background(32);
    directionalLight(255, 255, 255, 0, 1, 0);
    ambientLight(64, 64, 64);
    orbitControl();

    // 1. Top-Left
    push();
    translate(-width * 0.325, -height * 0.325);
    rotateX(tx);
    ring(255, 0, 0, height * 0.15);
    rotateY(ty);
    ring(0, 255, 0, height * 0.125);
    rotateZ(tz);
    ring(0, 0, 255, height * 0.1);
    specularMaterial(255);
    box(height * 0.1, height * 0.09, height * 0.08);
    pop();

    // 2. Top-Middle
    push();
    translate(0, -height * 0.325);
    rotateY(ty);
    ring(0, 255, 0, height * 0.15);
    rotateZ(tz);
    ring(0, 0, 255, height * 0.125);
    rotateX(tx);
    ring(255, 0, 0, height * 0.1);
    specularMaterial(255);
    box(height * 0.1, height * 0.09, height * 0.08);
    pop();

    // 3. Top-Right
    push();
    translate(width * 0.325, -height * 0.325);
    rotateZ(tz);
    ring(0, 0, 255, height * 0.15);
    rotateX(tx);
    ring(255, 0, 0, height * 0.125);
    rotateY(ty);
    ring(0, 255, 0, height * 0.1);
    specularMaterial(255);
    box(height * 0.1, height * 0.09, height * 0.08);
    pop();

    // 4. Bottom-Left
    push();
    translate(-width * 0.325, height * 0.325);
    rotateX(tx);
    ring(255, 0, 0, height * 0.15);
    rotateZ(tz);
    ring(0, 0, 255, height * 0.125);
    rotateY(ty);
    ring(0, 255, 0, height * 0.1);
    specularMaterial(255);
    box(height * 0.1, height * 0.09, height * 0.08);
    pop();

    // 5. Bottom-Middle
    push();
    translate(0, height * 0.325);
    rotateY(ty);
    ring(0, 255, 0, height * 0.15);
    rotateX(tx);
    ring(255, 0, 0, height * 0.125);
    rotateZ(tz);
    ring(0, 0, 255, height * 0.1);
    specularMaterial(255);
    box(height * 0.1, height * 0.09, height * 0.08);
    pop();

    // 6. Bottom-Right
    push();
    translate(width * 0.325, height * 0.325);
    rotateZ(tz);
    ring(0, 0, 255, height * 0.15);
    rotateY(ty);
    ring(0, 255, 0, height * 0.125);
    rotateX(tx);
    ring(255, 0, 0, height * 0.1);
    specularMaterial(255);
    box(height * 0.1, height * 0.09, height * 0.08);
    pop();
}

function ring(rd, bl, gr, rds) {
    push();
    noFill();
    ambientMaterial(rd, bl, gr);
    torus(rds, 1.5);
    translate(rds, 0, 0);
    sphere(3.5);
    pop();
}
