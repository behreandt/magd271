'use strict';

// Suzanne (monkey head) is a default 3D model provided
// in Blender.

var obj,
txtr, txtr1, txtr2,
toggle;

// Look up tutorials to find how to map textures onto
// meshes for your preferred 3D-editing software. Not
// all 3D software uses the same conventions for a
// coordinate system. p5.js is more conventional than
// Processing in that y+ is up rather than down in 3D.
// Also note that a material with a texture is not by
// default brought in with the model itself, and can
// be applied with the texture function.
function preload() {
  // The true/false parameter normalizes the scale of the
  // imported model to the world units of p5.js.
  obj = loadModel("data/suzanne.obj", false);
  txtr = txtr1 = loadImage("data/seattle.png");
  txtr2 = loadImage("data/corfu.png");
  toggle = true;
}

function setup() {
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);
}

function draw() {
    background(32);
    pointLight(255, 127, 127, -width / 2.0, -height / 2.0, 0);
    pointLight(127, 255, 127, width / 2.0, -height / 2.0, 0);
    pointLight(127, 127, 255, width / 2.0, height / 2.0, 0);
    directionalLight(127, 127, 127, 0, 0, 1);

    push();
    scale(1.25);
    rotateY(map(mouseX, 0, width, 0, TWO_PI));
    rotateX(map(mouseY, 0, height, -PI, PI));
    texture(txtr);
    model(obj);
    pop();
}

function mousePressed() {
  toggle = !toggle;
  if(toggle) {
    txtr = txtr1;
  } else {
    txtr = txtr2;
  }
}
