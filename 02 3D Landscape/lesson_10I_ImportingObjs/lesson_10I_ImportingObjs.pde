// Suzanne (monkey head) is a default 3D model provided
// in Blender.

// The PShape variable can be used to store .obj
// files that are loaded into the sketch in setup.
PShape obj;
PImage txtr1, txtr2;
boolean toggle;

void setup() {
  size(420, 420, P3D);
  surface.setResizable(true);
  surface.setTitle("Importing .obj Files");
  pixelDensity(displayDensity());
  background(64);

  // While the .obj file is all that needs to be
  // loaded in setup, the sketch's data folder should
  // also contain a .mtl (material) file and any
  // image which may be a texture on the material.

  // .obj and .mtl files are ultimately text, so open
  // them with a code editor like Atom or Sublime to
  // learn more about what information they contain.

  // Look up tutorials to find how to map textures onto
  // meshes for your preferred 3D-editing software. Not
  // all 3D software uses the same conventions for a
  // coordinate system. For example, Processing is very
  // unusual in that the positive y-axis is still down in
  // 3D, so you may have to rotate models to be upside-down
  // before exporting them to Processing.
  obj = loadShape("suzanne.obj");
  txtr1 = loadImage("seattle.png");
  txtr2 = loadImage("corfu.png");
}

void draw() {
  background(32);
  pointLight(255, 127, 127, 0, 0, 0);
  pointLight(127, 255, 127, width, 0, 0);
  pointLight(127, 127, 255, width, height, 0);
  directionalLight(127, 127, 127, 0, 0, -1);

  pushMatrix();
  translate(width / 2.0, height / 2.0, 0);
  scale(1.25);
  rotateY(map(mouseX, 0, width, 0, TWO_PI));
  rotateX(map(mouseY, 0, height, 0, TWO_PI));
  shape(obj);
  popMatrix();
}

void mousePressed() {
  toggle = !toggle;
  if (toggle) {
    obj.setTexture(txtr1);
  } else {
    obj.setTexture(txtr2);
  }
}
