// Cameras.
KeyListener kl;
FirstPersonController fps;
Camera cam;
PVector camDest;
boolean fpsEnabled;

// Light variables.
Light pointLight1;
Light pointLight2;
Light spotLight;

// Generate textures.
ProcGenTexture pgt;

// Pyramid.
Pyramid pyramid;
PVector pyrDest;

// Sphere.
Sphere sphere;

// Box.
Box box;

// Tube.
Tube tube;

void setup() {
  // Basic setup.
  size(640, 420, P3D);
  //fullScreen(P3D);
  pixelDensity(displayDensity());
  surface.setResizable(true);
  surface.setTitle("3D Landscape");
  background(64);

  // Set camera.
  kl = new KeyListener();
  fps = new FirstPersonController(kl);
  cam = new Camera();
  camDest = new PVector(random(-1500, 1500), 
    random(0, height - 100), 
    random(-1500, 1500));

  // Set lights.
  pointLight1 = new Light(Lights.Point, 
    204, 0, 50, 
    width * 0.75, height * 0.5, 0);
  pointLight1.lightFallConst = 1;

  pointLight2 = new Light(Lights.Point, 
    50, 0, 204, 
    width * 0.25, height * 0.5, -100);
  pointLight2.lightFallConst = 1;

  spotLight = new Light(Lights.Spot, 
    204, 204, 204, 
    width / 2, -1000, 0, 
    0, 1, 0, HALF_PI, 0.017);
  spotLight.lightFallConst = 0.7;

  // Set textures.
  // This pgt will create a 2048x2048 texture with 8
  // rows and 8 columns, using the following files as
  // source images.
  pgt = new ProcGenTexture(8, 8, 
    2048, 2048, 
    loadImages("petals01.png", 
    "petals02.png", 
    "petals03.png", 
    "petals04.png", 
    "petals05.png"));

  // Set pyramid.
  pyramid = new Pyramid(300, -height * 0.5, 0, 
    100, 100, 100, 
    0, 0, 0);
  pyramid.addTexture("ma.png");
  pyrDest = new PVector(random(-750, 750), 
    random(0, height - 100), 
    random(-750, 750));

  // Set sphere.
  sphere = new Sphere(width * 0.5, height * 0.35, 0, 
    height * 0.35, height * 0.35, height * 0.35, 
    0, 0, 0, 128);
  sphere.addTexture("go.png");
  sphere.noiseScalar = new PVector(-0.25, 0.25);
  sphere.noiseRange = new PVector(-2, 2);
  sphere.shininess = 2.0;

  // Set box.
  box = new Box(-200, height * 0.25, 150, 
    height * 0.275, height * 0.45, height * 0.25, 
    0, QUARTER_PI, 0);
  box.addTexture(pgt.generate());

  // Set tube.
  tube = new Tube(width + 200, height, 150, 
    height * 0.25, height, height * 0.5, 
    0, 0, 0, 
    0.5, 128, 16);
  tube.addTexture("bricks.png");
  tube.uvScl = new PVector(8, 8);
  tube.shininess = 0;
}

void draw() {
  background(32);
  pointLight1.draw();
  pointLight2.draw();
  spotLight.draw();

  // Toggle available cameras (FPS and touring camera).
  if (fpsEnabled) {
    fps.draw();
  } else {
    cam.draw();
    cam.moveTo(camDest, 0.0125);
    cam.lookAt(pyramid.position, 0.025);
    if (frameCount % 600 == 0) {
      camDest = new PVector(random(-1500, 1500), 
        random(0, height - 100), 
        random(-1500, 1500));
    }
  }

  // Pyramid
  pushMatrix();
  translate(tube.position.x, tube.position.y, tube.position.z);
  rotateY(frameCount / 60.0);
  pyramid.draw();
  pyramid.rotateBy(0.025, 0.025, 0);
  popMatrix();

  // Sphere
  // If viewed from the FPS cam, the sphere will be lumpy because
  // Perlin Noise is being applied to it. If viewed from the touring
  // camera, it will be striped and the mouse will control how much
  // of the sphere is being drawn.
  if (fpsEnabled) {
    sphere.drawIncrY = 1;
    sphere.update();
  } else {
    sphere.drawIncrY = 2;
  }
  sphere.rotateBy(0.01, 0, 0);
  sphere.uvOff.y += 0.01;
  sphere.draw();

  // Box
  box.uvOff.x += 0.001;
  box.draw();

  // Tube
  tube.draw();
}

void keyPressed() {
  kl.keyPressed(keyCode);
}

void keyReleased() {
  kl.keyReleased(keyCode);
  if (key == 'f' || key == 'F') {
    fpsEnabled = !fpsEnabled;
  }
}

void mousePressed() {
  if (mouseButton == RIGHT) {
    save(millis() + ".png");
  } else if (mouseButton == LEFT) {
    cam.isOrthographic = !cam.isOrthographic;
  }
}