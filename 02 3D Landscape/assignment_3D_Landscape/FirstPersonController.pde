class FirstPersonController extends Camera {
  float moveSpeed = 7.5, 
    radius = 300, 
    avatarHeight = 400, 
    incr = 0.01, 
    angle, 
    pitchConstraint, 
    lookSmoothing = 0.025, 
    yawConstraint, 
    leftDeadZone, rightDeadZone;
  int leftKey = 65, 
    rightKey = 68, 
    backKey = 83, 
    forwardKey = 87;
  KeyListener kl;

  FirstPersonController() {
    this(null, new PVector(width * 0.5, height * 0.5, height / 1.155));
  }

  FirstPersonController(KeyListener kl) {
    this(kl, new PVector(width * 0.5, height * 0.5, height / 1.155));
  }

  FirstPersonController(PVector pos) {
    this(null, pos);
  }

  FirstPersonController(KeyListener _kl, PVector pos) {
    super(pos);
    pitchConstraint = height * 0.125;
    leftDeadZone = width * 0.375;
    rightDeadZone = width * 0.625;
    kl = _kl;
    if (kl != null) {
      kl.add(leftKey, rightKey, forwardKey, backKey);
    }
  }

  @Override
    void draw() {
    move();
    look();
    if (isOrthographic) {
      ortho(-width * 0.5, width * 0.5, 
        -height * 0.5, height * 0.5, 
        nearClip, farClip);
    } else {
      perspective(fov, aspect, nearClip, farClip);
    }
    camera(position.x, position.y, position.z, 
      lookAt.x, lookAt.y, lookAt.z, 
      up.x, up.y, up.z);
  }

  void move() {
    if (kl.get(forwardKey)) {
      position.x += sin(angle) * moveSpeed;
      position.z += cos(angle) * moveSpeed;
    }

    if (kl.get(backKey)) {
      position.x -= sin(angle) * moveSpeed;
      position.z -= cos(angle) * moveSpeed;
    }

    if (kl.get(leftKey)) {
      position.x += sin(angle + HALF_PI) * moveSpeed;
      position.z += cos(angle + HALF_PI) * moveSpeed;
    }

    if (kl.get(rightKey)) {
      position.x -= sin(angle + HALF_PI) * moveSpeed;
      position.z -= cos(angle + HALF_PI) * moveSpeed;
    }
  }

  void look() {
    if (focused) {
      if (mouseX > rightDeadZone && mouseX < width) { 
        angle -= incr;
      }
      if (mouseX < leftDeadZone && mouseX > 0) { 
        angle += incr;
      }

      float point = map(mouseY - height * 0.5, 
        height, 0, 
        height - pitchConstraint + avatarHeight, 
        pitchConstraint);
      lookAt.x = radius * sin(angle) + position.x;
      lookAt.y += (point - lookAt.y) * lookSmoothing;
      lookAt.z = radius * cos(angle) + position.z;
    }
  }
}