class Pyramid extends Primitive {
  private PImage[] textures = new PImage[5];

  public Pyramid() {
    this(new PVector(width / 2.0, height / 2.0, 0), 
      new PVector(50, 50, 50), 
      new PVector());
  }

  public Pyramid(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot));
  }

  public Pyramid(PVector position, 
    PVector scale, 
    PVector rotation) {
    super(position, scale, rotation);
    for (int i = 0; i < 5; ++i) {
      textures[i] = createImage(1, 1, RGB);
      textures[i].pixels[0] = color(255);
    }
  }

  public void draw() {
    pushStyle();
    noStroke();
    shininess(shininess);
    specular(specular);
    emissive(emissive);
    ambient(ambient);
    textureMode(NORMAL);
    textureWrap(REPEAT);
    pushMatrix();
    translate(position.x, position.y, position.z);
    rotateZ(rotation.z);
    rotateX(rotation.x);
    rotateY(rotation.y);
    scale(scale.x, scale.y, scale.z);

    // Front
    beginShape(TRIANGLES);
    texture(textures[FRONT_FACE]);
    vertex(1, 1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, 1, 1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    vertex(0, -1, 0, 
      uvOff.x + uvScl.x * 0.5, uvOff.y);
    endShape(CLOSE);

    // Right
    beginShape(TRIANGLES);
    texture(textures[RIGHT_FACE]);
    vertex(1, 1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(1, 1, 1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    vertex(0, -1, 0, 
      uvOff.x + uvScl.x * 0.5, uvOff.y);
    endShape(CLOSE);

    // Back
    beginShape(TRIANGLES);
    texture(textures[BACK_FACE]);
    vertex(-1, 1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(1, 1, -1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    vertex(0, -1, 0, 
      uvOff.x + uvScl.x * 0.5, uvOff.y);
    endShape(CLOSE);

    // Left
    beginShape(TRIANGLES);
    texture(textures[LEFT_FACE]);
    vertex(-1, 1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, 1, -1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    vertex(0, -1, 0, 
      uvOff.x + uvScl.x * 0.5, uvOff.y);
    endShape(CLOSE);

    // Bottom
    beginShape(QUADS);
    texture(textures[BOTTOM_FACE]);
    vertex(-1, 1, 1, 
      uvOff.x, uvOff.y);
    vertex(1, 1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(1, 1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, 1, -1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    popMatrix();
    popStyle();
  }

  public void addTexture(PImage txtr) {
    for (int i = 0; i < 5; ++i) {
      this.textures[i] = txtr;
    }
  }

  public void addTexture(PImage txtr, int face) {
    if (face < 0 || face > 4) { return; }
    textures[face] = txtr;
  }

  public void addTexture(String txtr) {
    PImage texture = loadImage(txtr);
    for (int i = 0; i < 5; ++i) {
      textures[i] = texture;
    }
  }

  public void addTexture(String txtr, int face) {
    if (face < 0 || face > 4) { 
      return;
    }
    textures[face] = loadImage(txtr);
  }
}