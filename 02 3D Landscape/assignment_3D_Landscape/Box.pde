class Box extends Primitive {
  private PImage[] textures = new PImage[6];

  public Box() {
    this(new PVector(width / 2.0, height / 2.0, 0), 
      new PVector(50, 50, 50), 
      new PVector());
  }

  public Box(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot));
  }

  public Box(PVector position, 
    PVector scale, 
    PVector rotation) {
    super(position, scale, rotation);
    for (int i = 0; i < 6; ++i) {
      textures[i] = createImage(1, 1, RGB);
      textures[i].pixels[0] = color(255);
    }
  }

  public void draw() {
    pushStyle();
    noStroke();
    shininess(shininess);
    specular(specular);
    emissive(emissive);
    ambient(ambient);
    textureMode(NORMAL);
    textureWrap(REPEAT);
    pushMatrix();
    translate(position.x, position.y, position.z);
    rotateZ(rotation.z);
    rotateX(rotation.x);
    rotateY(rotation.y);
    scale(scale.x, scale.y, scale.z);

    // Front
    beginShape(QUADS);
    texture(textures[FRONT_FACE]);
    vertex(-1, -1, 1, 
      uvOff.x, uvOff.y);
    vertex(1, -1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(1, 1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, 1, 1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    // Right
    beginShape(QUADS);
    texture(textures[RIGHT_FACE]);
    vertex(1, -1, 1, 
      uvOff.x, uvOff.y);
    vertex(1, -1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(1, 1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(1, 1, 1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    // Back
    beginShape(QUADS);
    texture(textures[BACK_FACE]);
    vertex(1, -1, -1, 
      uvOff.x, uvOff.y);
    vertex(-1, -1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(-1, 1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y); 
    vertex(1, 1, -1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    // Left
    beginShape(QUADS);
    texture(textures[LEFT_FACE]);
    vertex(-1, -1, -1, 
      uvOff.x, uvOff.y);
    vertex(-1, -1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(-1, 1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, 1, -1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    // Bottom
    beginShape(QUADS);
    texture(textures[BOTTOM_FACE]);
    vertex(-1, 1, 1, 
      uvOff.x, uvOff.y);
    vertex(1, 1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(1, 1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, 1, -1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    // Top
    beginShape(QUADS);
    texture(textures[TOP_FACE]);
    vertex(-1, -1, -1, 
      uvOff.x, uvOff.y);
    vertex(1, -1, -1, 
      uvOff.x + 1 * uvScl.x, uvOff.y);
    vertex(1, -1, 1, 
      uvOff.x + 1 * uvScl.x, uvOff.y + 1 * uvScl.y);
    vertex(-1, -1, 1, 
      uvOff.x, uvOff.y + 1 * uvScl.y);
    endShape();

    popMatrix();
    popStyle();
  }

  public void addTexture(PImage txtr) {
    for (int i = 0; i < 6; ++i) {
      textures[i] = txtr;
    }
  }

  public void addTexture(PImage txtr, int face) {
    if (face < 0 || face > 5) return;
    textures[face] = txtr;
  }

  public void addTexture(String txtr) {
    PImage texture = loadImage(txtr);
    for (int i = 0; i < 6; ++i) {
      textures[i] = texture;
    }
  }

  public void addTexture(String txtr, int face) {
    if (face < 0 || face > 5) return;
    textures[face] = loadImage(txtr);
  }
}