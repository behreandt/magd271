// A spatial object is one that has a position in space.
// This class exists so that objects like cameras and lights,
// which do not have any physical extent but can move around,
// can share the same parent as geometric primitives.
abstract class Spatial {
  public PVector position = new PVector(width * .5, height * .5, 0);

  protected Spatial() {
    this(new PVector(width * .5, height * .5, 0));
  }

  protected Spatial(float x, float y, float z) {
    this(new PVector(x, y, z));
  }

  protected Spatial(PVector v) {
    position = v;
  }

  public String toString() {
    return getClass().getSimpleName()
      + "\n" + position.toString();
  }

  public void moveBy(PVector v) {
    position.add(v);
  }

  public void moveBy(float x, float y, float z) {
    position.x += x;
    position.y += y;
    position.z += z;
  }

  public void moveTo(PVector v) {
    position = v;
  }

  public void moveTo(PVector v, float t) {
    position.lerp(v, t);
  }

  public void moveTo(float x, float y, float z) {
    position.x = x;
    position.y = y;
    position.z = z;
  }

  public void moveTo(float x, float y, float z, float t) {
    position.lerp(x, y, z, t);
  }
}