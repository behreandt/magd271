class Camera extends Spatial {
  public PVector lookAt, 
    up = new PVector(0, 1, 0);
  public float fov = PI / 3.0, 
    aspect, 
    nearClip, 
    farClip;
  public boolean isOrthographic = false, 
    noCursor = false;

  public Camera() {
    this(new PVector(width * 0.5, height * 0.5, height / 1.155), 
      new PVector(width * 0.5, height * 0.5, 0), 
      new PVector(0, 1, 0), 
      PI / 3.0, width / (float)height, 
      height / 115.5, height / 0.01155, false);
  }

  public Camera(PVector position) {
    this(position, 
      new PVector(position.x, position.y, position.z - height / 1.155), 
      new PVector(0, 1, 0), 
      PI / 3.0, width / (float)height, 
      height / 115.5, height / 0.01155, false);
  }

  public Camera(PVector position, PVector lookAt, 
    PVector up, float fov, float aspect, 
    float nearClip, float farClip, boolean isOrthographic) {
    super(position);
    if (noCursor) {
      noCursor();
    }
    this.lookAt = lookAt;
    this.up = up;
    this.fov = fov;
    this.aspect = aspect;
    this.nearClip = nearClip;
    this.farClip = farClip;
    this.isOrthographic = isOrthographic;
  }

  public String toString() {
    return  getClass().getSimpleName()+ "\n[ "
      + position + ",\n  "
      + lookAt + ",\n  "
      + up + " ]";
  }

  void draw() {
    if (isOrthographic) {
      ortho(-width * 0.5, width * 0.5, 
        -height * 0.5, height * 0.5, 
        nearClip, farClip);
    } else {
      perspective(fov, aspect, nearClip, farClip);
    }
    camera(position.x, position.y, position.z, 
      lookAt.x, lookAt.y, lookAt.z, 
      up.x, up.y, up.z);
  }

  public void lookAt(PVector v) {
    lookAt = v;
  }

  public void lookAt(PVector v, float t) {
    lookAt.lerp(v, t);
  }

  public void lookAt(float x, float y, float z) {
    lookAt.x = x;
    lookAt.y = y;
    lookAt.z = z;
  }

  public void lookAt(float x, float y, float z, float t) {
    lookAt.lerp(x, y, z, t);
  }
}