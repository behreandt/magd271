class ProcGenTexture {
  private java.util.List<PImage> pool
    = new java.util.ArrayList<PImage>();
  public int rows = 8, 
    cols = 8, 
    w = 256, 
    h = 256;
  private int sampleW, sampleH;

  public ProcGenTexture(PImage... images) {
    this(8, 8, 256, 256, images);
  }
  
  public ProcGenTexture(int rows, int cols, 
    int w, int h, 
    PImage... images) {
    this.rows = rows;
    this.cols = cols;
    this.w = w;
    this.h = h;
    sampleW = ceil(w / (float)cols);
    sampleH = ceil(h / (float)rows);

    for (int i = 0, size = images.length; i < size; ++i) {
      add(images[i]);
    }
  }

  public void add(PImage img) {
    img.resize(sampleW, sampleH);
    pool.add(img);
  }

  public PImage generate() {
    int size = pool.size();
    if (size <= 0) {
      return createImage(w, h, ARGB);
    }
    PImage result = createImage(w, h, ARGB);
    result.loadPixels();
    for (int x = 0; x < w; x+= sampleW) {
      for (int y = 0; y < h; y += sampleH) {
        result.set(x, y, 
          pool.get(int(random(0, size))));
      }
    }
    result.updatePixels();
    return result;
  }
}

PImage[] loadImages(String... imageNames) {
  int size = imageNames.length;
  PImage[] result = new PImage[size];
  for (int i = 0; i < size; ++i) {
    result[i] = loadImage(imageNames[i]);
  }
  return result;
}