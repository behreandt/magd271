// SOURCE & REFERENCE
// Dan Shiffman, Spherical Geometry
// https://youtu.be/RkuBWEkBrZA

class Sphere extends Primitive {
  private PImage texture;
  private int detailX = 256, 
    detailY = 256, 
    drawFromX = 0, 
    drawFromY = 0, 
    drawToX = 256, 
    drawToY = 256, 
    drawIncrY = 1;
  private PVector vertices[][], 
    uvs[][];
  private PVector noiseScalar = new PVector(0, 0), 
    noiseRange = new PVector(-2, 2);

  public Sphere() {
    this(new PVector(width / 2.0, height / 2.0, 0), 
      new PVector(50, 50, 50), 
      new PVector(), 
      256, 
      new PVector());
  }

  public Sphere(int detail) {
    this(new PVector(width / 2.0, height / 2.0, 0), 
      new PVector(50, 50, 50), 
      new PVector(), 
      detail, 
      new PVector());
  }

  public Sphere(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot, 
    int detail) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot), 
      detail, new PVector());
  }

  public Sphere(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot, 
    int detail, float noiseMin, float noiseMax) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot), 
      detail, new PVector(noiseMin, noiseMax));
  }

  public Sphere(PVector position, 
    PVector scale, 
    PVector rotation, 
    int detail, 
    PVector noiseScalar) {
    super(position, scale, rotation);
    texture = createImage(1, 1, RGB);
    texture.pixels[0] = color(255);
    detailX = detailY = detail < 3 ? 3 : detail;
    drawToX = detailX;
    drawToY = detailY;
    this.noiseScalar = noiseScalar;
    vertices = new PVector[detailY + 1][detailX + 1];
    uvs = new PVector[detailY + 1][detailX + 1];
    float longitude = 0, latitude = 0, xoff = 0, yoff = 0, n = 0;
    for (int i = 0; i < detailY + 1; ++i) {      
      latitude = map(i, 0, detailY, 0, PI);
      yoff = map(i, 0, detailY, noiseRange.x, noiseRange.y);
      for (int j = 0; j < detailX + 1; ++j) {
        xoff = map(j, 0, detailX, noiseRange.x, noiseRange.y);
        longitude = map(j, 0, detailX, 0, TWO_PI);
        n = map(noise(xoff, yoff), 0, 1, noiseScalar.x, noiseScalar.y);
        vertices[i][j] = new PVector(
          n + sin(latitude) * cos(longitude), 
          n + sin(latitude) * sin(longitude), 
          n + cos(latitude));
        uvs[i][j] = new PVector(
          map(i, 0, detailY, 0, 1), 
          map(j, 0, detailX, 0, 1));
      }
    }
  }

  public void update() {
    float longitude = 0, 
      latitude = 0, 
      xoff = 0, 
      yoff = 0, 
      noise = 0, 
      zoff = frameCount * 0.015;
    for (int i = 0; i < detailY + 1; ++i) {
      latitude = map(i, 0, detailY, 0, PI);
      yoff = map(i, 0, detailY, noiseRange.x, noiseRange.y);
      for (int j = 0; j < detailX + 1; ++j) {
        longitude = map(j, 0, detailX, 0, TWO_PI);
        xoff = map(j, 0, detailX, noiseRange.x, noiseRange.y);
        noise = map(noise(xoff, yoff, zoff), 
          0, 1, noiseScalar.x, noiseScalar.y);
        vertices[i][j] = new PVector(
          noise + sin(latitude) * cos(longitude), 
          noise + sin(latitude) * sin(longitude), 
          noise + cos(latitude));
      }
    }
  }

  public void draw() {
    pushStyle();
    noStroke();
    shininess(shininess);
    specular(specular);
    emissive(emissive);
    ambient(ambient);
    textureMode(NORMAL);
    textureWrap(REPEAT);
    pushMatrix();
    translate(position.x, position.y, position.z);
    rotateZ(rotation.z);
    rotateX(rotation.x);
    rotateY(rotation.y);
    scale(scale.x, scale.y, scale.z);
    for (int i = drawFromY; i < drawToY; i += drawIncrY) {
      beginShape(TRIANGLE_STRIP);
      texture(texture);
      for (int j = drawFromX; j < drawToX + 1; ++j) {
        vertex(vertices[i][j].x, 
          vertices[i][j].y, 
          vertices[i][j].z, 
          uvOff.x + uvs[i][j].x * uvScl.x, 
          uvOff.y + uvs[i][j].y * uvScl.y);
        vertex(vertices[i + 1][j].x, 
          vertices[i + 1][j].y, 
          vertices[i + 1][j].z, 
          uvOff.x + uvs[i + 1][j].x * uvScl.x, 
          uvOff.y + uvs[i + 1][j].y * uvScl.y);
      }
      endShape(CLOSE);
    }
    popMatrix();
    popStyle();
  }

  public void addTexture(PImage txtr) {
    texture = txtr;
  }

  public void addTexture(String txtr) {
    texture = loadImage(txtr);
  }
}