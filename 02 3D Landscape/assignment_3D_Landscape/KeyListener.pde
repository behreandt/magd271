// A key listener makes it possible for Processing to register multiple
// simultaneous key presses. It does this by creating a dictionary. Each
// entry in the dictionary consists of a Key-Value Pair, where the keyCode
// is the Key, and a boolean is the Value. If a key is being pressed, when
// the entry is looked up, the value of that entry will be True.

class KeyListener {
  private java.util.Map<Integer, Boolean> pressed = 
    new java.util.HashMap<Integer, Boolean>();

  public KeyListener() {
    this(UP, DOWN, LEFT, RIGHT, SHIFT);
  }

  public KeyListener(Integer... admissibleKeys) {
    for (int i = 0, size = admissibleKeys.length; i < size; ++i) {
      pressed.put(admissibleKeys[i], false);
    }
  }
  
  public String toString() {
    return pressed.toString();
  }

  public Boolean get(Integer kc) {
    return pressed.containsKey(kc) ? pressed.get(kc) : false;
  }

  public void add(Integer... newKeys) {
    for (int i = 0, size = newKeys.length; i < size; ++i) {
      pressed.put(newKeys[i], false);
    }
  }

  public void keyPressed(Integer kc) {
    pressed.put(kc, true);
  }

  public void keyReleased(Integer kc) {
    pressed.put(kc, false);
  }
}