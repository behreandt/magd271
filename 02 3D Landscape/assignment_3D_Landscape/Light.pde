enum Lights {
  Ambient, Directional, Point, Spot
}

class Light extends Spatial {
  private boolean on = true;
  public PVector direction = new PVector(-0.5, 0.5, -0.5);
  private float r = 255, 
    g = 244, 
    b = 214, 
    angle = PI, 
    concentration = 100, 
    specr = 128, 
    specg = 128, 
    specb = 128, 
    lightFallConst = 1, 
    lightFallLin, 
    lightFallQuad;
  private Lights type;

  public Light() {
    this(Lights.Directional, 
      255, 244, 214, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t) {
    this(t, 255, 244, 214, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, 
    float r, float g, float b) {
    this(t, r, g, b, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      width / 2.0, height / 2.0, 0, 
      -0.5, 0.5, -0.5, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c, 
    float x, float y, float z) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      x, y, z, 
      x, y, z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, 
    float r, float g, float b, 
    PVector v) {
    this(t, r, g, b, 
      v.x, v.y, v.z, 
      v.x, v.y, v.z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c, PVector v) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      v.x, v.y, v.z, 
      v.x, v.y, v.z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, color c, 
    PVector v, PVector n, 
    float angle, float concentration) {
    this(t, c >> 16 & 0xff, c >> 8 & 0xff, c & 0xff, 
      v.x, v.y, v.z, 
      n.x, n.y, n.z, 
      angle, concentration);
  }

  public Light(Lights t, 
    float r, float g, float b, 
    float x, float y, float z) {
    this(t, r, g, b, 
      x, y, z, 
      x, y, z, 
      QUARTER_PI, 600);
  }

  public Light(Lights t, 
    float r, float g, float b, 
    float x, float y, float z, 
    float nx, float ny, float nz, 
    float angle, float concentration) {
    this.r = r;
    this.g = g;
    this.b = b;
    position.x = x;
    position.y = y;
    position.z = z;
    direction.x = constrain(nx, -1, 1);
    direction.y = constrain(ny, -1, 1);
    direction.z = constrain(nz, -1, 1);
    this.angle = angle;
    this.concentration = concentration;
    type = t;
  }

  public String toString() {
    if (type == Lights.Ambient || type == Lights.Point) {
      return "[ " + type
        + ",\n  " + r + ", " + g +", " + b
        + ",\n  " + position.x + ", " + position.y + ", "
        + position.z + " ]";
    } else if (type == Lights.Directional) {
      return "[" + type
        + ",\n" + r + ", " + g +", " + b
        + ",\n" + direction.x + ", " + direction.y + ", "
        + direction.z + "]";
    } else {
      return "[" + type
        + ",\n" + r + ", " + g +", " + b
        + ",\n" + position.x + ", " + position.y + ", "
        + position.z
        + ",\n" + direction.x + ", " + direction.y + ", "
        + direction.z
        + ",\n" + angle + ", " + concentration + "]";
    }
  }

  public void draw() {
    if (on) {
      lightSpecular(specr, specg, specb);
      lightFalloff(lightFallConst, lightFallLin, lightFallQuad);
      if (type == Lights.Ambient) {
        ambientLight(r, g, b, 
          position.x, position.y, position.z);
      } else if (type == Lights.Point) {
        pointLight(r, g, b, 
          position.x, position.y, position.z);
      } else if (type == Lights.Spot) {
        spotLight(r, g, b, 
          position.x, position.y, position.z, 
          direction.x, direction.y, direction.z, 
          angle, concentration);
      } else if (type == Lights.Directional) {
        directionalLight(r, g, b, 
          direction.x, direction.y, direction.z);
      }
    }
  }

  public void setColor(float r, float g, float b) {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  // The color variable can be broken down using the Processing
  // functions red, green, and blue; however the reference encourages
  // the usage of bitwise operators as being more efficient. This
  // has not been tested in different color modes (HSB v. RGB).
  public void setColor(color c) {
    r = c >> 16 & 0xff; 
    g = c >> 8 & 0xff;
    b = c & 0xff;
  }

  public void setSpecular(float r, float g, float b) {
    specr = r;
    specg = g;
    specb = b;
  }

  public void setSpecular(color c) {
    specr = c >> 16 & 0xff; 
    specg = c >> 8 & 0xff;
    specb = c & 0xff;
  }

  public void setFalloff(float constant, 
    float linear, 
    float quadratic) {
    lightFallConst = constant;
    lightFallLin = linear;
    lightFallQuad = quadratic;
  }

  public color getColor() {
    return color(r, g, b);
  }

  public color getSpecular() {
    return color(specr, specg, specb);
  }
}