class Plane extends Primitive {
  private PImage texture;
  private PVector vertices[][], 
    uvs[][];
  private PVector noiseIncr = new PVector(0.05, 0.05, 0.015);
  private int rows = 32, 
    cols = 32;

  public Plane() {
    this(new PVector(width / 2.0, height, 0), 
      new PVector(width, 1, width), 
      new PVector(), 
      32, 32, 0);
  }

  public Plane(int rows, int cols) {
    this(new PVector(width / 2.0, height, 0), 
      new PVector(width, 1, width), 
      new PVector(), 
      rows, cols, 0);
  }

  public Plane(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot, 
    int rows, int cols, float noiseIncr) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot), 
      rows, cols, new PVector(noiseIncr, noiseIncr, noiseIncr));
  }

  public Plane(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot, 
    int rows, int cols, 
    float noiseIncrX, float noiseIncrY, float noiseIncrZ) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot), 
      rows, cols, new PVector(noiseIncrX, noiseIncrY, noiseIncrZ));
  }

  public Plane(PVector position, 
    PVector scale, 
    PVector rotation, 
    int rows, int cols, float noiseIncr) {
    this(position, scale, rotation, 
      rows, cols, new PVector(noiseIncr, noiseIncr, noiseIncr));
  }

  public Plane(PVector position, 
    PVector scale, 
    PVector rotation, 
    int rows, int cols, PVector noiseIncr) {
    super(position, scale, rotation);
    texture = createImage(1, 1, RGB);
    texture.pixels[0] = color(255);
    this.noiseIncr = noiseIncr;
    this.cols = cols;
    this.rows = rows;
    vertices = new PVector[cols][rows];
    uvs = new PVector[cols][rows];
    float _x, _y, _z, _u, _v, 
      _xoff = 0, _yoff = 0;
    for (int i = 0; i < cols; ++i) {
      _xoff = 0;
      for (int j = 0; j < rows; ++j) {
        _x = map(i, 0, cols, -1, 1);
        _z = map(j, 0, rows, -1, 1);
        _y = map(noise(_xoff, _yoff), 0, 1, -1, 1);
        _u = map(i, 0, cols, 0, 1);
        _v = map(j, 0, rows, 0, 1);

        vertices[i][j] = new PVector(_x, _y, _z);
        uvs[i][j] = new PVector(_u, _v);

        _xoff += this.noiseIncr.x;
      }
      _yoff += this.noiseIncr.y;
    }
  }

  public void update() {
    float _xoff = 0, _yoff = 0;
    for (int i = 0; i < cols; ++i) {
      _xoff = 0;
      for (int j = 0; j < rows; ++j) {
        vertices[i][j].y = map(noise(_xoff, _yoff, 
          frameCount * noiseIncr.z), 
          0, 1, -0.5, 0.5);
        _xoff += noiseIncr.x;
      }
      _yoff += noiseIncr.y;
    }
  }

  public void draw() {
    pushStyle();
    noStroke();
    shininess(shininess);
    specular(specular);
    emissive(emissive);
    ambient(ambient);
    textureMode(NORMAL);
    textureWrap(REPEAT);
    pushMatrix();
    translate(position.x, position.y, position.z);
    rotateZ(rotation.z);
    rotateX(rotation.x);
    rotateY(rotation.y);
    scale(scale.x, scale.y, scale.z);
    for (int j = 0; j < rows; ++j) {
      beginShape(TRIANGLE_STRIP);
      texture(texture);
      for (int i = 0; i < cols; ++i) {
        vertex(vertices[i][j].x, 
          vertices[i][j].y, 
          vertices[i][j].z, 
          uvOff.x + uvs[i][j].x * uvScl.x, 
          uvOff.x + uvs[i][j].y * uvScl.y);
        if (j != rows -1) {
          vertex(vertices[i][j + 1].x, 
            vertices[i][j + 1].y, 
            vertices[i][j + 1].z, 
            uvOff.x + uvs[i][j + 1].x * uvScl.x, 
            uvOff.x + uvs[i][j + 1].y * uvScl.y);
        }
      }
      endShape(CLOSE);
    }
    popMatrix();
    popStyle();
  }

  public void addTexture(PImage txtr) {
    this.texture = txtr;
  }

  public void addTexture(String txtr) {
    this.texture = loadImage(txtr);
  }
}