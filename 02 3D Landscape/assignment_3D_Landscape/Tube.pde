// SOURCE & REFERENCE
// Andres Colubri, Shaders
// https://processing.org/tutorials/pshader/
// Jasper Flick, Curves and Splines
// http://catlikecoding.com/unity/tutorials/curves-and-splines/

class Tube extends Primitive {
  private PImage texture;
  private int detailX = 256, 
    detailY = 10, 
    drawIncrY = 1, 
    undulation = 50;
  private float taper = 1, undSpeed = 0.06;
  private PVector vertices[][], 
    uvs[][], top[], bottom[], endUvs[];
  private boolean drawTop = true, 
    drawBottom = true, 
    randomizeBend = false, 
    addNoise = false, 
    bend = false;
  private PVector anchor1 = new PVector(), 
    anchor2 = new PVector(0, -1, 0), 
    control1 = new PVector(), 
    control2 = new PVector(0, -1, 0), 
    control1Dest = new PVector(), 
    control2Dest = new PVector(0, -1, 0), 
    bendiness = new PVector(-7, 7), 
    noiseRange = new PVector(-2, 2), 
    noiseScalar = new PVector(-0.5, 0.5);

  public Tube() {
    this(new PVector(width * 0.5, height, 0), 
      new PVector(50, 50, 50), 
      new PVector(), 
      1, 128, 1);
  }

  public Tube(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot, 
    float taper, int detailX, int detailY) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot), 
      taper, detailX, detailY);
  }

  public Tube(PVector position, 
    PVector scale, 
    PVector rotation, 
    float taper, int dtlX, int dtlY) {
    super(position, scale, rotation);
    texture = createImage(1, 1, RGB);
    texture.pixels[0] = color(255);
    this.taper = constrain(taper, 0, 1);
    detailX = dtlX < 3 ? 3 : dtlX;
    detailY = dtlY < 1 ? 1 : dtlY;
    vertices = new PVector[detailY + 1][detailX + 1];
    uvs = new PVector[detailY + 1][detailX + 1];
    top = new PVector[detailX + 1];
    bottom = new PVector[detailX + 1];
    endUvs = new PVector[detailX + 1];
    if (randomizeBend) {
      control1 = new PVector(random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y));
      control2 = new PVector(random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y));
    }
    PVector top = new PVector();
    PVector bottom = new PVector();
    if (bend) {
      top = bezier3D(1.0);
      bottom = bezier3D(1 / float(dtlY + 1));
    }
    float theta = 0, tpr = 1, step = 0;
    PVector b3d = new PVector();
    for (int j = 0; j < dtlX + 1; ++j) {
      theta = map(j, 0, dtlX, 0, TWO_PI);
      this.top[j] = new PVector(top.x + cos(theta) * taper, 
        -1, top.y + sin(theta) * taper);
      this.bottom[j] = new PVector(bottom.x + cos(theta), 
        0, bottom.y + sin(theta));
      endUvs[j] = new PVector(
        0.5 + cos(theta) / 2.0, 
        0.5 + sin(theta) / 2.0);
      for (int i = 0; i < dtlY + 1; ++i) {
        tpr = map(i, 0, dtlY, 1, taper);
        if (bend) {
          step = map(i, -1, this.detailY, 0, 1);
          b3d = bezier3D(step);
        }
        vertices[i][j] = new PVector(
          b3d.x + cos(theta) * tpr, 
          map(i, 0, this.detailY, 0, -1), 
          b3d.y + sin(theta) * tpr);
        uvs[i][j] = new PVector(
          map(j, 0, this.detailX, 0, 1), 
          map(i, 0, this.detailY, 0, 1));
      }
    }
  }

  public void update() {
    if (frameCount % undulation == 0) {
      control1Dest = new PVector(random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y));
      control2Dest = new PVector(random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y), 
        random(bendiness.x, bendiness.y));
    }
    control1.lerp(control1Dest, undSpeed);
    control2.lerp(control2Dest, undSpeed);
    PVector topLoc = bezier3D(1.0), 
      bottomLoc = bezier3D(1 / float(detailY + 1)), 
      b3d = new PVector();
    float theta = 0, 
      tpr = 1, 
      step = 0, 
      xoff = 0, 
      yoff = 0, 
      noise = 0, 
      zoff = frameCount * 0.015;
    for (int j = 0; j < detailX + 1; ++j) {
      xoff = j == 0 ? 0
        : map(j, 0, detailX, noiseRange.x, noiseRange.y);
      theta = j == 0 ? 0
        : map(j, 0, detailX, 0, TWO_PI);
      top[j] = new PVector(topLoc.x + cos(theta) * taper, 
        -1, topLoc.y + sin(theta) * taper);
      bottom[j] = new PVector(bottomLoc.x + cos(theta), 
        0, bottomLoc.y + sin(theta));
      endUvs[j] = new PVector(
        0.5 + cos(theta) / 2.0, 
        0.5 + sin(theta) / 2.0);
      for (int i = 0; i < detailY + 1; ++i) {
        yoff = map(i, 0, detailY, noiseRange.x, noiseRange.y);
        if (addNoise) {
          noise = map(noise(xoff, yoff, zoff), 
            0, 1, noiseScalar.x, noiseScalar.y);
        }
        tpr = map(i, 0, detailY, 1, taper);
        step = map(i, -1, detailY, 0, 1);
        b3d = bezier3D(step);
        vertices[i][j] = new PVector(
          noise + b3d.x + cos(theta) * tpr, 
          map(i, 0, detailY, 0, -1), 
          noise + b3d.y + sin(theta) * tpr);
      }
    }
  }

  public void draw() {
    pushStyle();
    noStroke();
    shininess(shininess);
    specular(specular);
    emissive(emissive);
    ambient(ambient);
    textureMode(NORMAL);
    textureWrap(REPEAT);
    pushMatrix();
    translate(position.x, position.y, position.z);
    rotateZ(rotation.z);
    rotateX(rotation.x);
    rotateY(rotation.y);
    scale(scale.x, scale.y, scale.z);

    if (drawTop) {
      beginShape();
      texture(texture);
      for (int j = 0; j < detailX + 1; ++j) {
        vertex(bottom[j].x, bottom[j].y, bottom[j].z, 
          uvOff.x + endUvs[j].x * uvScl.x, 
          uvOff.y + endUvs[j].y * uvScl.y);
      }
      endShape(CLOSE);
    }

    if (drawBottom) {
      beginShape();
      texture(texture);
      for (int j = 0; j < detailX + 1; ++j) {
        vertex(top[j].x, top[j].y, top[j].z, 
          uvOff.x + endUvs[j].x * uvScl.x, 
          uvOff.y + endUvs[j].y * uvScl.y);
      }
      endShape(CLOSE);
    }

    for (int i = 0; i < detailY; i += drawIncrY) {
      beginShape(TRIANGLE_STRIP);
      texture(texture);
      for (int j = 0; j < detailX + 1; ++j) {
        vertex(vertices[i][j].x, 
          vertices[i][j].y, 
          vertices[i][j].z, 
          uvOff.x + uvs[i][j].x * uvScl.x, 
          uvOff.y + uvs[i][j].y * uvScl.y);
        vertex(vertices[i + 1][j].x, 
          vertices[i + 1][j].y, 
          vertices[i + 1][j].z, 
          uvOff.x + uvs[i + 1][j].x * uvScl.x, 
          uvOff.y + uvs[i + 1][j].y * uvScl.y);
      }
      endShape(CLOSE);
    }
    popMatrix();
    popStyle();
  }

  public void addTexture(PImage txtr) {
    texture = txtr;
  }

  public void addTexture(String txtr) {
    texture = loadImage(txtr);
  }

  public PVector bezier3D(float step) {
    float oneMinusStep = 1 - step;
    PVector v0 = PVector.mult(anchor1, 
      oneMinusStep * oneMinusStep * oneMinusStep);
    PVector v1 = PVector.mult(anchor2, 
      3 * oneMinusStep * oneMinusStep * step);
    PVector v2 = PVector.mult(control1, 
      3 * oneMinusStep * step * step);
    PVector v3 = PVector.mult(control2, 
      step * step * step);
    return new PVector(
      v0.x + v1.x + v2.x + v3.x, 
      v0.y + v1.y + v2.y + v3.y, 
      v0.z + v1.z + v2.z + v3.z);
  }
}