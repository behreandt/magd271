// These constants make it easier to access the individual faces
// of a pyramid or cube by name.
public static final int FRONT_FACE = 0;
public static final int RIGHT_FACE = 1;
public static final int BACK_FACE = 2;
public static final int LEFT_FACE = 3;
public static final int BOTTOM_FACE = 4;
public static final int TOP_FACE = 5;

// This class is meant to parent 3D objects which have volume,
// and thus can be scaled and rotated. It also includes variables which
// control the texturing of their faces. It commands any class which
// extends it to define functionality for drawing and adding textures.
abstract class Primitive extends Spatial {
  public PVector offset = new PVector(), 
    scale = new PVector(1, 1, 1), 
    rotation = new PVector(), 
    uvOff = new PVector(), 
    uvScl = new PVector(1, 1);
  public float shininess = 0;
  public color specular = color(127, 122, 107), 
    emissive = color(0, 0, 0), 
    ambient = color(0, 0, 0);
  public Primitive parent = null;
  public java.util.List<Primitive> children
    = new java.util.ArrayList<Primitive>();

  protected Primitive() {
    this(new PVector(width * 0.5, height * 0.5, 0), 
      new PVector(1, 1, 1), 
      new PVector());
  }

  protected Primitive(float x, float y, float z, 
    float w, float h, float d, 
    float xRot, float yRot, float zRot) {
    this(new PVector(x, y, z), 
      new PVector(w, h, d), 
      new PVector(xRot, yRot, zRot));
  }

  protected Primitive(PVector position, 
    PVector scale, 
    PVector rotation) {
    super(position);
    this.scale = scale;
    this.rotation = rotation;
  }

  public String toString() {
    return getClass().getSimpleName()+ "\n[ "
      + position + ",\n  "
      + rotation + ",\n  "
      + scale + " ]";
  }

  public void rotateBy(PVector v) {
    rotation.add(v);
  }

  public void rotateBy(float x, float y, float z) {
    rotation.x += x;
    rotation.y += y;
    rotation.z += z;
  }

  public void rotateTo(PVector v) {
    rotation = v;
  }

  public void rotateTo(PVector v, float t) {
    rotation.lerp(v, t);
  }

  public void rotateTo(float x, float y, float z) {
    rotation.x = x;
    rotation.y = y;
    rotation.z = z;
  }

  public void rotateTo(float x, float y, float z, float t) {
    rotation.lerp(x, y, z, t);
  }

  public void scaleBy(float scalar) {
    scale.mult(scalar);
  }

  public void scaleTo(PVector v) {
    scale = v;
  }

  public void scaleTo(PVector v, float t) {
    scale.lerp(v, t);
  }

  public void scaleTo(float x, float y, float z) {
    scale.x = x;
    scale.y = y;
    scale.z = z;
  }

  public void scaleTo(float x, float y, float z, float t) {
    scale.lerp(x, y, z, t);
  }

  public void addChild(Primitive child) {
    child.parent = this;
    children.add(child);
  }
  
  public boolean removeChild(Primitive child) {
    child.parent = null;
    return children.remove(child);
  }

  public abstract void draw();
  public abstract void addTexture(PImage txtr);
  public abstract void addTexture(String txtr);
}