float theta1, theta2, 
  sphereX, sphereY, sphereZ, 
  sphereRadius, 
  boxX, boxY, boxZ, 
  boxW, boxH, boxD;

void setup() {
  // Adding P3D as an argument to the size function
  // switches Processing's rendering mode to 3D.
  size(640, 420, P3D);
  pixelDensity(displayDensity());
  surface.setResizable(true);
  surface.setTitle("Primitives");
  background(64);

  sphereX = width * 0.25;
  sphereY = height * 0.5;
  sphereZ = 0;
  sphereRadius = 75;

  boxX = width * 0.75;
  boxY = height * 0.5;
  boxZ = 0;
  boxW = 50;
  boxH = 100;
  boxD = 200;
}

void draw() {
  background(32);
  // Lighting will add a sense of volume to the scene.
  lights();

  strokeWeight(1.5);
  stroke(255);

  // Update to accomodate resizability.
  sphereX = width * 0.25;
  sphereY = height * 0.5;
  boxX = width * 0.75;
  boxY = height * 0.5;

  fill(0, 127, 255);
  pushMatrix();
  // 3D Shapes must be translated to their location.
  translate(sphereX, sphereY, sphereZ);
  rotateX(theta1);
  sphere(sphereRadius);
  popMatrix();

  fill(255, 127, 0);
  pushMatrix();
  translate(boxX, boxY, boxZ);
  rotateY(theta2);
  box(boxW, boxH, boxD);
  popMatrix();

  theta1 += 0.01;
  theta2 += 0.01;
  boxZ = map(mouseX, 0, width, -75, 75);
  sphereZ = map(mouseX, 0, width, 75, -75);

  sphereRadius = map(mouseY, 0, height, 50, 120);
  boxD = map(mouseY, 0, height, boxW, boxW * 4);
}