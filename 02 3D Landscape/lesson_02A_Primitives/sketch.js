'use strict';

// An array in JavaScript behaves more like a stack.
var translations = [];

function setup() {

    // WEBGL rendering needs to be specified in the
    // createCanvas(); function to switch to 3D.
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    // Unlike Processing, p5.js's 3D mode translates the origin
    // to width / 2.0, height / 2.0 automatically. Positive y-axis
    // is also inverted. Since there are many more primitives
    // in p5.js, the translations array keeps track of their
    // horizontal positions.
    for (var i = 0; i < 6; ++i) {
        translations.push(map(i, -1, 6, -width / 2.0, width / 2.0));
    }
}

function draw() {
    background(32);

    // The lights(); function in Java-based Processing
    // is not present in p5.js.
    directionalLight(128, 128, 128, 0, 0, 1);
    ambientLight(128, 128, 128);

    // Plane
    // pushMatrix(); and popMatrix(); are push(); and pop();
    push();
    translate(0, height * 0.5, 0);
    rotateX(-HALF_PI);
    // fill(); can be used on 3D materials, but is not
    // automatically shaded.
    ambientMaterial(255, 244, 214);
    plane(width, height);
    pop();

    // Box
    push();
    translate(translations[0], 0, 0);
    rotateX(frameCount * 0.01);
    ambientMaterial(255, 0, 127);
    box(30, 45, 60);
    pop();

    // Sphere
    push();
    translate(translations[1], 0, 0);
    ambientMaterial(127, 0, 255);
    sphere(30);
    pop();

    // Cylinder
    push();
    translate(translations[2], 0, 0);
    rotateX(frameCount * 0.01);
    ambientMaterial(255, 127, 0);
    cylinder(30, 50);
    pop();

    // Cone
    push();
    translate(translations[3], 0, 0);
    rotateX(frameCount * 0.01);
    ambientMaterial(127, 255, 0);
    cone(30, 50);
    pop();

    // Ellipsoid
    push();
    translate(translations[4], 0, 0);
    rotateX(frameCount * 0.01);
    ambientMaterial(0, 255, 127);
    ellipsoid(30, 45, 60);
    pop();

    // Torus
    push();
    translate(translations[5], 0, 0);
    rotateX(frameCount * 0.01);
    ambientMaterial(0, 127, 255);
    torus(20, 10);
    pop();
}
