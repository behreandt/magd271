'use strict';

var x1, y1, z1, r1,
    x2, y2, z2, r2,
    sphereDist,
    x1Dir, x1Speed,
    y1Dir, y1Speed,
    fill1, fill2;

function setup() {
    createCanvas(420, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    // Sphere 1
    r1 = random(width / 8.0, width / 5.0);
    x1 = random(r1 - width / 2.0, -r1 + height / 2.0);
    y1 = random(r1 - width / 2.0, -r1 + height / 2.0);
    z1 = random(-10, 10);
    x1Speed = 1;
    x1Dir = random(0.0, 1.0) < 0.5 ? -1 : 1;
    y1Speed = 1;
    y1Dir = random(0.0, 1.0) < 0.5 ? -1 : 1;
    fill1 = color(255, 0, 0, 127);

    // Sphere 2
    r2 = random(width / 10.0, width / 8.0);
    x2 = random(r2 - width / 2.0, -r2 + height / 2.0);
    y2 = random(r2 - width / 2.0, -r2 + height / 2.0);
    z2 = random(-10, 10);
    fill2 = color(0, 0, 255, 127);
}

function draw() {
    background(32);
    directionalLight(128, 128, 128, 0, 0, 1);
    ambientLight(128, 128, 128);

    x1 += x1Speed * x1Dir;
    if (x1 + r1 > width / 2.0 || x1 - r1 < -width / 2.0) {
        x1Dir *= -1;
    }
    y1 += y1Speed * y1Dir;
    if (y1 + r1 > height / 2.0 || y1 - r1 < -height / 2.0) {
        y1Dir *= -1;
    }

    x2 = mouseX - width / 2.0;
    y2 = mouseY - height / 2.0;

    sphereDist = dist(x1, y1, z1, x2, y2, z2);

    if (sphereDist < r1 + r2) {
        fill1 = color(255, 127, 0);
        fill2 = color(0, 127, 255);
    } else {
        fill1 = color(255, 0, 0);
        fill2 = color(0, 0, 255);
    }

    // Sphere 1
    push();
    translate(x1, y1, z1);
    specularMaterial(fill1);
    sphere(r1);
    pop();

    // Sphere 2
    push();
    translate(x2, y2, z2);
    specularMaterial(fill2);
    sphere(r2);
    pop();
}
