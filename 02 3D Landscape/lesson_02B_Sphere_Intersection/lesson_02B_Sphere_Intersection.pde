float x1, y1, z1, r1, 
  x2, y2, z2, r2, 
  sphereDist, 
  x1Dir, y1Dir;
int x1Speed, y1Speed;
color fill1, fill2;

void setup() {
  size(420, 420, P3D);
  pixelDensity(displayDensity());
  surface.setResizable(true);
  surface.setTitle("Sphere Intersection");
  background(64);
  noStroke();

  // Sphere 1
  r1 = random(width / 8.0, width / 5.0);
  x1 = random(r1, width - r1);
  y1 = random(r1, height - r1);
  z1 = random(-10, 10);
  x1Speed = 1;
  x1Dir = random(0.0, 1.0) < 0.5 ? -1 : 1;
  y1Speed = 1;
  y1Dir = random(0.0, 1.0) < 0.5 ? -1 : 1;
  fill1 = color(255, 0, 0, 127);

  // Sphere 2
  r2 = random(width / 10.0, width / 8.0);
  x2 = random(r2, width - r2);
  y2 = random(r2, height - r2);
  z2 = random(-10, 10);
  fill2 = color(0, 0, 255, 127);
}

void draw() {
  background(32);
  directionalLight(255, 245, 215, -0.5, 0.5, -0.5);

  x1 += x1Speed * x1Dir;
  if (x1 + r1 > width || x1 - r1 < 0) {
    x1Dir *= -1;
  }
  y1 += y1Speed * y1Dir;
  if (y1 + r1 > height || y1 - r1 < 0) {
    y1Dir *= -1;
  }

  x2 = mouseX;
  y2 = mouseY;

  sphereDist = dist(x1, y1, z1, x2, y2, z2);

  if (sphereDist < r1 + r2) {
    fill1 = color(255, 127, 0);
    fill2 = color(0, 127, 255);
  } else {
    fill1 = color(255, 0, 0);
    fill2 = color(0, 0, 255);
  }

  // Sphere 1
  pushMatrix();
  translate(x1, y1, z1);
  fill(fill1);
  sphere(r1);
  popMatrix();

  // Sphere 2
  pushMatrix();
  translate(x2, y2, z2);
  fill(fill2);
  sphere(r2);
  popMatrix();
}