// SOURCE & REFERENCE
// Shiffman, Spherical Geometry
// https://youtu.be/RkuBWEkBrZA

boolean showDiagnosticImages, 
  scaleTexture, offsetTexture;
int sphereDetail;
float uvScalar;

PImage back, bottom, front, left, right, top, 
  seattle;
PVector boxCenter, sphereCenter, 
  boxScale, sphereScale, 
  rotation, rotSpeed, 
  uvOff, uvSpeed, uvScl, 
  boxUV;
PVector[][] sphereVertices, sphereUV;

void setup() {
  size(840, 420, P3D);
  pixelDensity(displayDensity());
  surface.setResizable(true);
  surface.setTitle("Texturing A Mesh");
  background(64);
  noStroke();
  colorMode(RGB, 1, 1, 1);

  // The coordinates which map a 2 dimensional image
  // to the face of a 3 dimensional object are known
  // as u (the x-axis of the 2D image) and v (the
  // y-axis of the 2D image). Since an image of one
  // size and aspect could be mapped to a face of a
  // face of an entirely different size and aspect,
  // for example, a square image of 400 x 600 pixels
  // could be mapped to a triangle, it is easier to
  // to think of the texture's dimensions as 1:1.
  // This is done by specifying textureMode(NORMAL);

  textureMode(NORMAL);

  // What happens when the u v coordinates exceed 1:1?
  // If the texture is clamped, the excess space will
  // be a streak, but setting textureWrap(REPEAT) will
  // let us tile and pattern a texture.

  textureWrap(REPEAT);

  boxCenter = new PVector(width * 0.1, height / 2.0, -500);
  sphereCenter = new PVector(width * 0.9, height / 2.0, -500);
  float uniform = min(width, height) / 2.0;
  boxScale = new PVector(uniform, uniform, uniform);
  uniform *= 1.325;
  sphereScale = new PVector(uniform, uniform, uniform);
  rotation = new PVector();
  rotSpeed = new PVector(0, 0.01, 0.01);

  // Just as u and v cordinates are designed for scaling,
  // they can also be offset. Texture offsetting can be
  // used to simulate a scrolling background.

  boxUV = new PVector(1, 1);
  uvOff = new PVector(0, 0);
  uvScl = new PVector(1, 1);
  uvScalar = 1.005;

  front = loadImage("front.png");
  right = loadImage("right.png");
  back = loadImage("back.png");
  left = loadImage("left.png");
  bottom = loadImage("bottom.png");
  top = loadImage("top.png");
  seattle = loadImage("seattle.png");

  sphereDetail = 256;
  sphereVertices = new PVector[sphereDetail + 1][sphereDetail + 1];
  sphereUV = new PVector[sphereDetail + 1][sphereDetail + 1];
  float longitude = 0, latitude = 0;
  for (int i = 0; i < sphereDetail + 1; ++i) {
    latitude = map(i, 0, sphereDetail, 0, PI);
    for (int j = 0; j < sphereDetail + 1; ++j) {
      longitude = map(j, 0, sphereDetail, 0, TWO_PI);
      sphereVertices[i][j] = new PVector(
        sin(latitude) * cos(longitude), 
        sin(latitude) * sin(longitude), 
        cos(latitude));
      sphereUV[i][j] = new PVector(
        map(sin(latitude) * cos(longitude), -1, 1, 0, 1), 
        map(sin(latitude) * sin(longitude), -1, 1, 0, 1));
    }
  }
}

void draw() {
  background(0.125);
  directionalLight(1, 1, 1, 
    -0.5, 1, -0.5);
  pointLight(0.5, 0.5, 0.5, width, height, 0);

  rotation.add(rotSpeed);
  uvScl.x = map(mouseX, 0, width, 1, 4);
  uvScl.y = map(mouseX, 0, width, 1, 4);
  uvOff.x = map(mouseY, 0, height, 0, 1);
  uvOff.y = map(mouseY, 0, height, 0, 1);

  // Draw box.
  pushMatrix();
  translate(boxCenter.x, boxCenter.y, boxCenter.z);
  rotateZ(rotation.z);
  rotateX(rotation.x);
  rotateY(rotation.y);
  scale(boxScale.x, boxScale.y, boxScale.z);

  beginShape();
  texture(showDiagnosticImages ? front : seattle);
  vertex(-1, -1, 1, 
    uvOff.x, uvOff.y);
  vertex(1, -1, 1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y);
  vertex(1, 1, 1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y + boxUV.y * uvScl.y);
  vertex(-1, 1, 1, 
    uvOff.x, uvOff.y + boxUV.y * uvScl.y);
  // You can also cut shapes out of your 3D form. Just remember
  // that these need to wind counterclockwise as opposed to the
  // clockwise winding of the shape that contains them.
  beginContour();
  vertex(-0.5, 0.5, 1, 
    uvOff.x + boxUV.x * 0.25 * uvScl.x, uvOff.y + boxUV.y * 0.75 * uvScl.y);
  vertex(0.5, 0.5, 1, 
    uvOff.x + boxUV.x * 0.75 * uvScl.x, uvOff.y + boxUV.y * 0.75 * uvScl.y);
  vertex(0.5, -0.5, 1, 
    uvOff.x + boxUV.x * 0.75 * uvScl.x, uvOff.y + boxUV.y * 0.25 * uvScl.y);
  vertex(-0.5, -0.5, 1, 
    uvOff.x + boxUV.x * 0.25 * uvScl.x, uvOff.y + boxUV.y * 0.25 * uvScl.y);
  endContour();
  endShape(CLOSE);

  beginShape();
  texture(showDiagnosticImages ? right : seattle);
  vertex(1, -1, 1, 
    uvOff.x, uvOff.y);
  vertex(1, -1, -1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y);
  vertex(1, 1, -1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y + boxUV.y * uvScl.y);
  vertex(1, 1, 1, 
    uvOff.x, uvOff.y + boxUV.y * uvScl.y);
  endShape(CLOSE);

  beginShape();
  texture(showDiagnosticImages ? back : seattle);
  vertex(1, -1, -1, 
    uvOff.x, uvOff.y);
  vertex(-1, -1, -1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y);
  vertex(-1, 1, -1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y + boxUV.y * uvScl.y);
  vertex(1, 1, -1, 
    uvOff.x, uvOff.y + boxUV.y * uvScl.y);
  endShape(CLOSE);

  beginShape();
  texture(showDiagnosticImages ? left : seattle);
  vertex(-1, -1, -1, 
    uvOff.x, uvOff.y);
  vertex(-1, -1, 1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y);
  vertex(-1, 1, 1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y + boxUV.y * uvScl.y);
  vertex(-1, 1, -1, 
    uvOff.x, uvOff.y + boxUV.y * uvScl.y);
  endShape(CLOSE);

  beginShape();
  texture(showDiagnosticImages ? bottom : seattle);
  vertex(-1, 1, 1, 
    uvOff.x, uvOff.y);
  vertex(1, 1, 1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y);
  vertex(1, 1, -1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y + boxUV.y * uvScl.y);
  vertex(-1, 1, -1, 
    uvOff.x, uvOff.y + boxUV.y * uvScl.y);
  endShape(CLOSE);

  beginShape();
  texture(showDiagnosticImages ? top : seattle);
  vertex(-1, -1, -1, 
    uvOff.x, uvOff.y);
  vertex(1, -1, -1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y);
  vertex(1, -1, 1, 
    uvOff.x + boxUV.x * uvScl.x, uvOff.y + boxUV.y * uvScl.y);
  vertex(-1, -1, 1, 
    uvOff.x, uvOff.y + boxUV.y * uvScl.y);
  endShape(CLOSE);
  popMatrix();

  // Draw sphere.
  pushMatrix();
  translate(sphereCenter.x, sphereCenter.y, sphereCenter.z);
  rotateZ(rotation.z);
  rotateX(rotation.x);
  rotateY(rotation.y);
  scale(sphereScale.x, sphereScale.y, sphereScale.z);
  for (int i = 0; i < sphereDetail; ++i) {
    beginShape(TRIANGLE_STRIP);
    texture(showDiagnosticImages ? front : seattle);
    for (int j = 0; j < sphereDetail + 1; ++j) {
      vertex(sphereVertices[i][j].x, 
        sphereVertices[i][j].y, 
        sphereVertices[i][j].z, 
        uvOff.x + sphereUV[i][j].x * uvScl.x, 
        uvOff.x + sphereUV[i][j].y * uvScl.y);
      vertex(sphereVertices[i + 1][j].x, 
        sphereVertices[i + 1][j].y, 
        sphereVertices[i + 1][j].z, 
        uvOff.x + sphereUV[i + 1][j].x * uvScl.x, 
        uvOff.x + sphereUV[i + 1][j].y * uvScl.y);
    }
    endShape();
  }
  popMatrix();
}

void mouseReleased() {
  if (mouseButton == LEFT) {
    showDiagnosticImages = !showDiagnosticImages;
  }
}