'use strict';

var translations = [];
var seattle;

// External files loaded into p5.js are conventionally
// done via the preload function, which precedes setup.
// However, most browsers (excluding Mozilla Firefox)
// have a security measure which prevent a browser from
// loading a local file this way. To get around this,
// you must create a local server.
// See https://github.com/processing/p5.js/wiki/Local-server
// and Dan Shiffman's video https://youtu.be/UCHzlUiDD10 .
function preload() {
    seattle = loadImage("data/seattle.png");
}

function setup() {
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    for (var i = 0; i < 6; ++i) {
        translations.push(map(i, -1, 6, -width / 2.0, width / 2.0));
    }
}

function draw() {
    background(32);
    orbitControl();
    directionalLight(128, 128, 128, 0, 0, 1);
    ambientLight(128, 128, 128);

    // Plane
    push();
    texture(seattle);
    translate(0, height * 0.5, -200);
    rotateX(-HALF_PI);
    plane(width, height);
    pop();

    // Box
    push();
    texture(seattle);
    translate(translations[0], 0, 25);
    rotateX(frameCount * 0.01);
    box(30, 45, 60);
    pop();

    // Sphere
    push();
    texture(seattle);
    translate(translations[1], 0, 25);
    rotateX(frameCount * 0.01);
    sphere(30);
    pop();

    // Cylinder
    push();
    texture(seattle);
    translate(translations[2], 0, 25);
    rotateX(frameCount * 0.01);
    cylinder(30, 50);
    pop();

    // Cone
    push();
    texture(seattle);
    translate(translations[3], 0, 25);
    rotateX(frameCount * 0.01);
    cone(30, 50);
    pop();

    // Ellipsoid
    push();
    texture(seattle);
    translate(translations[4], 0, 25);
    rotateX(frameCount * 0.01);
    ellipsoid(30, 45, 60);
    pop();

    // Torus
    push();
    texture(seattle);
    translate(translations[5], 0, 25);
    rotateX(frameCount * 0.01);
    torus(20, 10);
    pop();
}
