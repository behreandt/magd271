'use strict';

var x1, y1, z1,
    w1, h1, d1,
    x2, y2, z2,
    w2, h2, d2,
    fill1, fill2;

function setup() {
    createCanvas(420, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    // Box 1
    x1 = -width * 0.125;
    y1 = 0;
    z1 = 10;
    w1 = 100;
    h1 = 150;
    d1 = 50;
    fill1 = color(255, 0, 0);

    // Box 2
    x2 = width * 0.125;
    y2 = 0;
    z2 = -10;
    w2 = h2 = d2 = 75;
    fill2 = color(0, 0, 255);
}

function draw() {
    background(32);
    directionalLight(128, 128, 128, 0, 0, 1);
    ambientLight(128, 128, 128);

    // Box 1
    push();
    translate(x1, y1, z1);
    ambientMaterial(fill1);
    box(w1, h1, d1);
    pop();

    // Box 2
    push();
    translate(x2, y2, z2);
    ambientMaterial(fill2);
    box(w2, h2, d2);
    pop();

    if (x1 - (w1 / 2.0) < x2 + (w2 / 2.0) &&
        x1 + (w1 / 2.0) > x2 - (w2 / 2.0) &&
        y1 - (h1 / 2.0) < y2 + (h2 / 2.0) &&
        y1 + (h1 / 2.0) > y2 - (h2 / 2.0) &&
        z1 - (d1 / 2.0) < z2 + (d2 / 2.0) &&
        z1 + (d1 / 2.0) > z2 - (d2 / 2.0)) {
        fill1 = color(255, 127, 0);
        fill2 = color(0, 127, 255);
    } else {
        fill1 = color(255, 0, 0);
        fill2 = color(0, 0, 255);
    }

    if (keyIsPressed) {
        if (keyCode == UP_ARROW) {
            y1--;
        }
        if (keyCode == LEFT_ARROW) {
            x1--;
        }
        if (keyCode == RIGHT_ARROW) {
            x1++;
        }
        if (keyCode == DOWN_ARROW) {
            y1++;
        }
        if (key == 'w' || key == 'W') {
            y2--;
        }
        if (key == 'a' || key == 'A') {
            x2--;
        }
        if (key == 's' || key == 'S') {
            y2++;
        }
        if (key == 'd' || key == 'D') {
            x2++;
        }
        if (key == 'q' || key == 'Q') {
            z2--;
        }
        if (key == 'e' || key == 'E') {
            z2++;
        }
        if (key == ',' || key == '<') {
            z1--;
        }
        if (key == '.' || key == '>') {
            z1++;
        }
    }
}
