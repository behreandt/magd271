'use strict';

var angle, camSpeed,
    x, y, z,
    lookAtX, lookAtY, lookAtZ,
    upX, upY, upZ,
    orthoScale,
    isOrthographic;

function setup() {
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    angle = 0;
    camSpeed = 2.0;
    x = 0;
    y = 0;
    z = (height / 2.0) / tan(PI * 60.0 / 360.0);
    lookAtX = x;
    lookAtY = y;
    lookAtZ = 0;
    upX = 0;
    upY = -1;
    upZ = 0;
    orthoScale = 2.0;
    isOrthographic = false;
}

function draw() {
    background(32);
    directionalLight(128, 128, 128, 0, 0, 1);
    ambientLight(128, 128, 128);

    // p5.js has a function to help move the scene around.
    // Click the mouse to rotate the scene. Upon mouse release,
    // the scene snaps back to its original camera.
    orbitControl();

    // Orthographic projection does not change the size of
    // an object based on distance.
    if (isOrthographic) {
        ortho(-width / orthoScale,
            width / orthoScale, -height / orthoScale,
            height / orthoScale,
            z / 1000.0, z * 1000.0);
    } else {
        perspective(PI / 3.0,
            float(width) / float(height),
            z / 1000.0, z * 1000.0);
    }

    // Unlike the Java-based Processing, the p5 camera
    // does not accept arguments to change its look and
    // up directions.
    camera(x, y, z);

    drawObjects();
    keyResponse();
    drawFloor(16, 500);
}

function drawObjects() {
    // Draw the sphere.
    push();
    translate(-width * 0.4, 0, -height / 2.0);
    specularMaterial(255);
    sphere(height / 2.5);
    pop();

    // Draw the box.
    push();
    translate(width * 0.4, 0, -height / 2.0);
    specularMaterial(255);
    rotateX(angle);
    rotateY(angle);
    box(height / 2.5);
    pop();

    angle += 0.01;
}

function keyResponse() {
    if (keyIsPressed) {
        if (key == 'w' || key == 'W' || keyCode == UP_ARROW) {
            y -= camSpeed;
        }
        if (key == 'a' || key == 'A' || keyCode == LEFT_ARROW) {
            x -= camSpeed;
        }
        if (key == 's' || key == 'S' || keyCode == DOWN_ARROW) {
            y += camSpeed;
        }
        if (key == 'd' || key == 'D' || keyCode == RIGHT_ARROW) {
            x += camSpeed;
        }
        if (key == 'q' || key == 'Q') {
            if (isOrthographic) {
                orthoScale += 0.01;
            }
        }
        if (key == 'e' || key == 'E') {
            if (isOrthographic) {
                orthoScale -= 0.01;
            }
        }
    }
}

function drawFloor(tiles, scale) {
    var x, z, c, min = tiles * -scale / 2.0,
        max = tiles * scale / 2.0;
    for (var i = 0; i < tiles; ++i) {
        for (var j = 0; j < tiles; ++j) {
            x = scale * 0.5 + map(i, 0, tiles, min, max);
            z = scale * 0.5 + map(j, 0, tiles, min, max);
            if (i % 2 == j % 2) {
                c = color(map(i, 0, tiles, 0, 255), map(j, 0, tiles, 0, 255), 127);
            } else {
                c = color(0, map(i, 0, tiles, 255, 127), map(j, 0, tiles, 255, 127));
            }

            // Since you can draw planes in p5, beginShape isn't needed.
            push();
            translate(x, height * 0.5, z);
            rotateX(-HALF_PI);
            ambientMaterial(c);
            plane(scale, scale);
            pop();
        }
    }
}

function keyPressed() {
    if (key == 'o' || key == 'O') {
        isOrthographic = !isOrthographic;
    }
}
