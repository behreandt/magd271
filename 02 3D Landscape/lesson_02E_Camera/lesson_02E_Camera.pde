// Since 3D requires so many variables and functions,
// an object-oriented approach is increasingly preferable.
// See the Utilities demos for cameras and lights.

float angle, camSpeed, 
  x, y, z, 
  lookAtX, lookAtY, lookAtZ, 
  upX, upY, upZ, 
  orthoScale;
boolean isOrthographic;

void setup() {
  size(640, 420, P3D);
  pixelDensity(displayDensity());
  surface.setTitle("Camera");
  background(64);
  noStroke();

  angle = 0;
  camSpeed = 2.0;
  x = width / 2.0;
  y = height / 2.0;
  z = (height / 2.0) / tan(PI * 60.0 / 360.0);
  lookAtX = x;
  lookAtY = y;
  lookAtZ = 0;
  upX = 0;
  upY = 1;
  upZ = 0;
  orthoScale = 2.0;
  isOrthographic = false;
}

void draw() {
  background(32);
  lights();

  // Orthographic projection does not change the size of
  // an object based on distance.
  if (isOrthographic) {
    ortho(-width / orthoScale, 
      width / orthoScale, 
      -height / orthoScale, 
      height / orthoScale, 
      z / 1000.0, z * 1000.0);
  } else {
    perspective(PI / 3.0, 
      float(width) / float(height), 
      z / 1000.0, z * 1000.0);
  }
  camera(x, y, z, 
    lookAtX, lookAtY, lookAtZ, 
    upX, upY, upZ);

  drawObjects();
  keyResponse();
  drawFloor(16, 500);
}

void drawObjects() {
  // Draw the sphere.
  pushMatrix();
  translate(width * 0.25, height / 2.0, -height / 1.5);
  sphere(height / 2.5);
  popMatrix();

  // Draw the box.
  pushMatrix();
  translate(width * 0.75, height / 2.0, -height / 1.5);
  rotateX(angle);
  rotateY(angle);
  box(height / 2.5);
  popMatrix();

  angle += 0.01;
}

void keyResponse() {
  if (keyPressed) {
    if (key == 'w' || key == 'W' || keyCode == UP) {
      y -= camSpeed;
    }
    if (key == 'a' || key == 'A' || keyCode == LEFT) {
      x -= camSpeed;
    }
    if (key == 's' || key == 'S' || keyCode == DOWN) {
      y += camSpeed;
    }
    if (key == 'd' || key == 'D' || keyCode == RIGHT) {
      x += camSpeed;
    }
    if (key == 'q' || key == 'Q') {
      if (isOrthographic) {
        orthoScale += 0.01;
      }
      z -= camSpeed;
    }
    if (key == 'e' || key == 'E') {
      if (isOrthographic) {
        orthoScale -= 0.01;
      }
      z += camSpeed;
    }
  }
}

void drawFloor(int tiles, float scale) {
  pushStyle();
  float x, z, min = tiles * -scale / 2.0, max = tiles * scale / 2.0;
  for (int i = 0; i < tiles; ++i) {
    for (int j = 0; j < tiles; ++j) {

      if (i % 2 == j % 2) {
        fill(map(i, 0, tiles, 0, 255), map(j, 0, tiles, 0, 255), 127);
      } else {
        fill(0, map(i, 0, tiles, 255, 127), map(j, 0, tiles, 255, 127));
      }

      x = map(i, 0, tiles, min, max);
      z = map(j, 0, tiles, min, max);

      beginShape();
      vertex(x, height, z);
      vertex(x + scale, height, z);
      vertex(x + scale, height, z + scale);
      vertex(x, height, z + scale);
      endShape(CLOSE);
    }
  }
  popStyle();
}

void mousePressed() {
  isOrthographic = !isOrthographic;
}