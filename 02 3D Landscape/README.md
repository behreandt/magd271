## 3D Landscape

![Landscape](preview.png)

### Due Date
Thursday, February 09th, 2017
S01 9:30 a.m.
S02 11:00 a.m.

### Philosophy

This project aims to introduce students to designing and scripting in 3D. It invites students to ask what skills can be transferred from 2D design into 3D and what skills must be newly learned. When coding 2D in Processing, [translation](https://processing.org/reference/translate_.html), [rotation](https://processing.org/reference/rotate_.html) and [scaling](https://processing.org/reference/scale_.html) can often be accomplished manually.

``` java
float x = 320, y = 160, w = 50, h = 60;
// ...
x += 0.01;
h -= 0.01;
ellipse(x, y, w, h);
```

 However, 3D often requires the use of the three functions in conjunction with [push-](https://processing.org/reference/pushMatrix_.html) and [pop-matrix](https://processing.org/reference/popMatrix_.html). Though not necessary to complete the assignment, if students wish to go beyond the two primitives provided out of the box ([box](https://processing.org/reference/box_.html) and [sphere](https://processing.org/reference/sphere_.html)), then they are then encouraged to learn (1) how to build meshes in local space and (2) how to wind vertices (clockwise vs. counterclockwise).

``` java
pushMatrix();
translate(center.x, center.y, center.z);
rotateZ(rotation.z);
rotateX(rotation.x);
rotateY(rotation.y);
scale(scale.x, scale.y, scale.z);
beginShape(QUADS);
vertex(-1, -1, 0, 0, 0);
vertex(1, -1, 0, 1, 0);
vertex(1, 1, 0, 1, 1);
vertex(-1, 1, 0, 0, 1);
endShape();
popMatrix();
```

 While the concept of building primitive shapes (ellipses, rectangles, triangles) into more complex arrangements carries over from 2D to 3D, rotation becomes increasingly complex (the problem of [gimbal lock](https://en.wikipedia.org/wiki/Gimbal_lock) is introduced when rotating in 3D with Euler angles).

 From the visual designer's perspective, the use of light, interactive camera and a texture which must scale, tile and distort in conformity with the mesh on which it is placed introduces new challenges to the art of composition.

### Directions

* Form a group of 2 - 3 people.
* If you want to use the utility classes from the example assignment, feel free to download them.
    * KeyListener
    * First Person Controller (requires KeyListener)
    * Box (requires Primitive)
    * Plane (requires Primitive)
    * Sphere (requires Primitive)
    * ProcGenTexture
* Create a 3D landscape.
    * Translate, rotate and scale 3D primitives.
    * Consider the surfaces of those primitives (add [texture](https://processing.org/reference/texture_.html) if necessary).
    * Group primitives into more complex arrangements.
* Create custom lighting for the environment ([lights](https://processing.org/reference/lights_.html) is the default).
* Animate the [camera](https://processing.org/reference/camera_.html)'s movement through the environment.
* Allow the viewer to take a [screen capture](https://processing.org/reference/save_.html) of the landscape. Include a sample image with your submission.

### Inspiration

* [Orihaus - _Xaxi_](https://vimeo.com/78055024)
* [The Aviator](https://tympanus.net/Tutorials/TheAviator/)
* [Rosa Menkman - _Xilitla_](https://vimeo.com/91992348)
* [William Chyr - _Manifold Garden_](https://youtu.be/BSP-e8tnXhw)
* [_Proteus_](https://youtu.be/xvnRX2np2HQ)
* [_LSD: Dream Emulator_](https://youtu.be/ol4OSIGGukA)

### Rubric

Points | Description
------ | -----------
10 | Camera is animated.
10 | Custom lighting has been used.
10 | At least 3 kinds of 3D form have been employed (cuboid, ellipsoid, plane).
10 | There are at least 5 3D forms in the landscape.
20 | Landscape on the whole is aesthetically unified, showing intentionality in choice of color, texture, lighting, camera positioning, shape, scale, figure vs. ground.
10 | Sketch includes a sample screenshot and gives the user the ability to take more.
10 | Ensure that the code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps.
10 | Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions.
10 | Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd271_assignment2_lastnames. Include the names of group members in a comment if only one person is submitting. Compress the sketch folder into a .zip file and upload to D2L.
100 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
