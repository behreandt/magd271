// SOURCE & REFERENCE:
// Shiffman, Coding Challenge #11: 3D Terrain Generation
// with Perlin Noise in Processing
// https://youtu.be/IKB1hWWedMk

boolean showGrid;
int rows, cols, octaves;
float noiseIncr, noiseDtl;
PImage txtr;
PVector scl;
PVector[][] vertices, uv;

void setup() {
  size(640, 420, P3D);
  pixelDensity(displayDensity());
  surface.setResizable(true);
  surface.setTitle("Perlin Plane");
  background(64);
  textureMode(NORMAL);
  textureWrap(REPEAT);
  colorMode(HSB, 360, 100, 100);

  // Variables to control dimensions of the plane.
  rows = 32;
  cols = 32;
  vertices = new PVector[cols][rows];
  uv = new PVector[cols][rows];

  // Variables that govern appearance.
  txtr = loadImage("terrain.png");
  showGrid = false;

  // Variables to adjust the quality of Perlin noise.
  octaves = 8;
  noiseDtl = 0.535;
  noiseIncr = 0.015;
  noiseDetail(octaves, noiseDtl);

  // xoff and yoff influence the two dimensional Perlin noise.
  float _x, _y, _z, _u, _v, _xoff = 0, _yoff = 0;
  for (int i = 0; i < cols; ++i) {
    _xoff = 0;
    for (int j = 0; j < rows; ++j) {
      _x = map(i, 0, cols, -0.5, 0.5);
      _z = map(j, 0, rows, -0.5, 0.5);
      _y = map(noise(_xoff, _yoff), 0, 1, -0.5, 0.5);
      _u = map(i, 0, cols, 0, 1);
      _v = map(j, 0, rows, 0, 1);

      vertices[i][j] = new PVector(_x, _y, _z);
      uv[i][j] = new PVector(_u, _v);

      _xoff += noiseIncr;
    }
    _yoff += noiseIncr;
  }
}

void draw() {
  background(0);
  directionalLight(45, 15, 100, -0.5, 0.5, -0.5);

  pushMatrix();
  translate(width * 0.5, height * 0.675, 0);
  rotateY(map(mouseX, 0, width, 0, TWO_PI));
  scale(width, map(mouseY, 0, height, height * 2, 0.01), width);

  if (showGrid) {
    noFill();
    strokeWeight(0.0025);
  } else {
    noStroke();
  }
  for (int j = 0; j < rows; ++j) {
    beginShape(TRIANGLE_STRIP);
    if (!showGrid) {
      texture(txtr);
    }
    for (int i = 0; i < cols; ++i) {
      if (showGrid) {
        stroke(map(vertices[i][j].y, -0.5, 0.5, 0, 360), 100, 100);
      }
      vertex(vertices[i][j], uv[i][j]);
      if (j != rows -1) {
        vertex(vertices[i][j + 1], uv[i][j + 1]);
      }
    }
    endShape(CLOSE);
  }
  popMatrix();
}

void vertex(PVector v, PVector u) {
  vertex(v.x, v.y, v.z, u.x, u.y);
}

void mousePressed() {
  if(mouseButton == LEFT) {
  showGrid = !showGrid;
  } else if(mouseButton == RIGHT) {
    setup();
  }
}