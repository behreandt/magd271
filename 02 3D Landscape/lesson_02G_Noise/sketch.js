'use strict';

var rows, cols,
    vertices, uv, noiseIncr;

function setup() {
    createCanvas(640, 420, WEBGL);
    pixelDensity(displayDensity());
    background(64);

    rows = 32;
    cols = 32;
    vertices = [];
    uv = [];
    noiseIncr = 0.015;

    var _x, _y, _z, _u, _v, _xoff = 0,
        _yoff = 0;
    for (var i = 0; i < cols; ++i) {
        _xoff = 0;
        vertices.push([]);
        uv.push([]);
        for (var j = 0; j < rows; ++j) {
            _x = map(i, 0, cols, -0.5, 0.5);
            _z = map(j, 0, rows, -0.5, 0.5);
            _y = map(noise(_xoff, _yoff), 0, 1, 0.5, -0.5);
            _u = map(i, 0, cols, 0, 1);
            _v = map(j, 0, rows, 0, 1);
            vertices[i][j] = createVector(_x, _y, _z);
            uv[i][j] = createVector(_u, _v);
            _xoff += noiseIncr;
        }
        _yoff += noiseIncr;
    }
}

function draw() {
    background(32);
    orbitControl();
    ambientLight(128, 128, 128);

    push();
    noStroke();
    translate(0, 0, 0);
    scale(width, width, width);

    for (var j = 0; j < rows; ++j) {
        beginShape(TRIANGLE_STRIP);
        specularMaterial(map(j, 0, rows, 0, 255), 0, 127);
        for (var i = 0; i < cols; ++i) {
            vertex(vertices[i][j].x,
                vertices[i][j].y,
                vertices[i][j].z);
            if (j != rows - 1) {
                vertex(vertices[i][j + 1].x,
                    vertices[i][j + 1].y,
                    vertices[i][j + 1].z);
            }
        }
        endShape(CLOSE);
    }
    pop();
}
