## Project Design Document

### Due Dates
Thursday, April 20, 2017
S01 9:30 a.m.
S02 11:00 a.m.

### Philosophy

The final project is the culmination of the goals and philosophy of the course, as outlined in the syllabus. Since the final project involves a longer development time, multiple stakeholders and aims to create a more complex artwork, it is important to plan the development process in advance. This allows all stakeholders to be honest and clear with each other about what the project entails and what their role in the project might be.

In a professional environment, a design document may be _expected_ by funders, clients and managers before the project is even green-lit. However, it is important for students to recognize two key factors in authoring a design document: (1) that it is a [living document](https://en.wikipedia.org/wiki/Living_document), meaning that as the project evolves, unexpected problems arise, compromises are made, etc., the document's initial form may eventually be substantively revised or even ignored; (2) new media practice (including that of video game development) exist in a gray area between the professional and nonprofessional, meaning there is no one template which will suit all needs. Just because a design document is not permanent or codified does not mean that the exercise of writing it is not crucial as an act of organizing and communicating thought.

### Directions

* Work either individually or in groups of up to 4 people.
* Select from the following development possibilities:
    * Processing
    * JavaScript libraries that facilitate animation and interactivity in HTML5 canvas (e.g., [p5.js](https://p5js.org/), [anime.js](http://anime-js.com/), [matter.js](http://brm.io/matter-js/), [mo.js](http://mojs.io/), [three.js](https://threejs.org/)).
    * Scalable Vector Graphics (.svg) animated with SMIL, JS or CSS3.
* Give your final project a working name.
* Author a design document, to be submitted in a easily-readable file format.
    * Rich Text Format (.rtf)
    * Portable Document Format (.pdf)
    * Hypertext Markup Language (.html)
    * Document (.doc)
* Include the project name and group members at the top of the document.
* Write a one- to two-paragraph [executive summary](https://en.wikipedia.org/wiki/Executive_summary) of the project.
* Write an ordered list of at least seven features, written in complete sentences, in ascending order from most essential (1) to least essential (7).
    * Emphasize concreteness and specificity over abstraction and generality. For example, "The avatar will move based on the vertical and horizontal position of the user's hand as registered by Leap-Motion" is preferable to "It will be interactive".
* For each group member, specify the role he or she will play in the group.
    * List 3 skills, written in complete sentences, that the group member already possesses which qualify him or her to play the above role.
    * List 2 skills, written in complete sentences, that the group member knows in advance or he or she will have to research, acquire and/or improve during the course of development.
* Specify a plan for [version control](https://en.wikipedia.org/wiki/Version_control) and [task management](https://en.wikipedia.org/wiki/Task_management). For example, a group may decide to use Trello to keep a running to-do list and Google Drive to store assets and older versions.
    * For groups with more than more than one member, include a plan for communication (e.g., Skype, email, Facebook messenger).
* Include at least one visual with the document. This could be a piece of concept art, a diagram of user interaction flow, or a diagram of code structure.

### Inspiration

* [Daniel Schubert - Writing Great Design Documents](http://www.gdcvault.com/play/581/Writing-Great-Design)
* [Alicia Fortier - Morphler Design Document](http://aliciafortier.com/wp-content/uploads/2014/04/MorphlersGameDesignDocument.pdf)
* [Diablo Pitch Document](http://www.graybeardgames.com/download/diablo_pitch.pdf)

### Rubric

Points | Description
------ | -----------
10 | Document is named according to the convention s17_magd271_projectname_lastnames and is submitted as an acceptable format (.rtf, .pdf, .html, .doc) to the dropbox on D2L.
10 | Document specifies name of project and group members at the top.
10 | Executive summary is one- to two- paragraphs.
10 | An ordered list describes key features of the paragraph from most to least essential.
15 | A list of group members specifies their role, three skills they possess and two skills they plan to acquire in development.
15 | A version control plan describes how key assets and previous versions of the project will be stored. For groups, a brief summary of planned communication is included.
15 | At least one visual is included.
15 | The document is written in complete sentences, using standard English spelling and grammar. The document privileges the specific and concrete over the abstract and general.
100 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
