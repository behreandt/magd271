'use strict';

// For any slice in a pie chart, an arc needs to be drawn from a starting angle
// to an ending angle. If a label is to be drawn over the slice of the pie, then
// it would also be helpful to find the middle of the slice as well, here calculated
// by converting the percentage (val) to an angle, dividing by half and then adding
// to the start angle.
function Slice(lbl, val, str, stp, fll) {
    this.label = lbl;
    this.value = val;
    this.start = constrain(str, 0, TWO_PI);
    this.stop = constrain(stp, 0, TWO_PI);

    // To find a good place to display the label, we want to locate the middle
    // of the slice. This is done by calculating the start + half the arc. To
    // get half the arc, we take this.value, which is a percentage, remap
    // it to (0, TWO_PI), then divide by 2.
    this.middle = this.start + map(this.value, 0, 1, 0, TWO_PI) * 0.5;
    this.fllNoFocus = color(fll, 99, 59);
    this.fllFocus = color(fll, 99, 99);
    this.fll = color(fll, 99, 59);
    this.hasFocus = false;

    // How long it takes for the color to fade in and out.
    this.focusTransition = 0.05;
}

Slice.prototype.toString = function() {
  return this.label + ": " + (this.value * 100).toFixed(2) + "%";
}

Slice.prototype.draw = function(x, y, rad) {
    if (this.hasFocus) {
        this.fll = lerpColor(this.fll, this.fllFocus, this.focusTransition);
    } else {
        this.fll = lerpColor(this.fll, this.fllNoFocus, this.focusTransition);
    }
    fill(this.fll);
    arc(x, y, rad, rad,
        this.start, this.stop);
}

// atan2 returns a value in the range of -PI to PI.
// If the angle is less than 0 radians, we can fix the
// problem by adding TWO_PI to it.
Slice.prototype.hover = function(a) {
    a = a < 0 ? a + TWO_PI : a;
    return a > this.start && a < this.stop;
}

Slice.prototype.showLabel = function(x, y, rad, val) {
  fill(0, 0, 99);
  var pop = val * this.value;
  text(this + "\nEstimated population: " + round(pop),
    x + cos(this.middle) * rad * 0.5,
    y + sin(this.middle) * rad * 0.5);
}
