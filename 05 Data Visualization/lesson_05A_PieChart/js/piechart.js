'use strict';

function PieChart(x, y, ethn, lbl, pop) {
    this.x = x;
    this.y = y;
    this.radius = width / 8.0;
    this.ethnicity = ethn;
    this.population = pop;
    this.label = lbl;
    this.hasFocus = false;

    // In newer browsers, you can take an object and convert it into
    // an array of keys and an array of values using Object.keys and
    // and Object.values. However, if this feature is not available,
    // then it needs to be done manually.
    // var ethnicityLabels = Object.keys(this.ethnicity);
    // var ethnicityPercentages = Object.values(this.ethnicity);

    var ethnicityLabels = ['AmericanIndian', 'Asian', 'Black', 'Hispanic', 'NativeHawaiian', 'NonResidentAlien', 'TwoOrMore', 'Unknown', 'White']

    var ethnicityPercentages = [this.ethnicity.AmericanIndian, this.ethnicity.Asian, this.ethnicity.Black, this.ethnicity.Hispanic, this.ethnicity.NativeHawaiian, this.ethnicity.NonResidentAlien, this.ethnicity.TwoOrMore, this.ethnicity.Unknown, this.ethnicity.White]

    // Acquire the number of keys in the Ethnicity object.
    var sliceCount = ethnicityLabels.length;

    this.slices = [];
    for (var i = 0; i < sliceCount; ++i) {

        // If this is the first slice in the pie, then it starts
        // at 0 radians of rotation. If it is a later slice, then
        // it has to position itself relative to the slices which
        // have preceded it.
        var st = i === 0 ? 0 : this.slices[i - 1].stop;

        // The slice needs to convert the percentage of a population
        // into a slice of pie, hence the map to the range 0 - TWO_PI.
        this.slices.push(new Slice(ethnicityLabels[i], /* Label */
            ethnicityPercentages[i], /* Value */
            st, /* Start angle */
            st + map(ethnicityPercentages[i], 0, 1, 0, TWO_PI), /* Stop angle */
            map(i, 0, sliceCount, 0, 360))); /* Fill hue */
    }
}

PieChart.prototype.draw = function() {
    push();
    noStroke();
    ellipseMode(RADIUS);
    textAlign(CENTER, TOP);
    for (var i = 0, size = this.slices.length; i < size; ++i) {
        this.slices[i].draw(this.x, this.y, this.radius);
    }
    fill(0, 0, 99);
    if (this.hasFocus) {
        text(this.label +
            "\nUndergrad Population: " + this.population,
            this.x, this.y + this.radius);
    } else {
        text(this.label, this.x, this.y + this.radius);
    }
    pop();
    this.hover(mouseX, mouseY);
}

PieChart.prototype.hover = function(x, y) {
    if (dist(x, y, this.x, this.y) < this.radius) {
        this.hasFocus = true;
        for (var i = 0, size = this.slices.length; i < size; ++i) {
            var a = atan2(y - this.y, x - this.x);
            if (this.slices[i].hover(a)) {
                this.slices[i].hasFocus = true;
                this.slices[i].showLabel(this.x, this.y,
                    this.radius, this.population);
            } else {
                this.slices[i].hasFocus = false;
            }
        }
    } else {
        this.hasFocus = false;
        for (var i = 0, size = this.slices.length; i < size; ++i) {
            this.slices[i].hasFocus = false;
        }
    }
}
