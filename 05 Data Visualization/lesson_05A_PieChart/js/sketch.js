'use strict';

var cnvs, scorecard, ready,
    uwPie, uwmPie, uwwPie;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    colorMode(HSB, 359, 99, 99);

    // Do not show anything in draw until the .csv file has been loaded
    // and the callback function below has been called.
    ready = false;

    scorecard = loadTable('assets/scorecard.csv', 'csv', 'header', function() {
        console.log('Table has been loaded.');

        // Get table rows.
        var uw = scorecard.getRow(4165);
        var uwm = scorecard.getRow(4166);
        var uww = scorecard.getRow(4152);

        // Create ethnicity objects from the tableRow objects.
        var uwEth = new Ethnicity(
            uw.getNum('UGDS_WHITE'),
            uw.getNum('UGDS_BLACK'),
            uw.getNum('UGDS_HISP'),
            uw.getNum('UGDS_ASIAN'),
            uw.getNum('UGDS_AIAN'),
            uw.getNum('UGDS_NHPI'),
            uw.getNum('UGDS_2MOR'),
            uw.getNum('UGDS_NRA'),
            uw.getNum('UGDS_UNKN')
        );
        var uwmEth = new Ethnicity(
            uwm.getNum('UGDS_WHITE'),
            uwm.getNum('UGDS_BLACK'),
            uwm.getNum('UGDS_HISP'),
            uwm.getNum('UGDS_ASIAN'),
            uwm.getNum('UGDS_AIAN'),
            uwm.getNum('UGDS_NHPI'),
            uwm.getNum('UGDS_2MOR'),
            uwm.getNum('UGDS_NRA'),
            uwm.getNum('UGDS_UNKN')
        );
        var uwwEth = new Ethnicity(
            uww.getNum('UGDS_WHITE'),
            uww.getNum('UGDS_BLACK'),
            uww.getNum('UGDS_HISP'),
            uww.getNum('UGDS_ASIAN'),
            uww.getNum('UGDS_AIAN'),
            uww.getNum('UGDS_NHPI'),
            uww.getNum('UGDS_2MOR'),
            uww.getNum('UGDS_NRA'),
            uww.getNum('UGDS_UNKN')
        );

        // Create piechart objects. The goal here is to retain
        // large objects in memory only so long as we need them.
        uwPie = new PieChart(width * 0.25, height * 0.25,
            uwEth, uw.get('INSTNM'), uw.getNum('UGDS'));
        uwmPie = new PieChart(width * 0.5, height * 0.75,
            uwmEth, uwm.get('INSTNM'), uwm.getNum('UGDS'));
        uwwPie = new PieChart(width * 0.75, height * 0.25,
            uwwEth, uww.get('INSTNM'), uww.getNum('UGDS'));

        ready = true;
    });
}

function draw() {
    background(0, 0, 12);

    if (ready) {
        uwPie.draw();
        uwmPie.draw();
        uwwPie.draw();
    }

    // Show title.
    push();
    textSize(24);
    textAlign(CENTER, TOP);
    fill(0, 0, 99);
    text('Demographics for Three Major Wisconsin Colleges', width * 0.5, 5);
    pop();

    // Show source.
    push();
    textAlign(RIGHT, BOTTOM);
    fill(0, 0, 99);
    text('Source: https://catalog.data.gov/dataset/college-scorecard', width - 5, height - 5);
    pop();
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
