'use strict';

// When attempting to convert a JavaScript object to an array
// which contains either its keys (the variable names) or values
// (assigned to the variables), note that JavaScript organizes
// its variables alphabetically.
function Ethnicity(w, b, h, as, ai, nh, t, nr, u) {
    this.AmericanIndian = ai; /* American Indian / Alaska Native */
    this.Asian = as;
    this.Black = b;
    this.Hispanic = h;
    this.NativeHawaiian = nh; /* Native Hawaiian / Pacific Islander */
    this.NonResidentAlien = nr; /* Non-resident alien */
    this.TwoOrMore = t; /* Two or more races */
    this.Unknown = u;
    this.White = w;
}
