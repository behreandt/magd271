'use strict';

var frameCount, paused,
    includeHeaders, headers, data, format,
    tableContainer, svg, bkgImage;

// This simulates an enumeration.
var ParseAs = {
    Integer: 0,
    Decimal: 1,
    Bool: 2,
    Phrase: 3,
    Dollar: 4
};

// When all the HTML on index.html is finished loading,
// then call the function setup defined below.
window.onload = setup;

function setup() {
    frameCount = 0;
    paused = false;
    includeHeaders = true;
    format = [ParseAs.Integer, /* ID */
        ParseAs.Text, /* FIRSTNAME */
        ParseAs.Text, /* LASTNAME */
        ParseAs.Integer, /* AGE */
        ParseAs.Dollar, /* HOURLYPAY */
        ParseAs.Bool /* FULLTIME */
    ];
    tableContainer = document.getElementById('tableContainer');
    svg = document.getElementById('barGraph');
    svg.setAttributeNS(null, 'width', window.innerWidth);
    svg.setAttributeNS(null, 'height', window.innerHeight);
    svg.setAttributeNS(null, 'viewBox', '0 0 ' + window.innerWidth + ' ' + window.innerHeight);
    bkgImage = svg.getElementById('bkgImage');
    bkgImage.setAttributeNS(null, 'width', window.innerWidth);
    bkgImage.setAttributeNS(null, 'height', window.innerHeight);
    bkgImage.setAttributeNS(null, 'viewBox', '0 0 ' + window.innerWidth + ' ' + window.innerHeight);
    // Create an HTTP request.
    var request = new XMLHttpRequest();

    // Assign a function to handle the results of the request
    // when it changes a state (to finished).
    request.onreadystatechange = processResults;

    // Open the request.
    request.open('GET', 'assets/example.csv', true);

    // Send the request.
    request.send();
}

function processResults() {
    // See https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/readyState , https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
    if (this.readyState == 4 && this.status == 200) {

        // Log that the request worked.
        console.log('Successfully loaded csv file.');

        // Assign the response to raw data.
        var rawData = this.responseText;

        // Break down the data into rows & columns.
        // The third piece of information is a list of the desired
        // formatting for each column. This information is optional.
        data = parseCSV(rawData, includeHeaders, format);
        console.log(data);

        // Create a function for data which makes getting a column easier.
        data.column = function(c) {
            var result = [];
            for (var i = 0, size = this.length; i < size; ++i) {
                result.push(this[i][c]);
            }
            return result;
        }

        createTable(data, headers, format, tableContainer);
        createBarGraph(svg, data.column(4), data.column(1));

        window.onresize = function(e) {
            svg.setAttributeNS(null, 'width', window.innerWidth);
            svg.setAttributeNS(null, 'height', window.innerHeight);
            svg.setAttributeNS(null, 'viewBox', '0 0 ' + window.innerWidth + ' ' + window.innerHeight);
            bkgImage.setAttributeNS(null, 'width', window.innerWidth);
            bkgImage.setAttributeNS(null, 'height', window.innerHeight);
            bkgImage.setAttributeNS(null, 'viewBox', '0 0 ' + window.innerWidth + ' ' + window.innerHeight);
            updateBarGraph(svg, data.column(4), data.column(1));
        }
    }
}

function parseCSV(raw, includeHead, format) {

    // Specify that the result of this function will be an array
    // using square brackets [].
    var result = [];

    // Split the raw data into rows based on carriage return or
    // line feed or both.
    var rows = raw.split(/[\r?\n|\r|\n]+/);

    // If include headers, retrieve that information.
    if (includeHead) {
        headers = rows[0].split(/,\s*/);
    }

    // Go through each row of the table.
    for (var i = includeHead ? 1 : 0, size = rows.length; i < size; ++i) {

        // If the row is not blank...
        if (rows[i] !== "") {

            // If we leave the formatting undefined or if wanted to include
            // headers and we are on row 0, i.e. the header row.
            if (format === undefined) {

                // Split each row into separate columns based on a
                // comma followed by any number of spaces.
                result.push(rows[i].split(/,\s*/));
            } else {

                // Split the row into columns.
                var cols = rows[i].split(/,\s*/);

                // Create a temporary array to separate the parsed information
                // from the raw information.
                var parsed = [];

                // Loop through all the columns.
                for (var j = 0, size2 = cols.length; j < size2; ++j) {
                    // Check the format array to see how the requester
                    // wanted this column to be formatted.
                    if (format[j] === ParseAs.Integer) {
                        parsed.push(parseInt(cols[j]));
                    } else if (format[j] === ParseAs.Decimal || format[j] === ParseAs.Dollar) {
                        parsed.push(parseFloat(cols[j]));
                    } else if (format[j] === ParseAs.Bool) {
                        parsed.push(cols[j].toLowerCase() === 'true');
                    } else {
                        parsed.push(cols[j]);
                    }
                }

                // Add the parsed columns to the results.
                result.push(parsed);
            }
        }
    }

    // Return the result, which will be an array of arrays.
    return result;
}

function createTable(data, headers, format, container) {
    // Create an HTML DOM element by specifying the type.
    var table = document.createElement('table');
    table.id = 'dataTable';

    if (includeHeaders) {
        var header = document.createElement('tr');
        header.id = "header";
        for (var i = 0, size = headers.length; i < size; ++i) {

            // Create a header cell.
            var col = document.createElement('th');

            // Set the id and content to the header.
            col.textContent = col.id = headers[i];

            // Create a button which will sort the column
            // in ascending order.
            var sortAscendingButton = document.createElement('input');
            sortAscendingButton.type = 'button';
            sortAscendingButton.value = '+';

            // Set the button's id to a number which corresponds
            // to the column. When the button is pressed, this
            // number will be used to determine which column by
            // which the sort happens.
            sortAscendingButton.id = i;

            // Define the function which will happen when the
            // button is clicked.
            sortAscendingButton.onclick = function(e) {
                data.sort(function(a, b) {
                    var col = parseInt(e.srcElement.id);
                    if (a[col] === b[col]) {
                        return 0;
                    } else if (a[col] < b[col]) {
                        return -1;
                    } else {
                        return 1;
                    }
                });

                // Update the visual representation on the page
                // once the data is reorganized internally.
                updateTable(format);
            }
            col.appendChild(sortAscendingButton);

            // Create a button which will sort the column
            // in descending order.
            var sortDescendingButton = document.createElement('input');
            sortDescendingButton.type = 'button';
            sortDescendingButton.value = '-';
            sortDescendingButton.id = i;
            sortDescendingButton.onclick = function(e) {
                data.sort(function(a, b) {
                    var col = parseInt(e.srcElement.id);
                    if (a[col] === b[col]) {
                        return 0;
                    } else if (a[col] < b[col]) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
                updateTable(format);
            }
            col.appendChild(sortDescendingButton);

            header.appendChild(col);
        }
        table.appendChild(header);
    }

    // Go through all the rows.
    for (var i = 0, rows = data.length; i < rows; ++i) {
        // Create a table row.
        var row = document.createElement('tr');

        // Give the row an id so it can be accessed later.
        row.id = 'row(' + i + ')';

        // Go through all the columns.
        for (var j = 0, cols = data[i].length; j < cols; ++j) {
            var col = document.createElement('td');
            col.id = 'cell(' + j + ',' + i + ')';
            if (format[j] === ParseAs.Integer) {
                col.textContent = data[i][j];
                col.setAttribute('align', 'right');
            } else if (format[j] === ParseAs.Decimal) {
                col.textContent = data[i][j];
                col.setAttribute('align', 'right');
            } else if (format[j] === ParseAs.Dollar) {
                col.textContent = '$' + data[i][j].toFixed(2);
                col.setAttribute('align', 'right');
            } else if (format[j] === ParseAs.Bool) {
                col.textContent = data[i][j];
                col.style.textTransform = 'capitalize';
            } else {
                col.textContent = data[i][j];
            }

            // Add the column to the row.
            row.appendChild(col);
        }

        // Add the row to the table.
        table.appendChild(row);
    }

    // If container has been specified by the requester, // then append the table to the container.
    // If not, then append the table to the document body.
    if (container !== undefined) {
        container.appendChild(table);
    } else {
        document.body.appendChild(table);
    }
}

function updateTable(format) {

    // Get the table from the HTML.
    var table = document.getElementById('dataTable');

    // Loop through the rows.
    for (var i = 0, rows = data.length; i < rows; ++i) {

        // Loop through the columns.
        for (var j = 0, cols = data[i].length; j < cols; ++j) {

            // Find the cell by its id.
            var query = 'cell(' + j + ',' + i + ')';
            var col = document.getElementById(query);

            // Based on the desired formatting, set the content of the cell.
            if (format[j] === ParseAs.Integer) {
                col.textContent = data[i][j];
                col.setAttribute('align', 'right');
            } else if (format[j] === ParseAs.Decimal) {
                col.textContent = data[i][j];
                col.setAttribute('align', 'right');
            } else if (format[j] === ParseAs.Dollar) {
                col.textContent = '$' + data[i][j].toFixed(2);
                col.setAttribute('align', 'right');
            } else if (format[j] === ParseAs.Bool) {
                col.textContent = data[i][j];
                col.style.textTransform = 'capitalize';
            } else {
                col.textContent = data[i][j];
            }
        }
    }
}

function createBarGraph(svg, data, labels) {

    // Get the mask from the SVG's definitions.
    var mask = svg.getElementById('mask');

    // Calculate the maximum value of the data supplied.
    var max = Math.max.apply(null, data);

    // Get the rectangle size of the SVG.
    var bounds = svg.getBoundingClientRect();

    // This is the namespace of the SVG which is included
    // When a rectangle is created.
    var spec = 'http://www.w3.org/2000/svg';

    // How much space is given between each bar is specified
    // by a scalar between 0 (no width) and 1 (no space).
    var barScale = 0.85;
    var size = data.length;

    // Specify the position and scale of the bars.
    var h = 100;
    var w = barScale * (bounds.width / size);
    var y = bounds.bottom - h;
    var bar1Margin = bounds.left + (w * (1 - barScale) * 0.5);
    var x = bar1Margin;

    // Iterate over the data.
    for (var i = 0; i < size; ++i) {
        // Convert the data to a percentage by dividing the current
        // value by max, then multiply that percent by the height
        // of the bounding box.
        h = (data[i] / max) * bounds.height;
        x = map(i, 0, size, bar1Margin, bounds.right);
        y = bounds.bottom - h;

        // Create a rectangle path in the SVG. Set its position.
        var rect = document.createElementNS(spec, 'rect');
        if (labels) {
            rect.setAttributeNS(null, 'id', labels[i]);
        } else {
            rect.setAttributeNS(null, 'id', 'bar' + i);
        }
        rect.setAttributeNS(null, 'class', 'bar');
        rect.setAttributeNS(null, 'x', x);
        rect.setAttributeNS(null, 'y', y);

        // The fill and width of the rectangles could be specified as attributes,
        // as above, or they could be specified by CSS.
        rect.style.width = w + 'px';
        rect.style.height = h + 'px';

        // In case the mask cannot be found, create a backup plan.
        if (mask) {

            // Use the gradient defined in the SVG.
            rect.setAttributeNS(null, 'fill', 'url(#gradient)');

            // Add the rectangle to the mask.
            mask.appendChild(rect);
        } else {

            // Color the rectangle with CSS instead.
            rect.style.fill = 'rgba(0, 127, 255, 0.86)';

            // Add the rectangle to the SVG instead.
            svg.appendChild(rect);
        }
        if (labels) {
            var txt = document.createElementNS(spec, 'text');
            txt.setAttributeNS(null, 'text-anchor', 'middle');
            txt.setAttributeNS(null, 'x', x + w / 2);
            txt.setAttributeNS(null, 'y', bounds.bottom - h / 2);
            txt.setAttributeNS(null, 'class', 'txt' + i);
            txt.setAttributeNS(null, 'fill', 'rgb(255, 255, 204)');
            txt.setAttributeNS(null, 'class', 'label');
            txt.textContent = labels[i];
            svg.appendChild(txt);
        }
    }
}

function updateBarGraph(svg, data, lbls) {
    var bars = svg.getElementsByClassName('bar');
    if (lbls) {
        var labels = svg.getElementsByClassName('label');
    }
    var size = bars.length;
    var barScale = 0.85;
    var max = Math.max.apply(null, data);
    var bounds = svg.getBoundingClientRect();
    var w = barScale * (bounds.width / size);
    var bar1Margin = (w * (1 - barScale) * 0.5);
    var h = 100;
    var y = 0;
    var x = bar1Margin;
    for (var i = 0; i < size; ++i) {
        h = (data[i] / max) * bounds.height;
        x = map(i, 0, size, bar1Margin, bounds.right);
        y = bounds.bottom - h;
        bars[i].setAttributeNS(null, 'x', x);
        bars[i].setAttributeNS(null, 'y', y);
        bars[i].style.width = w + 'px';
        bars[i].style.height = h + 'px';
        if (lbls) {
            labels[i].setAttributeNS(null, 'x', x + w / 2);
            labels[i].setAttributeNS(null, 'y', bounds.bottom - h / 2);
            labels[i].textContent = lbls[i];
        }
    }
}

// From the map function in Processing and p5.js.
function map(value, lbOrigin, ubOrigin, lbTarget, ubTarget) {
    return ((value - lbOrigin) / (ubOrigin - lbOrigin)) * (ubTarget - lbTarget) + lbTarget;
}
