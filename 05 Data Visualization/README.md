## Data Visualization

![Data Visualization](preview.png)

### Due Dates

S01 9:30 a.m. on Tuesday, April 11, 2017

S02 11:59 p.m. on Thursday, April 6, 2017

### Philosophy

Crucial to interaction design is the ability to provide the user, viewer or player information relevant to time-sensitive choices he or she will have to make. This means that potentially complex data is to be selected, interpreted, and displayed. This is the case for newspapers discussing matters of public interest as well as for fictive data of video games (hit points, experience points, coins).

In video games, communications and new media art, data persistence is a key concern, i.e., how do we retain and share information across different applications and programming environments? Data always threatens to overwhelm a public with its volume and opacity. Data is always under threat of disappearing, of being corrupted in attempts to reformat it, and of being either ignored for its complexity or distorted in attempts to simplify and present it.

For user-experience designers, this means imagining yourself in the user's shoes and asking what information will be of interest to them. For programmers, this means developing skills in file input and output, parsing and data structuring. For visual designers, this means applying rules of color harmony, scaling and layout to a more analytical purpose in that bar or pie charts need to be clear and easily legible.

In terms of process, you are encouraged to recognize the importance of contributing to collective effort. This means not only working in a group where you can choose your partners, taking note that your choice of partners is a reflection of your own judgment, but also working in a group where your co-workers have not been chosen. In the latter case, your capacity to quickly establish new lines of communication and to adjust your identity, role, and workflow in response to new group dynamics will prove invaluable.

### Directions

* Work in a group of 2 - 4 people, to be randomly chosen.
* Select a source of data collected from the real world.
    * p5.js has tools to load and parse the following file formats:
        * [.txt (text)](http://p5js.org/reference/#/p5/loadStrings)
        * [.csv (Comma Separated Values)](http://p5js.org/reference/#/p5/loadTable)
        * [.xml (Extensible Markup Language)](http://p5js.org/reference/#/p5/loadXML)
        * [.json (JavaScript Object Notation)](http://p5js.org/reference/#/p5/loadJSON)
    * Examples of real world data for your consideration:
        * [data.gov](https://www.data.gov/)
        * [CIA World Factbook](https://www.cia.gov/library/publications/the-world-factbook/)
        * [Project Gutenberg](https://www.gutenberg.org/)
        * [Darius Kazemi, Corpora](https://github.com/dariusk/corpora)
            * .json format.
        * UW-Whitewater
            * [Institutional Research & Planning: Facts & Figures](http://www.uww.edu/irp/)
            * [Motion Capture Studio](http://blogs.uww.edu/mocap/mocap-data/)
                * .xml files available upon request.
                * .bvh files are a kind of text file, but would need to be parsed manually.
        * [UNICEF](https://data.unicef.org)
            * .xlsx files are common. Some can be _imperfectly_ converted to .csv files by Microsoft Excel.
        * [World Happiness Report](http://worldhappiness.report/)
* Clean and parse the data.
    * If the data is in a file format that is not supported by the library you are using, investigate how you can convert it.
    * If the data contains null or empty values, or values which mix numeric and non-numeric information (e.g., '$30,000.00' would need to be converted to 30000), develop a policy for how to interpret those values.
* Represent and organize the data in JavaScript. Derive information from the data. For example, sum, calculate percentages, find mean, median or mode.
    * Create [objects](https://www.w3schools.com/js/js_object_definition.asp) and [arrays](https://www.w3schools.com/js/js_arrays.asp) if necessary.
* Design an _interactive_ visualization of the data to be viewed in the browser. Examples:
    * Scatter plot
    * Line chart
    * Bar graph
    * Pie chart
    * Word cloud
    * Timeline
* Write a one- to two-page (single-spaced) post-mortem report and submit to D2L.
    * A one paragraph reflection of what you learned on the project and what you would do differently if given a similar project on data visualization in the future.
    * A one paragraph summary of what you contributed to the project.
    * A brief summary of your interpretation of the contributions of your partners.
    * A ranking of each partners' contribution to the project on a scale of 0 to 50, where 0 means minimal and unsatisfactory participation and 50 means substantial and satisfactory participation.

### Inspiration

* Edward Tufte
    * _Envisioning Information_ (P 93.5 .T84 1990)
    * _Visual Explanations_ (P 93.5 .T846 1997)
    * _Beautiful Evidence_ (P 93.5 T.837 2006)
* [d3.js](https://d3js.org/)
* [Mike Bostock](https://bost.ocks.org/mike/)
* [Jer Thorpe](http://blog.blprnt.com/selected-works)
* [Lev Manovich](http://manovich.net/index.php/exhibitions)
* [Darius Kazemi](http://tinysubversions.com/projects/)
* [Daniel Shiffman, Data, APIs and Language Processing Libraries](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a343yZ_JcPzIric4SOGqMZ)
* Shiffman, Mapping Earthquake Data: [part 1](https://youtu.be/ZiYdOwOrGyc) , [part 2](https://youtu.be/dbs4IYGfAXc)
* [Lynda video: Interactive Data Visualization with Processing](https://www.lynda.com/Processing-tutorials/Interactive-Data-Visualization-Processing/97578-2.html?org=uww.edu) (note the release date of 09/25/2012)

### Rubric

Points | Description
------ | -----------
50 | A post-mortem report with a reflection on your approach, a summary of your contributions, a summary of your partners' contributions is submitted as a document to D2L.
50 | The average of your partners' peer evaluation on the post-mortem report.
30 | The data has been imported, parsed and organized in working memory. The data has been manipulated in some manner (sorting, shuffling, averaging, summing, etc.).
30 | The data is visualized in a manner reflective of graphic design principles (hierarchy, clarity of meaning, color harmony, grouping of form).
20 | The visualization reacts to user input (keyboard, mouse, etc.).
10 | The webpage runs without errors.
10 | All files are contained in a folder, which is named according to the following convention: s17_magd271_assignment5_lastnames. The folder has been compressed into a .zip file and upload to D2L.
200 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
