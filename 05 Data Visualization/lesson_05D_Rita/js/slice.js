function Slice(lbl, val, str, stp, fll) {
    this.label = lbl;
    this.value = val;
    this.start = constrain(str, 0, TWO_PI);
    this.stop = constrain(stp, 0, TWO_PI);
    this.middle = this.start + map(this.value, 0, 1, 0, TWO_PI) * 0.5;
    this.fllNoFocus = color(fll, 99, 59);
    this.fllFocus = color(fll, 99, 99);
    this.fll = color(fll, 99, 59);
    this.hasFocus = false;
    this.focusTransition = 0.05;
}

Slice.prototype.toString = function() {
  return this.label + ": " + (this.value * 100).toFixed(2) + "%";
}

Slice.prototype.draw = function(x, y, rad) {
    if (this.hasFocus) {
        this.fll = lerpColor(this.fll, this.fllFocus, this.focusTransition);
    } else {
        this.fll = lerpColor(this.fll, this.fllNoFocus, this.focusTransition);
    }
    fill(this.fll);
    arc(x, y, rad, rad,
        this.start, this.stop);
}

// atan2 returns a value in the range of -PI to PI.
// If the angle is less than 0 radians, we can fix the
// problem by adding TWO_PI to it.
Slice.prototype.hover = function(a) {
    a = a < 0 ? a + TWO_PI : a;
    return a > this.start && a < this.stop;
}

Slice.prototype.showLabel = function(x, y, rad, val) {
  fill(0, 0, 99);
  var pop = val === undefined ? "" : val * this.value;
  text(this + "\nEstimated population: " + round(pop),
    x + cos(this.middle) * rad * 0.5,
    y + sin(this.middle) * rad * 0.5);
}
