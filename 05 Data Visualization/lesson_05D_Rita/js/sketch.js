'use strict';

var concordance, sortedConcordance,
  ready, sortedSize, i, fll, x, y, radius,
  speed, xDir, yDir, letterOpacity = 0.625;

function preload() {
    // p5.js's httpGet or loadStrings functions could be used
    // here if you wanted an array of strings.
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        // See https://www.w3schools.com/xml/dom_httprequest.asp
        // for example code and an explanation for the meanings
        // of these ready state codes. This will call a function
        // that we define when our request has been processed.
        if (this.readyState === 4 && this.status === 200) {
            goRita(this.responseText);
        }
    };
    // Open the Get request with the file path of the target text.
    xmlhttp.open('GET', 'assets/huysmans_labas.txt', true);
    xmlhttp.send();
}

function setup() {
    pixelDensity(displayDensity());
    createCanvas(windowWidth, windowHeight);
    background(0);
    textAlign(RIGHT, CENTER);
    textSize(64);
    frameRate(1);
    colorMode(HSB, 359, 99, 99);
    ready = false;
    i = 0;
    sortedSize = 0;
    fll = [];
    x = width * 0.5;
    y = height * 0.5;
    radius = min(width, height) * 0.5;
    speed = random(1, 3);
    var a = random(TWO_PI);
    xDir = cos(a);
    yDir = sin(a);
}

function draw() {
    frameRate(map(mouseX, 0, width, 1, 60));
    noStroke();
    fill(0, 0, 0, map(mouseY, height, 0, 0, 1));
    rect(0, 0, width, height);
    if (ready) {
        push();
        translate(x, y);
        rotate(frameCount / 90.0);
        fill(fll[i]);
        text(sortedConcordance[i], radius, 0);
        pop();

        i = (i + 1) % sortedSize;
        x += speed * xDir;
        y += speed * yDir;
        if(x < radius || x > width - radius) {
          xDir *= -1;
        }
        if(y < radius || y > height - radius) {
          yDir *= -1;
        }
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function goRita(input) {
    // Find the beginning of the text. This is imperfect, as it still
    // includes the chapters listed in the table of contents in the
    // case of La Bas.
    input = input.substr(input.search((/CHAPTER\s\S+|Chapter\s\S+/)));

    // Find the end of the text.
    input = input.substr(0, input.indexOf(("End"), ("Project Gutenberg")));

    // Remove excess line-breaks.
    input = input.replace(/(?:\r\n|\r|\n)/g, " ");

    // Create a concordance, which measures the frequency of words in
    // the text. There are optional specifications when compiling this
    // concordance, such as words to exclude, e.g., the main character's
    // name.
    concordance = RiTa.concordance(input, {
        ignoreCase: true,
        ignoreStopWords: true,
        ignorePunctuation: true,
        wordsToIgnore: ['de', 'des', 'esseintes', 'whose', 'himself',
            'through', 'such', 'seemed', 'while', 'yet', 'whom', 'thus'
        ]
    });

    // The concordance consists of key-value pairs, so we can use
    // Object.keys to get an array of keys and then sort them by value,
    // where a key is a word and its value is its frequency.
    sortedConcordance = Object.keys(concordance).sort(function(a, b) {
        return concordance[b] - concordance[a];
    });


    // Create text colors.
    sortedSize = sortedConcordance.length;
    for(var i = 0; i < sortedSize; ++i) {
      fll.push(color(map(i, 0, sortedSize, 0, 360), 99, 99, letterOpacity));
    }

    // Once the concordance is complete, mark as ready so that the draw
    // function can begin displaying the text.
    ready = true;
}
