'use strict';

var cnvs, ready,
    countries, gdpStrings, militaryExpendStrings,
    rankLimit, rankMin, rankMax,
    margin, gridIncr, startColor, stopColor;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    ready = false;
    margin = 120;
    gridIncr = 50;
    startColor = color(255, 0, 127);
    stopColor = color(0, 127, 255);
    countries = [];
    gdpStrings = loadStrings('assets/ciaFactBookGDP.txt', function() {
        for (var i = 0, size = gdpStrings.length; i < size; ++i) {

            // Find, trim and parse dollar amount to a number.
            var dollarSignIndex = gdpStrings[i].indexOf('$');
            var dollarSub = gdpStrings[i].substring(dollarSignIndex + 1);

            // This uses a regular expression which specifies that extraneous
            // marks which would interfere with the following parseInt function,
            // i.e., any characters which are not numbers, excepting a negative
            // sign and a decimal point, are replaced with an empty character ''.
            var cleaned = dollarSub.replace(/[^\d\.\-]/g, '');

            // Convert from a string to a number.
            var parsed = parseInt(cleaned);

            // Trim down name.
            var gapIndex = gdpStrings[i].indexOf('    ');
            var countryInitial = gdpStrings[i].substring(gapIndex + 1);
            var countryFinal = countryInitial.substring(0, 50).trim();

            countries.push(new Country(countryFinal, i + 1, parsed));
        }

        // How many countries should be displayed on screen. This will be updated in draw
        // based on the mouse's position on screen.
        rankLimit = min(rankLimit, countries.length);
        rankMin = min(7, countries.length);
        rankMax = min(25, countries.length);

        ready = true;
    });
}

function draw() {
    background(32);
    drawTitle();
    drawSource();

    // Only display the information when ready.
    if (ready) {
        // Remap the mouse's position to how many
        rankLimit = int(map(mouseX, 0, width, rankMin, rankMax));
        drawFrame();
        drawBars();
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function drawTitle() {
    push();
    textSize(24);
    textAlign(CENTER, TOP);
    fill(255);
    text('Gross Domestic Product Comparison', width * 0.5, 5);
    pop();
}

function drawSource() {
    push();
    textAlign(RIGHT, BOTTOM);
    fill(255);
    text('Source: https://catalog.data.gov/dataset/college-scorecard', width - 5, height - 5);
    pop();
}

function drawFrame() {
    push();
    rectMode(CORNERS);
    textAlign(RIGHT, CENTER);
    noFill();
    strokeWeight(1.5);
    stroke(255);
    rect(margin, margin, width - margin, height - margin);
    fill(255);
    var amt;
    for (var i = height - margin; i > margin; i -= gridIncr) {
        strokeWeight(1);
        stroke(255);
        line(margin, i, width - margin, i);
        noStroke();

        // Convert the range from the top of the chart to the
        // bottom into the equivalent value from the upper bound
        // of the data on the y-axis to the lower bound.
        amt = round(map(i, /* Value */
            height - margin, /* Origin lower bound */
            margin, /* Origin upper bound */
            0, /* Target lower bound */
            countries[0].gdp)); /* Target upper bound */
        text(amt, margin - 2.5, i);
    }
    pop();
}

function drawBars() {
    push();
    rectMode(CORNERS);
    textAlign(LEFT, CENTER);
    noStroke();
    var l, r, t, b = height - margin;
    for (var j = 0; j < rankLimit; ++j) {
        l = map(j, 0, rankLimit, margin, width - margin) + 2.5;
        r = map(j + 1, 0, rankLimit, margin, width - margin) - 2.5;
        t = map(countries[j].gdp, /* Original value */
            countries[0].gdp, /* Original lower bound */
            0, /* Original upper bound */
            margin, /* Target lower bound */
            height - margin); /* Target upper bound */

        // Perlin noise to test the display without loaded data.
        // t = map(noise(j / rankLimit, frameCount / 60.0),
        //     0, 1, margin, height - margin);

        fill(lerpColor(startColor, stopColor, j / rankLimit));
        rect(l, t, r, b);

        push();
        noStroke();
        fill(255);
        translate(l + (r - l) * 0.5, height - margin + 7.5);
        rotate(QUARTER_PI);
        text(countries[j].name, 0, 0);
        pop();
    }
    pop();
}

function Country(n, i, g) {
    this.name = n;
    this.gdpRank = i;
    this.gdp = g;
}
