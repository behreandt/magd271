'use strict';

var cnvs, cntxt, ready,
    margin, gridIncr,
    data, usRows, usHappiness;

function setup() {
    pixelDensity(displayDensity());
    cnvs = createCanvas(windowWidth, windowHeight);
    cntxt = cnvs.elt.getContext('2d');
    ready = false;
    margin = 120;

    // Divide the vertical axis into increments of 10%.
    gridIncr = (height - margin * 2.0) / 10;

    // Get the values from the .csv file. When the file is done loading, then call
    // the function which parses the data.
    data = loadTable('assets/worldHappinessReport.csv', 'csv', 'header', function() {

        // Get only the rows pertaining to the USA.
        usRows = data.findRows('United States', 'country');

        // Create an empty array.to hold happiness objects for the USA.
        usHappiness = [];

        // Use a for-loop to push Happiness objects onto the usHappiness
        // array for every element in the usRows array.
        for (var i = 0, size = usRows.length; i < size; ++i) {
            usHappiness.push(new Happiness(
                usRows[i].getNum('year'),
                usRows[i].getNum('Perceptions of corruption'),
                usRows[i].getNum('Confidence in national government'),

                // Unlike other values, the column for Democratic quality contains empty spots,
                // which throws an error if getNum is attempted. Thus, we use get and then parse
                // the values ourselves within the Happiness constructor.
                usRows[i].get('Democratic Quality')));
        }

        // When finished parsing the data, specify that it is ready
        // to be displayed using a boolean.
        ready = true;
    });
}

function draw() {
    background(32);
    drawTitle();
    drawSource();

    if (ready) {
        drawLabels();
        drawFrame();
        drawLine(color(255, 0, 0, 204), 'Corruption');
        drawLine(color(0, 255, 0, 204), 'Confidence');
        drawLine(color(0, 0, 255, 204), 'Democracy');
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function drawTitle() {
    push();
    textSize(24);
    textAlign(CENTER, TOP);
    fill(255);
    text('U. S. Perceived Corruption, Confidence in Government, Democratic Quality', width * 0.5, 5);
    pop();
}

function drawSource() {
    push();
    textAlign(RIGHT, BOTTOM);
    fill(255);
    text('Source: http://worldhappiness.report/', width - 5, height - 5);
    pop();
}

function drawFrame() {
    push();
    rectMode(CORNERS);
    textAlign(RIGHT, CENTER);
    noFill();
    strokeWeight(1.5);
    stroke(255);
    rect(margin, margin, width - margin, height - margin);
    fill(255);
    var amt = 0;

    // Draw the faint gray lines which demarcate y-axis units,
    // which in this case are percentages in units of 10.
    for (var i = height - margin; i > margin; i -= gridIncr) {
        strokeWeight(0.75);
        stroke(127);
        line(margin, i, width - margin, i);
        noStroke();
        amt = round(map(i, height - margin, margin, 0, 100));
        text(amt + '%', margin - 2.5, i);
    }
    pop();
}

function drawLabels() {
    var x, y = height - margin + 7.5;
    for (var i = 0, size = usHappiness.length; i < size; ++i) {
        x = map(i, 0, size - 1, margin, width - margin);
        push();
        noStroke();
        fill(255);
        translate(x, y);
        rotate(QUARTER_PI);
        text(usHappiness[i].year, 0, 0);
        pop();
    }
}

function drawLine(strk, key) {
    push();
    noFill();
    strokeWeight(2);
    stroke(strk);
    beginShape();

    var x = 0,
        y = 0,
        n = 0;
    for (var i = 0, size = usHappiness.length; i < size; ++i) {
        x = map(i, 0, size - 1, margin, width - margin);
        if (key === 'Corruption') {
            n = usHappiness[i].perceivedCorruption;
            y = map(n, 1, 0, margin, height - margin);
            vertex(x, y);
        } else if (key === 'Confidence') {
            n = usHappiness[i].confidenceNtnlGov;
            y = map(n, 1, 0, margin, height - margin);
            vertex(x, y);
        } else if (key === 'Democracy') {
            // So as to not give the wrong impression that democratic quality plummeted
            // in a certain year, only add a vertex to the shape if the democracy value
            // is not -1.
            n = usHappiness[i].democracy;
            if (n !== -1) {
                y = map(n, 1, 0, margin, height - margin);
                vertex(x, y);
            }
        }

        // If the mouse hovers within a certain range of the data point, then additional
        // information can be displayed.
        if(n !== -1 && dist(mouseX, mouseY, x, y) < 25) {
          push();
          textAlign(CENTER, CENTER);
          rectMode(CENTER);
          noStroke();
          fill(48);
          rect(x, y, 50, 50, 5);
          fill(255);
          text(usHappiness[i].year + '\r\n' + nfs(n * 100, 2, 1) + '%', x, y);
          pop();
        }
    }
    endShape();

    // Display label for the line on the right side of the graph.
    fill(strk);
    noStroke();
    text(key, x + 5, y);

    pop();
}

// Happiness object.
function Happiness(y, corr, govConf, dem) {
    this.year = y;
    this.perceivedCorruption = corr;
    this.confidenceNtnlGov = govConf;
    // If the value is empty, then give it a value of -1.
    this.democracy = dem == '' ? -1 : dem;
}
