'use strict';

var scalar, threshold, smoothing, oscillation,
    smoothChange, translateX, translateY;

function setup() {
    createCanvas(420, 420);
    pixelDensity(displayDensity());
    background(64);
    smoothing = 20.0;
    oscillation = 0;
    smoothChange = 1;
    translateX = width / 2.0;
    translateY = height / 2.0;
    colorMode(HSB, 360, 100, 100);
}

function draw() {
    background(0, 0, 12);
    noStroke();
    translateX = width / 2.0;
    translateY = height / 2.0;
    translate(translateX, translateY);

    // Since cos and sin oscillate between -1 and 1,
    // we can use them to create ping-pong motions.
    // However, remember that it takes time and energy
    // for the computer to calculate sine and cosine.
    oscillation = sin(frameCount / smoothing);
    threshold = min(width, height);
    scalar = oscillation * threshold;
    smoothing += 0.001 * smoothChange;
    if (smoothing < 25.0 || smoothing > 100.0) {
        smoothChange *= -1;
    }
    scale(scalar);

    fill(30, 255, 255, 255);
    quad(0.5, 0,
        0, 0.5, -0.5, 0,
        0, -0.5);

    fill(60, 255, 255, 255);
    quad(0.375, 0,
        0, 0.375, -0.5, 0,
        0, -0.5);

    fill(90, 255, 255, 255);
    quad(0.25, 0,
        0, 0.25, -0.5, 0,
        0, -0.5);

    fill(120, 255, 255, 255);
    quad(0.125, 0,
        0, 0.125, -0.5, 0,
        0, -0.5);

    fill(150, 255, 255, 255);
    quad(0.0625, 0,
        0, 0.0625, -0.5, 0,
        0, -0.5);
}
