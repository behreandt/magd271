## Digital Quilt

![Digital Quilt](preview.png)

### Due Date
Thursday, January 26th, 2017
S01 9:30 a.m.
S02 11:00 a.m.

### Philosophy

The first step in revising code in Processing is to replace constants with variables. For example,

``` java
ellipse(320, 160, 50, 60);
```

becomes

``` java
float x = 320, y = 160, w = 50, h = 60;
ellipse(x, y, w, h);
```

This step allows code to move toward durability (applicability to multiple situations), modularity (ability to recombine with other code to produce a variety of aesthetic experiences) and dynamism (change over time, or in response to user input). On the last quality, applying math operations to the variables above allow the design to scale and reposition itself as the sketch changes in size.

``` java
float x = width / 2.0, y = height / 2.0, w = height / 3.0, h = height / 3.0;
ellipse(x, y, w, h);
```

The goal of this project is to create a digital quilt out of several patches. From the artistic perspective, this cultivates the following ideas:

* Working under constraint as a method for artistic growth.

* Repetition with a difference: Stylistic consistency shows sustained intention (choices about organic vs. inorganic shape, relative scale, color, etc. are unified by a goal or idea). Parametric thinking can aide in amplifying this intention by repeating simple procedures or shapes until they become complex patterns.

From the software engineering perspective, this demonstrates that the syntax of Processing/Java is guided by the principle of [Extensibility](https://en.wikipedia.org/wiki/Extensibility).

### Directions

* Form a group of 2 - 3 people.
* Download the template files from the course Bitbucket page.
* In the main sketch:
    * Declare an object of the data type `Quilt`.
    * In `setup`
        * Initialize the `Quilt` object with a constructor.
        * Add patches you have created to the quilt object using the `addPatch` instance method.
    * In `draw`
        * Call the quilt object's `draw` function.
* In separate tabs:
    * Name the tab appropriately. It is recommended that you match the name of the tab to the name of the class.
    * Define a class that extends the `Patch` class.
    * Define the draw method.
    * Use the variables `x`, `y`, `w` and `h` as the foundational references for the shapes which constitute the patch.
* Create a minimum of 4 patches, using Processing's shape functions (no imported images).

``` java
class HoundsToothPatch extends Patch {
    void draw(float x, float y, float w, float h) {
        // Your code here.
    }
}
```

### Inspiration

* [Libs Elliott](http://www.libselliott.com/)
* [Joshua Davis - _Aim High, Keep Moving_ ](https://www.behance.net/gallery/5278345/194-Aim-High-Keep-Moving)
* [A Pattern A Day](http://a-pattern-a-day.tumblr.com/)

### Rubric

Points | Description
------ | -----------
40 | Sketch contains a minimum of four patches.
10 | Patches utilize at least 4 Processing 2D forms (point, line, ellipse, rect, triangle, quad, beginShape).
20 | Quilt as a whole is aesthetically unified, showing intentionality in choice of color, shape, scale, figure vs. ground.
10 | Ensure that the code contains at least 4 single- or multi-line comments, written in complete sentences, that explain what the code is doing at key steps.
10 | Ensure that the sketch runs without errors. The main tab of the sketch should be the only tab with global-scope setup and draw functions.
10 | Ensure that the main tab of the sketch shares the same name as the sketch folder. Name the sketch folder and main tab according to the following convention: s17_magd271_assignment1_lastnames. Compress the sketch folder into a .zip file and upload to D2L.
100 | Total

__Any code or media within a submission are presumed to be the work of the student(s) unless otherwise stated. When media or code from outside sources are used, a link to the source and notice of attribution at the top of the main sketch in a comment should be provided. Media and code used are expected to be from the intellectual commons (i.e., not copyrighted or registered as a trademark) and to be altered substantially by the student(s). On first incident, students whose work does not meet this standard will have one week to resubmit the assignment for half credit. On second incident, students will be referred to Student Conduct Programs and will fail the course.__
