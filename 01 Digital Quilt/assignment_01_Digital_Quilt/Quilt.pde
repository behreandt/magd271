import java.util.*;

enum Axis {
  Major, Minor
}

class Quilt {

  public int patches, 
    borderThickness;
  public boolean maintainAspect = true, 
    border = false, 
    mouseResponsive = false;
  public color borderFillColor;
  public Axis axis;
  private float patchW, 
    patchH, 
    quiltW, 
    quiltH;
  private final float minorAxis, 
    majorAxis, 
    minorpatchW, 
    minorpatchH, 
    majorpatchW, 
    majorpatchH, 
    minPatches, 
    maxPatches;
  private List<Patch> basket;

  public Quilt(int p) {
    this(p, false, 0, color(0));
  }

  public Quilt(int p, boolean border, int borderThickness, 
    color borderFillColor) {
    this.basket = new ArrayList<Patch>();
    this.patches = p;
    this.minPatches = 1;
    this.maxPatches = 64;
    this.axis = Axis.Minor;
    this.quiltW = width;
    this.quiltH = height;
    if (this.patches > 0) {
      this.patchW = quiltW / this.patches;
      this.patchH = quiltH / this.patches;
    }
    this.minorAxis = min(width, height);
    this.majorAxis = max(width, height);
    this.minorpatchW = this.minorAxis / this.patches;
    this.minorpatchH = this.minorAxis / this.patches;
    this.majorpatchW = this.majorAxis / this.patches;
    this.majorpatchH = this.majorAxis / this.patches;

    this.border = border;
    this.borderThickness = int(constrain(
      borderThickness, 0, this.minorAxis / 2.0));
    this.borderFillColor = borderFillColor;
  }

  public void addPatch(Patch p) {
    this.basket.add(p);
  }

  public Patch getPatch(int i) {
    return this.basket.get(i);
  }

  public Patch randomPatch() {
    if (this.basket.size() > 0) {
      return this.basket.get(int(random(0, this.basket.size())));
    } else return null;
  }

  public void draw() {
    maintainAspect();
    scaleToMouse();

    if (this.patches > 0) {
      if (this.basket.size() > 0) {
        drawPatches();
        if (border) {
          drawBorder();
        }
      } else {
        noticeToAddPatches();
      }
    } else {
      noticeToIncreasePatchCount();
    }
  }

  private void maintainAspect() {
    if (this.maintainAspect) {
      if (this.axis == Axis.Major) {
        this.patchW = this.majorpatchW;
        this.patchH = this.majorpatchH;
      } else {
        this.patchW = this.minorpatchW;
        this.patchH = this.minorpatchH;
      }
    }
  }

  private void scaleToMouse() {
    if (this.mouseResponsive) {
      if (this.maintainAspect) {
        if (this.axis == Axis.Major) {
          if (this.majorAxis == width) {
            this.patchW = this.patchH
              = this.majorAxis / ceil(map(
              mouseY, 0, height, minPatches, maxPatches));
          } else {
            this.patchW = this.patchH
              = this.majorAxis / ceil(map(
              mouseX, 0, width, minPatches, maxPatches));
          }
        } else {
          if (this.minorAxis == width) {
            this.patchW = this.patchH
              = this.minorAxis / ceil(map(
              mouseY, 0, height, minPatches, maxPatches));
          } else {
            this.patchW = this.patchH
              = this.minorAxis / ceil(map(
              mouseX, 0, width, minPatches, maxPatches));
          }
        }
      } else {
        this.patchW = width / ceil(map(
          mouseX, 0, width, minPatches, maxPatches));
        this.patchH = height / ceil(map(
          mouseY, 0, height, minPatches, maxPatches));
      }
    }
  }

  private void drawPatches() {
    for (float x = 0; x < quiltW; x+= patchW) {
      for (float y = 0; y < quiltH; y+= patchH) {
        this.randomPatch().draw(x, y, patchW, patchH);
      }
    }
  }

  private void noticeToAddPatches() {
    background(32);
    textAlign(CENTER, CENTER);
    textSize(width / 18);
    fill(255);
    text("ADD PATCHES TO THE QUILT.", width / 2.0, height / 2.0);
  }

  private void noticeToIncreasePatchCount() {
    background(32);
    textAlign(CENTER, CENTER);
    textSize(width / 18);
    fill(255);
    text("INCREASE THE PATCH COUNT.", width / 2.0, height / 2.0);
  }

  private void drawBorder() {
    noStroke();
    fill(this.borderFillColor);
    beginShape();
    vertex(0, 0);
    vertex(width, 0);
    vertex(width, height);
    vertex(0, height);
    beginContour();
    vertex(this.borderThickness, height - this.borderThickness);
    vertex(width - this.borderThickness, height - this.borderThickness);
    vertex(width - this.borderThickness, this.borderThickness);
    vertex(this.borderThickness, this.borderThickness);
    endContour();
    endShape(CLOSE);
  }
}