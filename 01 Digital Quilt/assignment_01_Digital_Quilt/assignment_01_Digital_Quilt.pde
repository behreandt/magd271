Quilt q;
int patchCount = 4;
boolean paused = false;

void setup() {
  size(1024, 512);
  pixelDensity(displayDensity());
  background(0);
  frameRate(6);
  surface.setTitle("MAGD271 Assignment 1 Digital Quilt");

  // You can initialize the quilt object here. Change
  // patchCount to see how the size of the patch comapares
  // to the size of the window.
  q = new Quilt(patchCount);

  // The quilt will attempt to maintain the 1:1 aspect
  // of the tiles that you create, unless you set the
  // boolean variable below to false.
  //q.maintainAspect = false;

  // After you've created a pattern, you can add it
  // to the quilt using the quilt's addPattern(Pattern)
  // function.
  q.addPatch(new ExamplePatchA());
  q.addPatch(new ExamplePatchB());
  q.addPatch(new ExamplePatchC());
  q.addPatch(new ExamplePatchD());
  q.addPatch(new ExamplePatchE());
  q.addPatch(new ExamplePatchF());
  q.addPatch(new ExamplePatchG());
  q.addPatch(new ExamplePatchH());
  q.addPatch(new ExamplePatchI());
  q.addPatch(new ExamplePatchJ());
  q.addPatch(new ExamplePatchK());
  q.addPatch(new ExamplePatchL());
  q.addPatch(new ExamplePatchM());
}

void draw() {
  q.draw();
}

// You can take a screenshot of your quilt by clicking the
// right mouse button with the code below.
void mousePressed() {
  if (mouseButton == LEFT) {
    paused = !paused;
    if (paused) {
      noLoop();
    } else {
      loop();
    }
  } else if (mouseButton == RIGHT) {
    save(millis() + ".png");
    println("Saved image to the sketch folder.");
  }
}