int petalCount, countOsc;
float x, y, w, h, thetaMajor, thetaMinor;

void setup() {
  
  // If the renderer is switched to P2D, then gradients
  // can be created by specifying color on a vertex-by-
  // vertex basis. Note, however, that on PCs especially,
  // using P2D and P3D can lead to major slow-down when
  // launching your sketch.
  size(420, 420, P2D);
  //size(420, 420);
  background(64);
  noStroke();

  surface.setTitle("Shapes By Vertex");
  surface.setResizable(true);
  pixelDensity(displayDensity());

  // Set variables' initial values.
  petalCount = 5;
  countOsc = 1;
  x = width / 2.0;
  y = height / 2.0;
  w = h = min(width, height) / 2.0;
  thetaMajor = 0;
  thetaMinor = 0;
}

void draw() {
  background(32);

  // With resizable sketch windows, width and height
  // are no longer constant, and so variables based
  // on width or height must be updated in draw.
  x = width / 2.0;
  y = height / 2.0;
  w = h = min(width, height) / 2.0;

  // User-Controlled
  thetaMajor = map(mouseX, 0, width, 0, TWO_PI);
  petalCount = int(map(mouseY, 0, height, 1, 50));

  // Automated
  //thetaMajor += 0.05;
  //petalCount += 1 * countOsc;
  //if (petalCount < 3 || petalCount > 50) {
  //  countOsc *= -1;
  //}

  for (int i = 0; i < petalCount; ++i) {
    // Rotation of each petal.
    thetaMinor = map(i, 0, petalCount, 0, TWO_PI);
    pushMatrix();
    translate(x, y);
    scale(w, h);
    // Rotation of the flower as a whole + petal.
    rotate(thetaMajor + thetaMinor);
    beginShape();
    fill(map(i, 0, petalCount, 204, 255), 0, 
      map(i, 0, petalCount, 127, 255), 204);
    vertex(0, 0);
    fill(map(i, 0, petalCount, 204, 255), 
      map(i, 0, petalCount, 127, 255), 0, 204);
    bezierVertex(
      0.25, 0.25, 
      0.75, 0.25, 
      1, 0);
    fill(0, map(i, 0, petalCount, 204, 255), 
      map(i, 0, petalCount, 127, 255), 204);
    bezierVertex(
      0.75, -0.25, 
      0.25, -0.25, 
      0, 0);
    endShape(CLOSE);
    popMatrix();
  }
}