class ChildApplet extends PApplet {

  // Secretly, the name of the file has been the name of a Java
  // class the entire time. This is why you can not name your
  // sketch 'final' or 'static' or any other key word in Java.

  // This breaks the rules of good programming, because we can
  // write code like majorSketch.child.majorSketch.child ..., but
  // if we want reciprocity between the two sketch windows, the
  // main sketch can feed the ChildApplet instance a reference to
  // itself - using the 'this' keyword, in the constructor, which
  // then caches it so messages can be sent back and forth. A
  // more nuanced approach would be to create a messaging service
  // between the two.
  lesson_01C_Multiple_Circle majorSketch;

  color focusedColor, unfocusedColor, currentColor;
  float x, shortEdge, longEdge;
  boolean inCourt;

  public ChildApplet(lesson_01C_Multiple_Circle parent) {
    super();
    PApplet.runSketch(new String[]{this.getClass().getName()}, this);
    majorSketch = parent;
  }

  public void settings() {
    size(360, 360);
    pixelDensity(displayDensity());
  }

  public void setup() {
    background(64);
    surface.setTitle("MinorSketch");
    surface.setResizable(true);
    noStroke();
    ellipseMode(RADIUS);

    focusedColor = color(255, 0, 127);
    unfocusedColor = color(0, 255, 127);
    currentColor = color(64);

    x = width / 2.0;
    shortEdge = height < width ? height : width;
    longEdge = max(width, height);
    inCourt = false;
  }

  public void draw() {
    // Linear interpolation (lerp for short) moves smoothly
    // from an origin value to a target value at a rate.
    currentColor = focused
      ? lerpColor(currentColor, focusedColor, 0.05)
      : lerpColor(currentColor, unfocusedColor, 0.05);
    background(currentColor);
    shortEdge = height < width ? height : width;
    longEdge = max(width, height);
    fill(255, 255, 0, 100);
    ellipse(width / 2.0, height / 2.0, shortEdge / 2.0, shortEdge / 2.0);
    fill(255, 255, 0, 150);
    ellipse(width / 2.0, height / 2.0, shortEdge / 4.0, shortEdge / 4.0);
    fill(255, 255, 0, 200);
    ellipse(width / 2.0, height / 2.0, shortEdge / 6.0, shortEdge / 6.0);
    fill(0);
    ellipse(x, height / 2.0, shortEdge / 4.5, shortEdge / 4.5);
    x = focused ? x + longEdge / 360.0: x - longEdge / 360.0;
    x = x > width + shortEdge / 4.5
      ? - shortEdge / 4.5
      : x < - shortEdge / 4.5
      ? width + shortEdge / 4.5
      : x;
  }
}