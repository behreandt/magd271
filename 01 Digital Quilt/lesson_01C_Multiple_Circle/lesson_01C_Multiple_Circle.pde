// You can create a second sketch window by creating a
// class which extends PApplet. This may be useful for
// displaying one window to the public, while a second
// window contains controls or diagnostic info under
/// your purview.
// See Examples > Demos > Tests > Multiple Windows, which
// contains the following note:
// 'Based on code by GeneKao (https://github.com/GeneKao)'
ChildApplet minorSketch;

color focusedColor, unfocusedColor, currentColor;
boolean inCourt;
float x, shortEdge, longEdge;

void settings() {
  size(420, 420);
  pixelDensity(displayDensity());
}

void setup() {
  background(64);
  minorSketch = new ChildApplet(this);
  surface.setTitle("Major Sketch");
  surface.setResizable(true);
  focusedColor = color(32);
  unfocusedColor = color(255);
  currentColor = color(64);

  x = 0;
  // The ternary operator offers an alternative to min() and max() if
  // you have only two comparisands.
  shortEdge = height < width ? height : width;
  longEdge = max(width, height);

  inCourt = false;
}

void draw() {
  if (focused) {
    currentColor = lerpColor(currentColor, focusedColor, 0.05);
  } else {
    currentColor = lerpColor(currentColor, unfocusedColor, 0.05);
  }

  background(currentColor);
  noStroke();

  ellipseMode(CENTER);
  fill(255, 0, 0, 127);
  ellipse(width * 0.5, height * 0.5, width * 0.85, height * 0.85);

  ellipseMode(CORNER);
  fill(0, 255, 0, 127);
  ellipse(width * 0.5, height * 0.5, width * 0.45, height * 0.5);

  ellipseMode(RADIUS);
  fill(0, 0, 255, 127);
  ellipse(width * 0.5, height * 0.5, width * 0.25, height * 0.25);

  ellipseMode(CORNERS);
  fill(255, 255, 0, 127);
  ellipse(width * 0.5, height * 0.3, width * 0.7, height * 0.5);

  ellipseMode(RADIUS);

  shortEdge = height < width ? height : width;
  longEdge = max(width, height);
  fill(0);
  ellipse(x, height / 2.0, shortEdge / 4.5, shortEdge / 4.5);

  // Move the ball to the right if this window has focused,
  // otherwise, move the ball to the left.
  x = focused ? x + longEdge / 360.0: x - longEdge / 360.0;
  x = x > width + shortEdge / 4.5
    ? - shortEdge / 4.5
    : x < - shortEdge / 4.5
    ? width + shortEdge / 4.5
    : x;
}