'use strict';

// You can create a sketch window by using
// 'instance mode' of p5.js
var sketch0 = function(p) {

    var focusedColor, unfocusedColor, currentColor,
        inCourt,
        x, shortEdge, longEdge;

    p.setup = function() {
        p.createCanvas(420, 420);
        p.pixelDensity(p.displayDensity());
        p.background(64);
        focusedColor = p.color(32);
        unfocusedColor = p.color(255);
        currentColor = p.color(64);

        x = 0;
        // The ternary operator offers an alternative to min() and max() if
        // you have only two comparisands.
        shortEdge = p.height < p.width ? p.height : p.width;
        longEdge = p.max(p.width, p.height);

        inCourt = false;
    };

    p.draw = function() {
        if (p.focused) {
            currentColor = p.lerpColor(currentColor, focusedColor, 0.05);
        } else {
            currentColor = p.lerpColor(currentColor, unfocusedColor, 0.05);
        }

        p.background(currentColor);
        p.noStroke();

        p.ellipseMode(p.CENTER);
        p.fill(255, 0, 0, 127);
        p.ellipse(p.width * 0.5, p.height * 0.5, p.width * 0.85, p.height * 0.85);

        p.ellipseMode(p.CORNER);
        p.fill(0, 255, 0, 127);
        p.ellipse(p.width * 0.5, p.height * 0.5, p.width * 0.45, p.height * 0.5);

        p.ellipseMode(p.RADIUS);
        p.fill(0, 0, 255, 127);
        p.ellipse(p.width * 0.5, p.height * 0.5, p.width * 0.25, p.height * 0.25);

        p.ellipseMode(p.CORNERS);
        p.fill(255, 255, 0, 127);
        p.ellipse(p.width * 0.5, p.height * 0.3, p.width * 0.7, p.height * 0.5);

        p.ellipseMode(p.RADIUS);

        p.fill(0);
        p.ellipse(x, p.height / 2.0, shortEdge / 4.5, shortEdge / 4.5);

        // Move the ball to the right if this window has focused,
        // otherwise, move the ball to the left.
        x = p.focused ? x + longEdge / 360.0 : x - longEdge / 360.0;
        x = x > p.width + shortEdge / 4.5 ?
            -shortEdge / 4.5 :
            x < -shortEdge / 4.5 ?
            p.width + shortEdge / 4.5 :
            x;
    };
};
var myp5 = new p5(sketch0, "canvas0");

var sketch1 = function(p) {
    var focusedColor, unfocusedColor, currentColor,
        x, shortEdge, longEdge,
        inCourt;

    p.setup = function() {
        p.createCanvas(320, 320);
        p.pixelDensity(p.displayDensity());
        p.background(64);
        p.noStroke();
        p.ellipseMode(p.RADIUS);

        focusedColor = p.color(255, 0, 127);
        unfocusedColor = p.color(0, 255, 127);
        currentColor = p.color(64);

        x = p.width / 2.0;
        shortEdge = p.height < p.width ? p.height : p.width;
        longEdge = p.max(p.width, p.height);

        inCourt = false;
    };

    p.draw = function() {
      // Linear interpolation (lerp for short) moves smoothly
      // from an origin value to a target value at a rate.
      currentColor = p.focused
            ? p.lerpColor(currentColor, focusedColor, 0.05)
            : p.lerpColor(currentColor, unfocusedColor, 0.05);
          p.background(currentColor);
          p.fill(255, 255, 0, 100);
          p.ellipse(p.width / 2.0, p.height / 2.0, shortEdge / 2.0, shortEdge / 2.0);
          p.fill(255, 255, 0, 150);
          p.ellipse(p.width / 2.0, p.height / 2.0, shortEdge / 4.0, shortEdge / 4.0);
          p.fill(255, 255, 0, 200);
          p.ellipse(p.width / 2.0, p.height / 2.0, shortEdge / 6.0, shortEdge / 6.0);
          p.fill(0);
          p.ellipse(x, p.height / 2.0, shortEdge / 4.5, shortEdge / 4.5);
          x = p.focused ? x + longEdge / 360.0: x - longEdge / 360.0;
          x = x > p.width + shortEdge / 4.5
            ? - shortEdge / 4.5
            : x < - shortEdge / 4.5
            ? p.width + shortEdge / 4.5
            : x;
    };
};
var myp5 = new p5(sketch1, "canvas1");
