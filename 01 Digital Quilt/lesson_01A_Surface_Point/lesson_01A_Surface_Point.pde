void setup() {
  size(420, 420);

  // For high density displays, this line will help ensure
  // that you have a clear, smooth display.
  pixelDensity(displayDensity());
  
  background(0);
  
  // Sets the interpretation of color to Hue (0 - 360),
  // Saturation (0 - 100), Brightness (0 - 100).
  colorMode(HSB, 360, 100, 100);

  // By default the sketch window is not resizable upon run. You
  // can change this using the built-in surface object.
  surface.setResizable(true);

  // By default the sketch window is named after the main .pde file.
  // You can change this with the following.
  surface.setTitle("Points On A Surface");
}

void draw() {
  
  // Since points are so minimal, there is more significance to HOW
  // they are distributed in the space, the weight and color given
  // to them, than to the form itself. Consider noise vs. random,
  // pointillism as an artistic school.
  stroke(0, random(0, 100), random(0, 100));
  strokeWeight(random(1, 5.0));
  point(random(0, width), random(0, height));
}