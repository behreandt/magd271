// This will make JavaScript a bit more scrupulous about error-checking. For example,
// you won't be able to use variables unless they have been declared. See
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode .
'use strict';

function setup() {
    createCanvas(420, 420);

    // For high density displays, this line will ensure
    // that you have a clear, smooth display.
    pixelDensity(displayDensity());

    background(0);

    // Sets the interpretation of color to Hue (0 - 360),
    // Saturation (0 - 100), Brightness (0 - 100).
    colorMode(HSB, 360, 100, 100, 100);
}

function draw() {

    // Since points are so minimal, there is more significance to HOW
    // they are distributed in the space, the weight and color given
    // to them, than to the form itself. Consider noise vs. random,
    // pointillism as an artistic school.
    stroke(0, random(0, 100), random(0, 100));
    strokeWeight(random(1, 5));
    point(random(0, width), random(0, height));
}
