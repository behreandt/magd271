float r, g, b, bIncr;
int bOsc, cm, dDensity, w, h;

void setup() {
  size(420, 420);
  
  // Pixel density plays an even more crucial role
  // when we attempt to paint the pixels on our sketch
  // manually. If a display has 2 pixel density, then
  // we must multiply width and height by that factor.
  pixelDensity(displayDensity());
  dDensity = displayDensity();
  w = width * dDensity;
  h = height * dDensity;

  background(64);
  strokeWeight(3);
  stroke(255);
  textAlign(CENTER, TOP);
  surface.setTitle("The Pixel Array");

  r = g = b = 0;
  bIncr = 0.5;
  bOsc = 1;
  cm = RGB;
}

void draw() {
  noCursor();

  // Sets the interpretation of color to either
  // Red-Green-Blue or Hue-Saturation-Brightness.
  colorMode(cm, 255, 255, 255);

  b += bIncr * bOsc;
  if (b <= 0 || b >= 255) {
    bOsc *= -1;
  }

  // The sketch display is a two-dimensional grid
  // of pixels, each of which contains ARGB values
  // stored in a color variable. This variable can
  // be expressed in hexadecimal (counting from 0
  // to 8, then A to F). The function below loads
  // the pixels from memory so we can paint them
  // directly rather than using point, line, etc.
  loadPixels();

  // Nested for-loops are used to cycle through
  // the grid. We move from row to row (from y = 0
  // to y = height - 1, then, within each row,
  // from column to column (from x = 0 to x = width - 1),
  // increasing y and x by one. Although the grid is
  // 2-dimensional to us, it is stored internally as a
  // 1-dimensional array. To convert from 2D to 1D,
  // we declare a variable i as well, which serves as
  // our index for the pixel array. Alternatively,
  // we could have set the stride for moving through
  // pixels as x + y * width (which we do below in the
  // mousePressed function.
  for (int y = 0, i = 0; y < h; ++y) {
    for (int x = 0; x < w; ++x, ++i) {
      r = map(y, 0, h, 0, 255);
      g = map(x, 0, w, 0, 255);
      pixels[i] = color(r, g, b);
    }
  }
  updatePixels();

  text(hex(get(mouseX, mouseY)), 
    mouseX, mouseY + 3);
  point(mouseX, mouseY);
}

// Right-click switches between RGB and HSB color.
// Left-click prints the color at the mouse location.
void mousePressed() {
  if (mouseButton == RIGHT) {
    // The ternary operator is a shortcut version of
    // if-else statements. In this case, it asks, 'Is
    // the variable cm equal to RGB? If so, then assign
    // the value HSB to cm : Otherwise, assign the value
    // RGB to cm.
    cm = cm == RGB ? HSB : RGB;
  } else if (mouseButton == LEFT) {
    println(hex(get(mouseX, mouseY)));
  }
}