'use strict';

var r, g, b, bIncr,
    bOsc, dDensity, w, h;

function setup() {
    createCanvas(420, 420);

    // Pixel density plays an even more crucial role
    // when we attempt to paint the pixels on our sketch
    // manually. If a display has 2 pixel density, then
    // we must multiply width and height by that factor.
    pixelDensity(displayDensity());
    dDensity = displayDensity();
    w = width * dDensity;
    h = height * dDensity;

    background(64);
    strokeWeight(3);
    stroke(255);
    textAlign(CENTER, TOP);

    r = g = b = 0;
    bIncr = 1;
    bOsc = 1;
}

function draw() {

    b += bIncr * bOsc;
    if (b <= 0 || b >= 255) {
        bOsc *= -1;
    }

    // The sketch display is a two-dimensional grid
    // of pixels, each of which contains RGBA values.
    // This variable can be expressed in hexadecimal
    // (counting from 0 to 8, then A to F). The function
    // below loads the pixels from memory so we can
    // paint them directly rather than using point, etc.
    loadPixels();

    // Nested for-loops are used to cycle through
    // the grid. We move from row to row (from y = 0
    // to y = height - 1, then, within each row,
    // from column to column (from x = 0 to x = width - 1),
    // increasing y and x by one. Although the grid is
    // 2-dimensional to us, it is stored internally as a
    // 1-dimensional array.

    // The stride of a pixel array is different in p5.js
    // in that each channel of color is recorded by an element
    // of the array: red, green, blue, alpha. So the stride
    // of pixels is 4. This also means that p5.js is
    // less flexible in transitioning from RGB to HSB color.
    for (var y = 0, i = 0; y < h; ++y) {
        for (var x = 0; x < w; ++x, i += 4) {
            r = map(y, 0, h, 0, 255);
            g = map(x, 0, w, 0, 255);
            pixels[i] = r;
            pixels[i + 1] = g;
            pixels[i + 2] = b;
            pixels[i + 3] = 255; // Alpha-channel
        }
    }

    updatePixels();
    // Hex is not needed here.
    text(get(mouseX, mouseY), mouseX, mouseY + 3);
}

// Right-click switches between RGB and HSB color.
// Left-click prints the color at the mouse location.
function mousePressed() {
    if (mouseButton == RIGHT) {
        // The ternary operator is a shortcut version of
        // if-else statements. In this case, it asks, 'Is
        // the variable cm equal to RGB? If so, then assign
        // the value HSB to cm : Otherwise, assign the value
        // RGB to cm.
        // cm = cm == RGB ? HSB : RGB;
    } else if (mouseButton == LEFT) {
      // This is not effective here if the white dot
      // of the mouse's location is drawn in the place
      // of the default cursor.
      console.log(get(mouseX, mouseY));
    }
}
