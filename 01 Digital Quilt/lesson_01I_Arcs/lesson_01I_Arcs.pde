float sw, x, y, r, theta;
int count, minCount, maxCount, countOsc;

void setup() {
  size(420, 420);
  colorMode(HSB, TWO_PI, 255, 255);
  pixelDensity(displayDensity());
  background(#202020);
  ellipseMode(RADIUS);
  noFill();
  strokeCap(ROUND);
  surface.setTitle("Arcs");
  surface.setResizable(true);
  pixelDensity(displayDensity());
  
  sw = 1.5;
  x = width * 0.5;
  y = height * 0.5;
  r = min(width, height) / 2.125;
  theta = 0;
  count = 5;
  minCount = 5;
  maxCount = max(width, height) / 6;
  countOsc = 1;
}

void draw() {
  background(0);
  x = width * 0.5;
  y = height * 0.5;
  r = min(width, height) * 0.47;
  theta += 0.01;

  count = count + 1 * countOsc;
  if (count < minCount || count > maxCount) {
    countOsc *= -1;
  }
  maxCount = max(width, height) / 6;

  sw = map(count, minCount, maxCount, 4, 1);
  strokeWeight(sw > 1 ? sw : 1);
  
  float theta1, theta2, _r;
  for (int i = 0; i < count; ++i) {
    theta1 = theta + map(i, 0, count, 0, TWO_PI);
    theta2 = theta + map(i + 1, 0, count, 0, TWO_PI * 1.75);
    _r = map(i, 0, count, sw, r);
    stroke(theta1 - theta, 255, 255);
    arc(x, y, _r, _r, theta1, theta2, OPEN);
  }
}