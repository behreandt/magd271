'use strict';

var sw, x, y, r, theta,
    count, minCount, maxCount, countOsc;

function setup() {
    createCanvas(420, 420);
    pixelDensity(displayDensity());
    background(64);
    ellipseMode(RADIUS);
    colorMode(HSB, TWO_PI, 255, 255);
    noFill();
    strokeCap(ROUND);

    sw = 1.5;
    x = width / 2.0;
    y = height / 2.0;
    r = min(width, height) / 2.125;
    theta = 0;
    count = 5;
    minCount = 5;
    maxCount = max(width, height) / 6;
    countOsc = 1;
}

function draw() {
    background(32);
    theta += 0.01;

    count = count + 1 * countOsc;
    if (count < minCount || count > maxCount) {
        countOsc *= -1;
    }

    sw = map(count, minCount, maxCount, 4, 1);
    strokeWeight(sw > 1 ? sw : 1);

    var theta1 = 0, theta2 = 0, _r = 0;
    for (var i = 0; i < count; ++i) {
        theta1 = theta + map(i, 0, count, 0, TWO_PI);
        theta2 = theta + map(i + 1, 0, count, 0, TWO_PI * 1.75);
        _r = map(i, 0, count, sw, r);
        stroke(theta1 - theta, 255, 255, 204);
        arc(x, y, _r, _r, theta1, theta2, OPEN);
    }
}
