// Rather than attempting to manually scale shapes,
// consider thinking of unit shapes, whose vertices
// range from 0 to 1 (where 0, 0 is the top left
// corner and the pivot of rotation) or from -0.5
// to 0.5 (where 0, 0 is the center and pivot of
// rotation. This kind of thinking will come in handy
// for working with meshes and UV-mapping in Unity.

void setup() {
  size(420, 420);
  pixelDensity(displayDensity());
  background(64);
  surface.setTitle("Scaling Complex Shapes");
  surface.setResizable(true);
  pixelDensity(displayDensity());
}

void draw() {
  background(32);
  noStroke();
  translate(width / 2.0, height / 2.0);
  scale(map(mouseX, 0, width, -width / 2.0, width / 2.0));
  fill(127, 0, 255, 107);

  // Centered triangles.
  triangle(0, -0.5, 
    -0.5, 0.5, 
    0.5, 0.5);
  fill(255, 0, 127, 107);
  triangle(0, 0.5, 
    0.5, -0.5, 
    -0.5, -0.5);

  // Oriented triangles.
  fill(0, 127, 255, 54);
  triangle(0, 0, 
    1, 0, 
    0, 1);
  fill(0, 255, 127, 54);
  triangle(0, 0, 
    -1, 0, 
    0, -1);
  fill(255, 127, 0, 54);
  triangle(0, 0, 
    1, 0, 
    0, -1);
  fill(127, 255, 0, 75);
  triangle(0, 0, 
    -1, 0, 
    0, 1);
}