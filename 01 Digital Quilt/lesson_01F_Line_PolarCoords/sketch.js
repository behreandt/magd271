'use strict';

var resolution,
    theta, maxTheta;

function setup() {
    createCanvas(420, 420);
    pixelDensity(displayDensity());
    background(64);
    resolution = 600;
    theta = 0;
    maxTheta = TWO_PI;
}

function draw() {
    background(32);

    maxTheta = map(mouseX, 0, width, 0, TWO_PI);
    resolution = map(mouseY, 0, height, 3, 900);

    // Sky gradient
    // Syntax of a for-loop:
    // for(controlling variable initial state;
    // loop again if this condition is true;
    // change controlling variable so condition will become false) {
    // code to execute each time through the loop; }
    for (var y = 0; y < height; ++y) {
        stroke(map(y, 0, height, 127, 255),
            map(y, 0, height, 255, 127),
            127);
        line(0, y, width, y);
    }

    for (var i = 0; i < resolution; ++i) {
        theta = map(i, 0, resolution, 0, maxTheta);
        stroke(127, map(i, 0, height, 127, 0),
            map(i, 0, height, 255, 127), 127);
        line(width / 2.0, height / 2.0,
            width / 2.0 + cos(theta) * width / 2.0,
            height / 2.0 + sin(theta) * width / 2.0);
    }
}
