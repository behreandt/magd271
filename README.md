# MAGD 271
## Interactive Communication
## Spring 2017

* Meetings
    * Section 01 McGraw 127, Tuesday and Thursday, 09:30 a.m. - 10:45 a.m.
    * Section 02 McGraw 127, Tuesday and Thursday, 11:00 a.m. - 12:15 p.m.

* Instructor: Jeremy Behreandt
* Office: Andersen Library - Original Building 1217J
* Office Hours: Wednesday, Friday 09:00 a.m. - 11:30 a.m.
    * Also available by appointment
* Email: behreandjj28@uww.edu
* Course Website
    * Primary: Desire 2 Learn
    * Secondary: [Bitbucket](https://bitbucket.org/behreandt/magd271/)

### Course Description

The power and influence of a human-computer interface, whether used for aesthetic enjoyment or commerce, depends upon its ability to efficiently communicate information and to convince the user that his or her choices and actions are impactful.

To that end, this course will explore the how animation techniques, peripherals aside from keyboard and mouse, and user experience design coordinate to create a successful interactive application.

Creative coders are required to work not in one specific software package or programming language, but to use a variety of Application Program Interfaces (APIs) and frameworks to create artistic and commercial works. Hence, the goal of the course will be to begin with skills and frameworks addressed in MAGD 150 and to expand outward from there.

### Course Objectives

By the end of this course, students will be able to

  * adapt new media projects from one platform to another;
  * apply principles of animation to interactive media experiences;
  * explore the relationship between hardware, software and user interaction.

### Course Materials

It is crucial to shift between traditional literacy - the ability to read a book considered to be an 'authoritative text' in the field - and new media literacy - the ability to navigate search engines, repositories, forums, software menus, tutorial videos, professional portfolios and reference documentation.

New media literacy includes:

  * Generalizing a specific example to solve a broader problem;
  * Extracting a solution from general approaches to solve a more specific problem;
  * Translating an implementation from one programming language to another (from Java to JavaScript, from Java to C#, etc.);
  * Sourcing free and/or open source alternatives in the event that proprietary software is not available (GIMP for Photoshop, Blender for Maya, Open Office for MS Office);
  * Sourcing hardware to produce material for a project;
  * Understanding the implications of version and cross-browser compatibility for the creation and display of new media projects (different versions of Processing will have different features, browser-based projects should be tested on multiple browsers).

Thus, the software tools below will be emphasized in the course, but students are reminded that alternatives are almost always available and that the following list is by no means comprehensive.

  * [Processing](https://processing.org/)
  * [p5.js](https://p5js.org/)
  * [Atom](https://www.atom.io) or suitable alternative
  * [Arduino](https://www.arduino.cc/)

The following hardware peripherals will be available for loan:

  * [MaKey MaKey](http://www.makeymakey.com/) Deluxe Kit
  * [SparkFun Arduino 101 Kit](https://www.sparkfun.com/products/13844)

Students are encouraged to make frequent backups of digital work (using flash drives, external hard drives, email and/or other source control methods) and to maintain an archive of coursework using University provided storage on [Google Drive](http://www.uww.edu/icit/services/google).

### Etiquette

All students enrolled in this course are encouraged and expected to take ownership of it, and as such to act as active producers of - rather than as passive receptacles for - knowledge. A few guidelines to facilitate this ownership are as follows:

* There are multiple alternatives to learning in a traditional classroom. It is assumed that you have opted into this course not only to learn the subject matter, but because a face-to-face, collaborative learning environment suits your learning style and personal growth. By not attending labs and lectures, you inhibit the education of other students in withdrawing your knowledge and perspective. Thus, you are expected to attend class.
    * Medical and family emergencies will be considered excused absences.

* Please consider the heart of what it means to 'attend'. If you plan to attend, arrive at the agreed upon meeting time; if you must leave before the conclusion, do so discreetly. If another class member has been acknowledged by the room, then to interrupt, ignore or otherwise undermine their address is disrespectful. Even though digital devices such as smart phones and laptops are integral to coursework, they also provide numerous distractions; using them inappropriately will be considered disrespectful. Students found to disrupt the classroom environment will be asked to leave.

* Electronic communications between you, as student, and myself, as instructor, are to be considered semi-formal, and as contributing to this course's records in the event of any dispute. As such, please use only your institutionally provided (UWW) email account and direct correspondence to my UWW email. In the subject, include the class name and topic (e.g., "MAGD271: Assignment clarification"); in the body, adhere to conventional English grammar and syntax, beginning with a salutation and concluding with a valediction.

* Since topics covered in this course are of a technical nature, observe common practices when asking others to assist with problematic code via email:
    * Either include the complete code or provide a link to it so that the recipient can run your program.
    * Detail the results you expected and the difference between expected and actual results. If necessary, include screen captures, copy Exceptions and/or stack traces from logs or consoles.
    * List any solutions you've attempted, including documentation you may have referenced.
    * Separate large problems or questions into smaller, more specific ones, which can be addressed one at a time.

### Academic Misconduct

To uphold the goal of this course as a one in which students learn to create digital artwork, any code or media contained within an assignment submitted for evaluation are presumed to be the work of the student unless attributed. 'Media' may include image, sound or other digitally storable work. Attribution should include the proper name of an author if available, as well as a Uniform Resource Locator (URL) leading to evidence that the resource is in the intellectual commons (i.e., not copyrighted or subject to a registered trademark). Students who violate academic integrity will be subject to procedures outlined in UWS Chapter 14, which may include being asked to redo an assignment, receiving a failing grade in the assignment or in the course.

### UWW Statement

The University of Wisconsin-Whitewater is dedicated to a safe, supportive and non-discriminatory learning environment.  It is the responsibility of all undergraduate and graduate students to familiarize themselves with University policies regarding Special Accommodations, Academic Misconduct, Religious Beliefs Accommodation, Discrimination and Absence for University Sponsored Events (for details please refer to the Schedule of Classes; the 'Rights and Responsibilities' section of the Undergraduate Catalog; the Academic Requirements and Policies and the Facilities and Services sections of the Graduate Catalog; and the 'Student Academic Disciplinary Procedures ([UWS Chapter 14](https://docs.legis.wisconsin.gov/code/admin_code/uws/14)); and the 'Student Nonacademic Disciplinary Procedures' ([UWS Chapter 17](https://docs.legis.wisconsin.gov/code/admin_code/uws/17/Title)).

### Grading Policy

It is your responsibility to track your performance over the course of the semester. Grades will be posted on D2L for your review. Requirements for the successful completion of an assignment will be enumerated and assigned point values.

This course engages with the aesthetic dimension of human experience, wherein the imagination plays a strong role; some requirements will be less specific than others, and taste may play a role in their evaluation. Please ask for clarification of requirements in advance of the assignment deadline.

If you feel that I have made an error recording your grade, please email me to politely request either an explanation or correction.

Since this course aims to acclimate you to project management and to the expectation that you deliver a product to a client according to specifications by the agreed-upon deadline, __late assignments will not be accepted.__

Letter | Lower Bound | Upper Bound | Interpretation
------ | -----------: | -----------: | --------------
A | 94 | 100 | Outstanding achievement; student performance surpasses course expectations.
A- | 90 | 93 | Excellent performance; student clearly exceeds course requirements.
B+ | 87 | 89 | High achievement; student substantially meets requirements and criteria.
B | 84 | 86 | Good work; student meets the course requirements and criteria.
B- | 80 | 83 | Most of the course requirements and criteria are met.
C+ | 77 | 79 | Acceptable performance in the class; student meets nearly all course requirements and criteria.
C | 74 | 76 | Average performance.
C- | 70 | 73 | Below average performance.
D+ | 67 | 69 | Below average performance.
D | 64 | 66 | Below average performance.
D- | 60 | 63 | Below average performance.
F | 0 | 59 | Poor/Fail

### Assignments

Subject to change per discretion of the instructor.

Deadline | Description | Points
-------- | ----------- | ------:
01/26/2017  | Digital Quilt           |  100
02/09/2017  | 3D Landscape            |  100
02/28/2017  | Animation               |  100
03/16/2017  | Website                 |  200
04/06/2017  | Data Visualization      |  200
04/20/2017  | Project Design Document |  100
05/11/2017  | Final Project           |  300
            | Attendance              |  300
            | Total                   | 1400

### Tentative Schedule

Subject to change per discretion of the instructor.

Week | Dates | Description
----: | ----- | -----------
01   | 01/17/2017, 01/19/2017 | Processing Recap
02   | 01/24/2017, 01/26/2017 | 3D
03   | 01/31/2017, 02/02/2017 | 3D
04   | 02/07/2017, 02/09/2017 | Migration to p5.js
05   | 02/14/2017, 02/16/2017 | Animation
06   | 02/21/2017, 02/23/2017 | Animation
07   | 02/28/2017, 03/02/2017 | Animation, Website
08   | 03/07/2017, 03/09/2017 | Website
09   | 03/14/2017, 03/16/2017 | Website, Data Visualization
10   | 03/28/2017, 03/30/2017 | Data Visualization
11   | 04/04/2017, 04/06/2017 | Data Visualization
12   | 04/11/2017, 04/13/2017 | Final Project
13   | 04/18/2017, 04/20/2017 | Final Project
14   | 04/25/2017, 04/27/2017 | Final Project
15   | 05/02/2017, 05/04/2017 | Final Project
16   | [05/11/2017](http://www.uww.edu/Documents/registrar/Schedule_of_Classes/Spring_2017/2017%20Spring%20Exam%20Schedule.pdf) | Project Presentation
 | 7:45 a.m. - 9:45 a.m. | Section 01
 | 10:00 a.m. - 12:00 p.m. | Section 02
